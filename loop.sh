#!/bin/bash -x
# first argument is the green-miner ID
MINER=$1
#while [ 1 ]
#do
bash mount_apks.sh
adb devices
#./set_usb.py $MINER on
./process_queue.py $MINER
echo "Oh it crashed!"
python3 -c "import libgreenminer; libgreenminer.StatusUpdater('$MINER').crashed()"
sleep 1
echo "Let's use this time constructively!"
echo "Rebooting the phone for 60s"
timeout -sKILL 60 adb reboot
echo "USB power on"
sudo hub-ctrl -h 0 -P 4 -p 1
sudo hub-ctrl -h 0 -P 2 -p 1
bash ~/green-miner/adb_init.sh
echo "let's do a git pull, too"
git pull
echo "Waiting 30 seconds for the reboot. Press ctrl-c to quit"
sleep 10
#done
# ok we're doing this so when you git pull and the loop.sh changes then this will change as well
echo "Execing loop.sh $MINER again"
exec bash -x loop.sh $MINER
