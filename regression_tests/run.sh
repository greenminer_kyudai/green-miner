#
# Copyright (c) 2013 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

DEVICE_ID=$1

FilterTest() {
	output=`eval $1`
   	echo "$output" | grep -e '^\s*\*\*\*'
}

# make sure we're running from project root
if [[ `pwd` =~ 'regression_tests$' ]]; then
	cd ..
fi

FilterTest "./regression_tests/sanity_check.sh $DEVICE_ID"

echo ' *** TEST: running python unit tests'
nosetests 2>&1 | sed 's/^/\t/'

if [ -z $DEVICE_ID ]; then
	echo ' *** MSG: No device id found, exiting early'
	exit
fi

# run tests that require a device
echo ' *** TEST: running set_usb tests'
FilterTest "./regression_tests/check_set_usb.py $DEVICE_ID"
