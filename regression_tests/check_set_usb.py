#!/usr/bin/env python3
#
# Copyright (c) 2013 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os, serial, subprocess, sys, time

def error(message, *args):
	return " *** ERROR: " + message.format(*args)

def msg(message, *args):
	return " *** MSG: " + message.format(*args)

# make sure libgreenminer is available to us
sys.path.append(os.path.dirname(sys.path[0]))
from libgreenminer import Phone, DeviceBuilder


deviceid = sys.argv[1]
arduino = DeviceBuilder(deviceid=deviceid).build_arduino()

def check_usb(char, timeout):
	ser = serial.Serial()
	ser.baudrate = 19200
	ser.port = arduino.port
	ser.timeout = None
	ser.open()
	ser.flush()

	start = time.time()
	while start + timeout > time.time():
		reading = ser.readline().strip()
		if reading.startswith('{0}\t{1}'.format(deviceid, char).encode('utf-8')):
			ser.close()
			return True

def test_toggle():
	set_to = 'on'
	check = '+'
	if (check_usb('+', 3)):
		set_to = 'off'
		check = '#'

	subprocess.check_call(['./set_usb.py', deviceid, set_to])
	assert check_usb(check, 3), error('set_usb {0} failed', set_to)
	print(msg('set_usb {0} worked', set_to))

try:
	test_toggle()
	test_toggle()
except AssertionError as e:
	print(e.message)
	exit(1)
