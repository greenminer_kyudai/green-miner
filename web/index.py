#!/usr/bin/env python3
#
# Copyright (c) 2013 Alex Wilson, Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import cgi, datetime, os, pystache, time
from itertools import *
from operator import *
from urllib.parse import quote, urlencode

data_folder = os.path.expanduser('uploads/data')
grapher = "report.py?file={0}"

# Get Arguments from URL (?var1=val1&var2=val2)
arguments = cgi.FieldStorage()
query = arguments.getvalue('q') or ''
sort_type = arguments.getvalue('sort') or 'd'
try:
    start = int(arguments.getvalue('start'))
except:
    start = 0
try:
    number = int(arguments.getvalue('number'))
except:
    number = 100

class Archive(object):
    def __init__(self, path):
        self.path = path

    def dotfile(self):
        return self.path.startswith(".")

    def graph_path(self):
        return grapher.format(quote(self.path))

    def download_path(self):
           return os.path.join("uploads/data", quote(self.path))

    def get_name(self, max_len):
        if (len(self.path) > max_len):
            return self.path[0:max_len - 3] + '...'
        return self.path

    def get_time(self):
        # filename format sdflj.aslkdfj.sodifj.sidjfo.{DATE_TIME}.tar.gz
        time_string = self.path.split(".")[-3]
        return time.strptime(time_string, "%Y%m%dT%H%MZ")

    def get_time_string(self):
        return time.strftime("%Y/%m/%d %H:%M", self.get_time())

    def get_size(self):
        return os.path.getsize(os.path.join(data_folder, self.path))

    def get_size_string(self):
        return "{0: .1f}K".format(self.get_size() / (1024))

sorting = {
    'f': { 'key': attrgetter('path'), 'reverse':True },
    'F': { 'key': attrgetter('path'), 'reverse':False },
    'd': { 'key': methodcaller('get_time'), 'reverse':True },
    'D': { 'key': methodcaller('get_time'), 'reverse':False },
    's': { 'key': methodcaller('get_size'), 'reverse':True },
    'S': { 'key': methodcaller('get_size'), 'reverse':False },
}[sort_type]

archives = []
# Get all archives in folder
for root, dirnames, files in os.walk(data_folder):
    # Filter dotfiles
    files = [f for f in files if not f[0] == '.']
    for f in files:
        if query in f:
            try:
                archives.append(Archive(f))
            except:
                continue
    break # don't recurse to other folders

total_results = len(archives[:])
results = []

# Get page starts
pages = []
page_num = 1
for page in range(0, total_results, number):
    pages.append({
        'number': page_num,
        'start': page,
    })
    page_num += 1;

# Get archives to dispaly
count = 0
for archive in sorted(archives, **sorting)[start:start + number]:
    results.append({
        'name': archive.get_name(max_len=80),
        'report': archive.graph_path(),
        'download': archive.download_path(),
        'date': archive.get_time_string(),
        'size': archive.get_size_string(),
    })
    if(count >= start + number):
        break
    count += 1

def nextpage(current_page, total_results, number):
    if(current_page + number > total_results):
        return current_page - current_page % number
    else:
        return current_page + number

data = {
    'query': query,
    'sort': sort_type,
    'sort_by_file': urlencode({'q': query, 'sort': 'f' if (sort_type  == 'F') else 'F'}),
    'sort_by_date': urlencode({'q': query, 'sort': 'd' if (sort_type == 'D') else 'D'}),
    'sort_by_size': urlencode({'q': query, 'sort': 's' if (sort_type == 'S') else 'S'}),
    'pages': pages,
    'page_prev': max(0, start - number),
    'page_next': nextpage(start, total_results, number),
    'archives': results,
}

print(pystache.render("""Content-type: text/html


<!doctype html>
<html><head>
    <title>greenminer test results</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="resources/bootstrap.min.css" />
    <link rel="stylesheet" href="resources/style.css" />
    <link href='http://fonts.googleapis.com/css?family=Inconsolata' rel='stylesheet' type='text/css'>
    </head><body>
    <div class="navbar" style="margin-bottom: 0">
        <div class="navbar-inner">
            <a class="brand" href="#">GreenMiner</a>
            <ul class="nav">
                <li class="active"><a href="index.py">Test Results</a></li>
                <li><a href="add_tests.html">Add Tests</a></li>
                <li><a href="list_apps.py">Browse Tests and APKs</a></li>
                <li><a href="queue.html">Manage Queue</a></li>
                <li><a href="status.html">Status</a></li>
                <li><a href="graphs.py">Graphing</a></li>
            </ul>
        </div>
    </div>
    <div class="header">
        <h1>Test Results</h1>
    <form class="form-inline" method="GET">
        <input type="text" name="q" value="{{query}}" class="input-large" />
        <button type="submit" value="search" class="btn">Search</button>
        <input type="hidden" name="sort" value="{{sort}}" />
    </form>
    </div>
<div class="pagination">
  <ul>
    <li><a href="index.py?start={{page_prev}}">Prev</a></li>
    {{#pages}}
      <li><a href="index.py?q={{query}}&sort={{sort}}&start={{start}}">{{number}}</a></li>
    {{/pages}}
    <li><a href="index.py?start={{page_next}}">Next</a></li>
  </ul>
</div>
<table class="table table-striped">
<tr class="headers">
    <th><a href="index.py?{{sort_by_file}}">File</a></th>
    <th><a href="index.py?{{sort_by_date}}">Date</a></th>
    <th><a href="index.py?{{sort_by_size}}">Size</a></th>
    <th></th>
    <th></th>
</tr>
    {{#archives}}
        <tr><td>{{name}}</td>
        <td>{{date}}</td>
        <td>{{size}}</td>
        <td><a href="{{report}}">View Report</a></td>
        <td><a href="{{download}}">Download</a></td></tr>
    {{/archives}}
</table>
</body></html>""", data))
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
