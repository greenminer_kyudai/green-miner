#!/usr/bin/env python3
#
# Copyright (c) 2014 Wyatt Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import cgi, os, json

import cgitb
cgitb.enable()

arguments = cgi.FieldStorage()

GIT_REPO = os.path.dirname(os.path.realpath(__file__))

def get_tests_per_app():
    tests_folder = os.path.join(GIT_REPO, 'tests')
    result = {}

    for folder, folders, files in os.walk(tests_folder):
        if folder == tests_folder:
            continue

        # Get the name of the folder
        test_name = os.path.basename(folder)

        # Check if we need to exclude it
        if not test_name.startswith('_'):
            apps = [f for f in folders if not f.startswith('_')]

            for app in apps:
                result[app] = result.get(app, {})
                result[app]['tests'] = result[app].get('tests', []) + [test_name]

                target_location = os.path.join(folder, 'target')

                if os.path.exists(target_location):
                    result[app]['target'] = open(target_location, 'r').readline().strip()
                else:
                    result[app]['target'] = 'android'

        # don't recurse any further
        folders[:] = []

    return result

if __name__ == "__main__":
    # Get the app tests
    app_tests = get_tests_per_app()

    # Create a data array about it
    data = [
        {
            'name': app,
            'tests': sorted(data['tests'], key=lambda x: x.lower()),
            'target': data['target']
        } for app, data in sorted(app_tests.items())
    ]

    print('Content-Type: application/json\n\n')
    print(json.dumps(data))
