#!/usr/bin/env python3
#
# Copyright (c) 2013 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import cgi, datetime, os, pystache, re, time, urllib.request
from itertools import *
from operator import *

from urllib.parse import quote, urlencode

import cgitb
# cgitb.enable()

apk_folder = 'uploads/apks/'
RW_BLOCKSIZE = 512

# make a version filename, and make sure that it won't
# result in directory traversal.
def make_filename(app, version):
    path = os.path.normpath(os.path.join(apk_folder, app, version) + '.apk')
    if os.path.commonprefix([path, apk_folder]) != apk_folder:
        raise ValueError("version name is invalid")
    return path

def handle_upload(stream, filename):
    os.makedirs(os.path.split(filename)[0], exist_ok=True)
    with open(filename, 'wb') as apk:
        while True:
            data = stream.read(RW_BLOCKSIZE)
            if len(data) == 0:
                break
            apk.write(data)
    return True

if __name__ == '__main__':
    form = cgi.FieldStorage()
    message = []
    errors = []
    version = form.getvalue('version', "").strip()
    app = form.getvalue('app', "").strip()


    if os.environ['REQUEST_METHOD'] == 'POST':
        errors = []

        if not app:
            errors.append("app missing")

        if not version:
            errors.append("version missing")

        if 'upload' not in form:
            errors.append("no file received")

        if not errors:
            upload = form['upload'].file
            try:
                filename = make_filename(app, version)

                if handle_upload(upload, filename):
                    message = "file uploaded successfully"
            except ValueError as e:
                errors.append(str(e))

    data = {
        'version': version,
        'message': message,
        'errors': {'each': errors} if errors else None,
    }

    print(pystache.render("""Content-type: text/html


    <!doctype html>
    <html><head>
        <title>greenminer apk upload</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="resources/bootstrap.min.css"/>
        <link rel="stylesheet" href="resources/style.css"/>
        <link href='http://fonts.googleapis.com/css?family=Inconsolata' rel='stylesheet' type='text/css'>
        </head><body>
        <div class="header">
            <h1>greenminer apk upload</h1>
        <form class="form-inline" enctype="multipart/form-data" method="POST">
            <div class="control-group">
                <label for="app">app
                   <input type="text" id="app" name="app" class="input-large"/>
                </label>
                <label for="version">version
                   <input type="text" id="version" name="version" class="input-large"/>
               </label>
            </div>
            <label for="upload">apk file</label>
            <input type="file" id="upload" name="upload" class="input-large"/>
            <button type="submit" class="btn">Upload</button>
        </form>
        </div>

        {{#message}}
        <div class="alert alert-success">
            {{message}}
        </div>
        {{/message}}
        {{#errors}}
        <div class="alert alert-error">
            <ul>
            {{#each}}
                <li>{{.}}</li>
            {{/each}}
            </ul>
        </div>
        {{/errors}}
    </body></html>""", data))
    # vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
