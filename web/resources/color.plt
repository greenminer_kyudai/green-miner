## Formatting for points (1-8)
# Lines
set style line 1 lc rgb '#1F78B4' pt 7
set style line 2 lc rgb '#33A02C' pt 7
set style line 3 lc rgb '#E31A1C' pt 7
set style line 4 lc rgb '#FF7F00' pt 7
set style line 5 lc rgb '#6A3D9A' pt 7
set style line 6 lc rgb '#B15928' pt 7
set style line 7 lc rgb '#A6CEE3' pt 7
set style line 8 lc rgb '#B2DF8A' pt 7
set style line 9 lc rgb '#FB9A99' pt 7
set style line 10 lc rgb '#FDBF6F' pt 7
set style line 11 lc rgb '#CAB2D6' pt 7
set style line 12 lc rgb '#FFFF99' pt 7

## Formatting for Border (9)
set style line 13 lc rgb '#333333' lt 1 lw 2

## Formatting for Grid (10)
set style line 14 lc rgb '#666666' lt 0 lw 1

# Palette
set palette maxcolors 14
set palette defined (\
	0 '#1F78B4',\
	1 '#33A02C',\
	2 '#E31A1C',\
	3 '#FF7F00',\
	4 '#6A3D9A',\
	5 '#B15928',\
	6 '#A6CEE3',\
	7 '#B2DF8A',\
	8 '#FB9A99',\
	9 '#FDBF6F',\
	10 '#CAB2D6',\
	11 '#FFFF99',\
	12 '#333333',\
	13 '#666666')

set style increment user
