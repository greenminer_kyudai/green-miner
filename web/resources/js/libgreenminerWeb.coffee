app = angular.module 'libgreenminerWeb', [ 'ngResource', 'ngAnimate', 'mgcrea.ngStrap' ]

app.controller 'PageCtrl', [ '$scope', ($scope) ->

]

app.controller 'StatusCtrl', [ '$scope', '$interval', 'Errors', 'Status', ($scope, $interval, Errors, Status) ->
    $scope.errors = []
    $scope.statuses = {}

    error_comparator = (a, b) ->
        # compare by date and then device
        date_comp = b.when.valueOf() - a.when.valueOf()
        if date_comp != 0
            return date_comp
        return 1 if a.device > b.device
        return -1 if a.device < b.device
        return 0

    handle_new_errors = (new_errors) ->
        for error in new_errors
            error.when = new Date(error.when.replace(/\s/, 'T'))

        $scope.errors = $scope.errors.concat(new_errors)
        $scope.errors.sort(error_comparator)

        # now we dedupe the array
        $scope.errors = (error for error, i in $scope.errors when \
             i == 0 or error_comparator($scope.errors[i - 1], error) != 0)

    
    # every second we udpate the since_update
    $scope.since_update = 0
    updating = false

    refresh_errors = () ->
        new_errors = Errors.query(() -> handle_new_errors new_errors)

    refresh_status = () ->
        new_statuses = Status.get(() ->
                            $scope.since_update = 0
                            updating = false
                            $scope.statuses = new_statuses)

    refresh_errors()
    refresh_status()

    update_all = () ->
        $scope.since_update++
        if $scope.since_update > 5 and not updating
            updating = true
            refresh_errors()
            refresh_status()
            
    $interval update_all, 1000
]


app.controller 'QueueCtrl', [ '$scope', 'Queue', ($scope, Queue) ->
	$scope.queue = Queue.get_nice()

	# remove an entier batch
	$scope.remove_batch = (batch) ->
		for test in $scope.queue.queue[batch]
			$scope.remove_runs batch, test
		delete $scope.queue.queue[batch]

	# removes runs from a batch, then possibly deletes the batch
	$scope.remove_runs_from_batch = (batch, test) ->
		$scope.remove_runs batch, test

		batch_count = 0
		for test in $scope.queue.queue[batch]
			batch_count += test.count

		if batch_count == 0
			delete $scope.queue.queue[batch]

	# just removes runs from the batch
	$scope.remove_runs = (batch, test) ->
		$scope.queue.total -= test.count
		Queue.report_finished(test.app, test.version, test.test, batch) while (test.count -= 1) >= 0
		test.count = 0

	return
]

app.service 'Queue', [ '$resource', '$http', ($resource, $http) ->
	transform_queue = (data) ->
		data = angular.fromJson(data)
		queue = {}
		total = 0

		for own app_version, test_batch_counts of data
			[app, version] = app_version.split(':')
			
			for own test_batch, count of test_batch_counts
				[test, batch] = test_batch.split('#')
				
				queue[batch] ||= []
				queue[batch].push {
					test: test
					app: app
					version: version
					count: count
				}
				total += count
		{'total': total, 'queue': queue}

	methods = 
		get_nice:
			method: 'GET'
			url: 'queue.pl?show_queue'
			isArray: false
			responseType: 'json'
			transformResponse: transform_queue
		
	resource = $resource 'queue.pl', null, methods

	# now we add a few of our own non-resourcey things
	resource.transform_queue = transform_queue
	resource.report_finished = (app, version, test, batch) ->
		$http.post 'queue.pl?report_finished', angular.toJson([app + ':' + version, test + '#' + batch])

	resource
]

app.service 'Errors', [ '$resource', ($resource) ->
    $resource 'errors.py'
]

app.service 'Status', [ '$resource', ($resource) ->
    $resource 'status.py'
]

app.service 'Apps', [ '$resource', ($resource) ->
    $resource 'get_tests.py'
]

app.service 'Versions', [ '$resource', ($resource) ->
    $resource 'list_files.py'
]

app.controller 'AddTestsCtrl', [ '$scope', 'Apps', 'Versions', '$http', ($scope, Apps, Versions, $http) ->
    $scope.newTest = {}

    $scope.apps = Apps.query()

    $scope.toggleTest = (test) ->
        n = $scope.newTest.tests.indexOf(test)

        if n >= 0
            $scope.newTest.tests.splice n
        else
            $scope.newTest.tests.push test

    $scope.isTestActive = (test) ->
        n = $scope.newTest.tests.indexOf(test)
        n >= 0

    $scope.getListName = ->
        if $scope.newTest.app?
            if $scope.newTest.app.target is 'android'
                'apk'
            else
                'tarballs'
        else
            null

    $scope.$watch 'newTest.app', (selected_app) ->
        $scope.newTest.tests = []

    $scope.queueAction = ->
        list_name = $scope.getListName()
        if list_name?
            if list_name is 'apk'
                "queue.pl?add_tests"
            else if list_name is 'tarballs'
                "queue_tarball.pl?add_tests"
        else
            "."

    $scope.updateBatchName = ->
        $scope.newTest.batch_name = $scope.newTest.batch_name.replace(/\s+/, '-')
        return
]

app.directive 'progressBar', () ->
    definition =
        restrict: 'E'
        scope:
            max: '='
            current: '='
        link: ($scope) ->
            $scope.width = () ->
                -50 + (Math.min(($scope.current / $scope.max), 1) * 50) + 'px'
        template: '''<div class="progressBar">
                    <span ng-style="{'margin-left': width()}"></span>
                   </div>'''

    return definition
