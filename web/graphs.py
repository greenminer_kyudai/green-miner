#!/usr/bin/env python3
#
# Copyright (c) 2013 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import cgi, json, os, pystache, re, shutil, subprocess, sys, time
import statistics

import cgitb
cgitb.enable()

arguments = cgi.FieldStorage()

GIT_REPO = os.path.expanduser("/var/green-star")
RSCRIPT = os.path.expanduser("Rscript")
WEBDAV_PATH = 'uploads/data'

def make_selection(options, name):
    selected = arguments.getvalue(name)
    options = sorted(set(options))
    return [dict(value=o, selected=(o == selected)) for o in options]

def get_devices():
    return sorted(json.loads(open(os.path.join(GIT_REPO, "devices.json")).read()).keys())


def get_test_names():
    possible_tests = os.listdir(os.path.join(GIT_REPO, "tests"))
    for folder in possible_tests:
        full_path = os.path.join(GIT_REPO, "tests", folder)
        if not folder.startswith("_"):
            yield folder

def get_batch_names():
    # file names: commit.test.device.batch
    for f in os.listdir(WEBDAV_PATH):
        try:
            yield f.split('.')[4]
        except:
            pass

def make_graphdir_name(*args):
    # filter out Nones
    args = [str(x) for x in args if x]
    pattern = re.compile('[\W_]+')
    return "".join(pattern.sub('', x) for x in args)

def start_making_graphs(device=None, batch=None, test=None):
    # make graph temp dir
    tmpdir = os.path.join("./graph/", make_graphdir_name(int(time.time()),
        device, batch, test))
    os.makedirs(tmpdir, exist_ok=True)
    web_dir = os.getcwd()

    # copy files to graphing dir
    shutil.copy(os.path.join(web_dir, 'models.csv'), tmpdir)
    shutil.copy(os.path.join(web_dir, 'make_graphs.R'), tmpdir)

    # writes to files in the local folder
    results = statistics.Results()
    archives = statistics.get_archive_list(results, batch=batch, test=test,
            device=device)
    stats = statistics.calculate_statistics(archives, results)
    for name, stat in stats.stats.items():
        # trim commit id to 10 characters
        statistics.write_csv(stat, os.path.join(tmpdir, name))

    # run make_graph.R
    os.chdir(tmpdir)
    try:
        rscript = subprocess.Popen([RSCRIPT, os.path.join(web_dir,
            "make_graphs.R")], stderr=subprocess.DEVNULL,
            stdout=subprocess.DEVNULL)

        with open('.Rscript.pid', 'w') as pidfile:
            pidfile.write(str(rscript.pid))
    finally:
        os.chdir(web_dir)
    tmpdir = os.path.relpath(tmpdir)
    return (tmpdir, stats)

data = {
    'batches': make_selection(get_batch_names(), 'batch'),
    'tests': make_selection(get_test_names(), 'test'),
    'devices': make_selection(get_devices(), 'device'),

    # add filter so we don't mess up the javascript templates
    'javascript': lambda x: '<script type="text/javascript">{}</script>'.format(x)

}
if ('graph' in arguments):
    try:
        graphdir, results = start_making_graphs( \
                    batch=arguments.getvalue('batch', None),
                    test=arguments.getvalue('test', None),
                    device=arguments.getvalue('device', None))
    except subprocess.CalledProcessError as e:
        data['errors'] = {
            'each': [{'err':'An error occurred while graphing your data',
                    'with': [str(e)]
                    }]
        }

    data['graphdir'] = os.path.split(graphdir)[1]
    data['count'] = results.processed
    csvs = [os.path.join(graphdir, f) for f in os.listdir(graphdir) \
            if os.path.splitext(f)[1] == '.csv']
    csvs.append(os.path.join(graphdir, 'make_graphs.R'))
    csvs = sorted(csvs, key=os.path.basename)

    data['output'] = {
        'each': [{'name': os.path.split(f)[1], 'src':f} for f in csvs]
    }

    if results.errors:
        if not data.get('errors'):
            data['errors'] = {'each': []}

        data['errors']['each'] += [{'err': k, 'with': map(str, v)} \
                for k, v in results.errors.items()]


print(pystache.render("""Content-type: text/html


<!doctype html>
<html><head>
    <title>greenminer test results</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="resources/bootstrap.min.css" />
    <link rel="stylesheet" href="resources/style.css" />
    <link href='https://fonts.googleapis.com/css?family=Inconsolata' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.7.2/mustache.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    </head><body data-graph-dir="{{graphdir}}">
    <div class="navbar" style="margin-bottom: 0">
        <div class="navbar-inner">
            <a class="brand" href="#">GreenMiner</a>
            <ul class="nav">
                <li><a href="index.py">Test Results</a></li>
                <li><a href="add_tests.html">Add Tests</a></li>
                <li><a href="list_apps.py">Browse Tests and APKs</a></li>
                <li><a href="queue.html">Manage Queue</a></li>
                <li><a href="status.html">Status</a></li>
                <li class="active"><a href="#">Graphing</a></li>
            </ul>
        </div>
    </div>
    <div class="header">
        <h1>Aggregate Results</h1>
    <form class="form-inline" method="GET">
        <label for="batch">batch</label>
        <select id="batch" name="batch">
            <option value="">all batches</option>
            {{#batches}}
                <option value="{{value}}" {{#selected}}selected{{/selected}}>
                    {{value}}
                </option>
            {{/batches}}
        </select>
        <label for="devices">devices </label>
        <select id="device" name="device">
            <option value="">all devices</option>
            {{#devices}}
                <option value="{{value}}" {{#selected}}selected{{/selected}}>
                    {{value}}
                </option>
            {{/devices}}
        </select>
        <label for="test">test </label>
        <select id="test" name="test">
            {{#tests}}
                <option value="{{value}}" {{#selected}}selected{{/selected}}>
                    {{value}}
                </option>
            {{/tests}}
        </select>
        <button type="submit" name="graph" value="graph" class="btn">graph!</button>
    </form>
    </div>

    {{#count}}
        <div class="alert alert-success">
            {{count}} archives successfully processed
        </div>
    {{/count}}
    {{#output}}
        <dl class="dl-horizontal content-main">
            <dt>data</dt>
            <dd>
                    <ul>{{#each}}
                        <li><a href="{{src}}">{{name}}</a></li>
                    {{/each}}</ul>
            </dd>
            <dt><a href="graph/{{graphdir}}">graphs</a>
                <div id="loadingProgressG">
                <div id="loadingProgressG_1" class="loadingProgressG">
                </div>
                </div>
            </dt>
            <dd>
                <ul id="graphs">
                </ul>
            </dd>
        </dl>
    {{/output}}

    {{#errors}}
        <div class="alert alert-error">
            <dl class="dl">
            {{#each}}
                <dt>{{err}}</dt>
                <dd>
                    <ul>{{#with}}
                        <li>{{.}}</li>
                    {{/with}}</ul>
                </dd>
            {{/each}}
            </dl>
        </div>
    {{/errors}}

        {{#this_is_a_comment}}
            Just below this we change our mustache tags to ||| so that
            it won't mess up our javascript, which includes a mustache
            template. At the bottom of the page, we switch it back.
        {{/this_is_a_comment}}

        {{=||| |||=}} |||#javascript|||
        $(function() {
            var graphdir = $('body').attr('data-graph-dir'),
                graph_list = $('#graphs'),
            update_graphs = function() {
                $.ajax('graph_ajax.py?folder=' + graphdir, {
                    'dataType': 'json',
                    'cache': false,
                    'complete': function(jqXHR, status) {
                        if (!update_graphs.finished) {
                            window.setTimeout(update_graphs, 1000);
                        }
                    },
                    'error': function(jqXHR, status, error) {
                        console.log('error');
                    },
                    'success': function(data, status, jqXHR) {
                        if (data.alive === false) {
                            update_graphs.finished = true;
                            $('body').addClass('ajax-finished');
                        }

                        list = Mustache.render("{{#graphs}}<li><a href='{{src}}'>{{name}}</a></li>{{/graphs}}", data);

                        graph_list.children().remove();
                        graph_list.append(list);
                    }});
           };
            update_graphs.finished = false;
            update_graphs();
        });
        |||/javascript||| |||={{ }}=|||

        <style>
        .ajax-finished #loadingProgressG {
            display: none;
        }

        #loadingProgressG{
        float: right;
        width:50px;
        height:10px;
        overflow:hidden;
        background-color:#E00DE0;
            -moz-border-radius:10px;
            -webkit-border-radius:10px;
            -ms-border-radius:10px;
            -o-border-radius:10px;
            border-radius:10px;
            }

            .loadingProgressG{
                    background-color:#EDB4ED;
                    margin-top:0;
                    margin-left:-50px;
                    -moz-animation-name:bounce_loadingProgressG;
                    -moz-animation-duration:2.7s;
                    -moz-animation-iteration-count:infinite;
                    -moz-animation-timing-function:linear;
                    -webkit-animation-name:bounce_loadingProgressG;
                    -webkit-animation-duration:2.7s;
                    -webkit-animation-iteration-count:infinite;
                    -webkit-animation-timing-function:linear;
                    -ms-animation-name:bounce_loadingProgressG;
                    -ms-animation-duration:2.7s;
                    -ms-animation-iteration-count:infinite;
                    -ms-animation-timing-function:linear;
                    -o-animation-name:bounce_loadingProgressG;
                    -o-animation-duration:2.7s;
                    -o-animation-iteration-count:infinite;
                    -o-animation-timing-function:linear;
                    animation-name:bounce_loadingProgressG;
                    animation-duration:2.7s;
                    animation-iteration-count:infinite;
                    animation-timing-function:linear;
                    width:50px;
                    height:10px;
                    }

            @-moz-keyframes bounce_loadingProgressG{
                    0%{
                        margin-left:-50px;
                        }

                    100%{
                        margin-left:50px;
                        }

                    }

            @-webkit-keyframes bounce_loadingProgressG{
                    0%{
                        margin-left:-50px;
                        }

                    100%{
                        margin-left:50px;
                        }

                    }

            @-ms-keyframes bounce_loadingProgressG{
                    0%{
                        margin-left:-50px;
                        }

                    100%{
                        margin-left:50px;
                        }

                    }

            @-o-keyframes bounce_loadingProgressG{
                    0%{
                        margin-left:-50px;
                        }

                    100%{
                        margin-left:50px;
                        }

                    }

            @keyframes bounce_loadingProgressG{
                    0%{
                        margin-left:-50px;
                        }

                    100%{
                        margin-left:50px;
                        }

                    }
        </style>
</body></html>""", data))
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
