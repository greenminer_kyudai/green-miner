#!/usr/bin/env python3
#
# Copyright (c) 2013 Alex Wilson, Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import cgi, csv, json, os, pystache, sqlite3, tarfile, time, urllib.parse

DATA_FOLDER = 'uploads/data'
GIT_FOLDER = '/var/green-star/'

## Get Arguments from URL (?var1=val1&var2=val2)
arguments = cgi.FieldStorage()
# Check for file argument
if("file" not in arguments):
	print("Content-type: text/plain\n")
	print("Please specify a file to open.")
	sys.exit()
else:
	filename = arguments.getvalue('file')
# Check for tag argument
if("tag" in arguments):
	tag = arguments.getvalue('tag')
else:
	tag = ""

## Load Data & Info
tar = tarfile.open(os.path.join(DATA_FOLDER, tag, filename), "r:gz")
try: #Test Information
	info = json.loads(tar.extractfile("info.json").read().decode('utf-8'))
except:
	info = dict()
try: #Configuration (Before)
	config_before = json.loads(tar.extractfile("before.json").read().decode('utf-8'))
except:
	config_before = dict()
try: #Configuration (After)
	config_after = json.loads(tar.extractfile("after.json").read().decode('utf-8'))
except:
	config_after = dict()

## Partitions
# Partition Info
partition_info = list()
for row in csv.DictReader(open(os.path.join(GIT_FOLDER, "tests", info.get('test'), 'partition_info.csv'))):
	partition_info.append(row['name'])
# Tidy Up
partitions = list()
row_num = 1
for row in csv.DictReader(tar.extractfile("partitions.csv").read().decode('utf-8').split('\n')):
	row['rownum'] = row_num
	row['name'] = partition_info[row_num - 1] # -1 because the row_num does not start at 0

	# Format units (name, decimal_points, unit)
	config = [('joules',2,'J'), ('duration',2,'s'), ('meanwatts',3,'W'), ('minwatt',3,'W'), ('minwatt_time',3,'s'), ('maxwatt',3,'W'), ('maxwatt_time',3,'s')]
	for name, decimal_points, unit in config:
		if(row.get(name)):
			row[name] = "{0:.{1}f}{2}".format(float(row.get(name)), decimal_points, unit)
		else:
			row[name] = "n/a"
	partitions.append(row)
	row_num += 1

## Images
images = list()
for m in tar.getmembers():
	f = m.name
	if(f.endswith('.png')):
		images.append({'name':f})
images = sorted(images, key=lambda x: x['name'])

try: # Overall Stats
	for row in csv.DictReader(tar.extractfile("totals.csv").read().decode('utf-8').split('\n')):
		config = [('duration',3,'s'), ('joules',2,'J'), ('meanwatts',3,'W'), ('maxwatt',3,'W'), ('maxwatt_time',3,'s'), ('minwatt',3,'W'), ('minwatt_time',3,'s')]
		for name, decimal_points, unit in config:
			if(row.get(name)):
				row[name] = "{0:.{1}f}{2}".format(float(row.get(name)), decimal_points, unit)
			else:
				row[name] = "n/a"
		stats = row
except:
	stats = dict()

## Display Website
data = {
	# Website
	'title': info.get('test', '') + ':' + info.get('application', '') + " " + info.get('version', 'Missing'),

	# File
	'path':urllib.parse.quote(os.path.join(tag, filename)),
	'download':os.path.join(DATA_FOLDER, tag, filename),
	'filename':filename,
	'tag': tag,

	# Images
	'has_images': not not images, # <sarcasm> SO PYTHONIC <3 </sarcasm>
	'images': images,

	# Test Config / Info
	'device': info.get('device'),
	'hostname': info.get('hostname'),
	'batch': info.get('batch'),
	'test': info.get('test'),
	'version': info.get('version'),
	'time_before': time.strftime("%a, %d %b %Y %H:%M", time.gmtime(config_before.get('time'))),
	'time_after': time.strftime("%a, %d %b %Y %H:%M", time.gmtime(config_after.get('time'))),

	# Arduino Config
	'power_source': "Battery" if info.get('config').get('battery') == True else ("Power Supply" if info.get('config').get('battery') == False else "Unknown"),
	'current_sense': info.get('config').get('tuning').get('type', "Unknown"),

	# Android Config / Info
	'battery_charge_before': config_before.get('charge_level'),
	'battery_charge_after': config_after.get('charge_level'),
	'battery_health_before': config_before.get('battery_health'),
	'battery_health_after': config_after.get('battery_health'),
	'battery_temperature_before': "{0:.0f}&deg;C".format(int(config_before.get('battery_temperature', 0)) / 10),
	'battery_temperature_after': "{0:.0f}&deg;C".format(int(config_after.get('battery_temperature', 0)) / 10),
	'airplane_mode': "On" if config_before.get('airplane_mode_on') == "1" else "Off",
	'wifi_before': config_before.get('wifi', 'None').replace(', ', '<br />'),
	'wifi_after': config_after.get('wifi', 'None').replace(', ', '<br />'),
	'bluetooth': "On" if config_before.get('bluetooth_on') == "1" else "Off",
	'screen_timeout': "{0:.0f}s".format(int(config_before.get('screen_off_timeout', 0)) / 1000),
	'screen_auto_brightness': "Enabled" if config_before.get('screen_auto_brightness') == "1" else "Disabled",
	'screen_brightness': config_before.get('screen_brightness'),
	'haptic_feedback': "Enabled" if config_before.get('haptic_feedback_enabled') == "1" else "Disabled",
	'os_version': config_before.get('os_version'),

	# Stats
	'partitions': partitions,
	'runtime': config_after.get('time', 0) - config_before.get('time', 0),
	'joules': stats.get('joules'),
	'average_watt': stats.get('meanwatts'),
	'maxwatt': stats.get('maxwatt'),
	'maxwatt_time': stats.get('maxwatt_time'),
	'minwatt': stats.get('minwatt'),
	'minwatt_time': stats.get('minwatt_time'),
}

# Render Website
print(pystache.render("""Content-type: text/html

<!doctype html>
<html lang="en">
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>
<meta charset="utf-8"/>
<link href="resources/kstyle.css" rel="stylesheet"/>
</head>
<body>
	<a href="move.py?tag=bad&amp;file={{path}}"><img src="resources/images/exclamation.svg" class="report" title="Tag Data" /></a>
	<a href="{{download}}"><img src="resources/images/download.svg" class="download" title="Download Data" /></a>

    <div class="navbar" style="margin-bottom: 0">
        <div class="navbar-inner">
            <a class="brand" href="#">GreenMiner</a>
            <ul class="nav">
                <li class="active"><a href="index.py">Test Results</a></li>
                <li><a href="add_tests.html">Add Tests</a></li>
                <li><a href="list_apps.py">Browse Tests and APKs</a></li>
                <li><a href="queue.html">Manage Queue</a></li>
                <li><a href="status.html">Status</a></li>
                <li><a href="graphs.py">Graphing</a></li>
		<li class="about"><a href="about/">About</a></li>
            </ul>
        </div>
    </div>
	<img src="resources/images/logo_purple.svg" class="logo" />
	<div class="header">
		<span class="title">{{title}}</span>
		<span class="subtitle">Part of batch '{{batch}}'</span>
		<span class="subtitle">Version: {{version}}</span>
		<span class="subtitle">Ran from {{time_before}} to {{time_after}} ({{runtime}}s)</span>
		<span class="subtitle">Ran on Device '{{device}}' on Host '{{hostname}}'</span>
	</div>

	<div class="content">
		<div class="container">
			<img src="graph_test.py?file={{path}}&png" />
			<img src="stackedsvg.py?file={{path}}" />
			<br />
			<div id="graph_legend">
				{{#partitions}}
				<div class="graph_legend_entry"><div class="color{{rownum}}">&nbsp;</div>{{name}}</div>
				{{/partitions}}
			</div>
			<span>
			Power Consumption for Test: {{joules}}<br />
			Average Power Use (Entire Test): {{average_watt}}<br />
			Maximum Power Consumption: {{maxwatt}} at {{maxwatt_time}}<br />
			Minimum Power Consumption: {{minwatt}} at {{minwatt_time}}
			</span>
			<br style="clear:both;"/>
		</div>
		<br />

		<div class="container">
			<form  method="GET" action="graphs.py">
				<h3>Graph aggregate results for {{test}}</h3>
				<input type="hidden" name="test" value="{{test}}">
				<label><input type="radio" name="batch" value="{{batch}}" checked>in the batch {{batch}} </label>
				<label><input type="radio" name="batch" value="">in any batch</label>
				<br />
				<label><input type="radio" name="device" value="" checked>on any device </label>
				<label><input type="radio" name="device" value="{{device}}">on the device {{device}} </label>
				<br />
				<button type="submit" name="graph" value="graph" class="btn">Create Graphs</button>
			</form>
		</div>
		<br />

		<table>
			<tr><th>Partition</th><th>Time</th><th>Joules</th><th>Average</th><th>Maximum</th><th>Minimum</th>
			{{#partitions}}
			<tr><td class="color{{rownum}}" style="color:#000;">{{name}}</td><td>{{duration}}</td><td>{{joules}}</td><td>{{meanwatts}}</td><td>{{maxwatt}} at {{maxwatt_time}}</td><td>{{minwatt}} at {{minwatt_time}}</td></tr>
			{{/partitions}}
		</table>
		<br />

		<table>
			<tr><th>Info</th><th>Description</th></tr>
			<tr><td>Power Source</td><td>{{power_source}}</td></tr>
			<tr><td>Current Sense</td><td>{{current_sense}}</td></tr>
		</table>
		<br />

		<table>
			<tr><th>Statistic</th><th>Before</th><th>After</th></tr>
			<tr><td>Battery Charge</td><td>{{battery_charge_before}}</td><td>{{battery_charge_after}}</td></tr>
			<tr><td>Battery Health</td><td>{{battery_health_before}}</td><td>{{battery_health_after}}</td></tr>
			<tr><td>Battery Temperature</td><td>{{{battery_temperature_before}}}</td><td>{{{battery_temperature_after}}}</td></tr>
			<tr><td>Airplane Mode</td><td colspan="2">{{airplane_mode}}</td></tr>
			<tr><td>Wireless</td><td>{{{wifi_before}}}</td><td>{{{wifi_after}}}</td></tr>
			<tr><td>Bluetooth</td><td colspan="2">{{bluetooth}}</td></tr>
			<tr><td>Screen Auto Brightness</td><td colspan="2">{{screen_auto_brightness}}</td></tr>
			<tr><td>Screen Brightness</td><td colspan="2">{{screen_brightness}}/255</td></tr>
			<tr><td>Screen Timeout</td><td colspan="2">{{screen_timeout}}</td></tr>
			<tr><td>Haptic Feedback</td><td colspan="2">{{haptic_feedback}}</td></tr>
			<tr><td>OS Version</td><td colspan="2">{{os_version}}</td></tr>
		</table>
	</div>
	<br />

	{{#has_images}}
	<div class="container">
		<strong style="font-size:24px; margin:5px 0 0 5px;">Screenshots</strong>
		<div class="gallery">
			{{#images}}<img src="report_png.py?file={{path}}&image={{name}}" title="{{name}}" />{{/images}}
		</div>
	</div>
	{{/has_images}}
</body>
</html>""", data))
