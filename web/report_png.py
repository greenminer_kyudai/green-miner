# Extracts a png from a .tar.gz file and returns it
#
# Copyright (c) 2013 Alex Wilson, Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import cgi, tarfile
import cgitb
cgitb.enable()

DATA_FOLDER = "/var/uploads/data"

if __name__ == "__main__":
	# Get Arguments from URL (?var1=val1&var2=val2)
	arguments = cgi.FieldStorage()
	filename = arguments["file"].value		# Tar Filename
	image = arguments["image"].value		# Image in Tar to Extract

	tar = tarfile.open(os.path.join(DATA_FOLDER, filename), "r:gz")
	data = tarfile.extractfile(image).read()
	tar.close()

	# PNG Header
	print("Content-type: image/png")
	print("Cache-Control: max-age=3600\n")
	sys.stdout.buffer.write(data)
