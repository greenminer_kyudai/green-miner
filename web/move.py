#!/usr/bin/env python3
#
# Copyright (c) 2013 Alex Wilson, Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import cgi, pystache, shutil, os, sys

data_folder = "/var/www/uploads/data"

if __name__ == '__main__':
	# Get Arguments from URL (?var1=val1&var2=val2)
	arguments = cgi.FieldStorage()

	# Check Arguments
	if("file" not in arguments):
		print("Content-type: text/plain\n")
		print("Please specify a file to move")
		sys.exit()
	elif("tag" not in arguments):
		print("Content-type: text/plain\n")
		print("Please specify a destination")
		sys.exit()
	else:
		filename = arguments["file"].value	# Filename
		tag = arguments["tag"].value		# Destination Folder

	try:
		path = os.path.join(data_folder, tag)
		if not os.path.exists(path):
			os.makedirs(path)
		shutil.move(os.path.join(data_folder, filename), os.path.join(path, filename))
	except Exception as e:
		print("Content-type: text/plain\n")
		print(e)
		sys.exit()

	data = {
		'filename': filename,
		'tag': tag
	}

	print(pystache.render("""Content-type: text/html

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta http-equiv="refresh" content="5; url=index.py" />
<link href="resources/kstyle.css" rel="stylesheet"/>
</head>
<body>
	<div class="move">
		Redirecting to index.py in 5 seconds.<br />
		<hr />
		{{filename}} successfully tagged as {{tag}}<br />
	</div>
</body>
</html>""", data))
