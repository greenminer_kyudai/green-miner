#!/usr/bin/perl
#
# Copyright (c) 2013 Jed Barlow
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
# IMPORTANT HTTP INFORMATIONS
#
# POST to this script with the following parameters
#    name:<type> <description>
#    test:string a name of a test
#    app:string the name of an app
#    repetitions:int
#    batch_name:optional string
#    versions:string a list of versions, seperated by whitespace
#    how:string demand or suggest, they are nearly the same...
#

use strict;
use CGI;
use JSON;
use Encode;
use Fcntl;
use DB_File;


#{{{ Settings
# The number of runs to assign to a new test if unspecified.
my $DEFAULT_NUM_RUNS = 10;

my ($LOCK_SH, $LOCK_EX, $LOCK_NB, $LOCK_UN) = (1, 2, 4, 8);

my $db_queue_file = 'queue.db';
my $db_done_file = 'done.db';
#}}}
#{{{ Init
#{{{ CGI stuff
my $q = CGI->new;
print $q->header;
#}}}
#{{{ Open databases
#{{{ Messy DB_File stuff
my %h_queue;
my $db_queue_obj = tie %h_queue, 'DB_File', $db_queue_file, O_CREAT|O_RDWR
    or die "Can't open database $db_queue_file\n";
my $db_fd = $db_queue_obj->fd;
open my $DB_QUEUE_F, "+<&=$db_fd"
    or die "Can't open file $db_queue_file\n";

my %h_done;
my $db_done_obj = tie %h_done, 'DB_File', $db_done_file, O_CREAT|O_RDWR
    or die "Can't open database $db_done_file\n";
$db_fd = $db_done_obj->fd;
open my $DB_DONE_F, "+<&=$db_fd"
    or die "Can't open file $db_done_file\n";
#}}}
#{{{ Setup tidy vars to use
my %db = (
    queue => \%h_queue,
    done  => \%h_done );
my %db_fhandles = (
    queue => $DB_QUEUE_F,
    done  => $DB_DONE_F );
my %db_objs = (
    queue => $db_queue_obj,
    done  => $db_done_obj );
#}}}
#}}}
#}}}
#{{{ DB and data helper methods
#{{{ Data stuff
# Format of database: 
# APP:VERSION => "TESTNAME1:RUNSREMAINING1:TESTNAME2:RUNSREMAINING2"
sub get_entry {
	my $app_version = shift();
    my %hash = split ':', $db{pop()}->{$app_version};

	# coerce values into numerical form, not string form
    for my $test (keys %hash) {
		%hash->{$test} = %hash->{$test} * 1;
	}

    \%hash
}
sub write_entry {
    my $app_version = shift();
    my @array = %{shift()};
    $db{shift()}->{"$app_version"} = join ':', @array;
}
sub delete_entry {
	my $app_version = shift;
    delete $db{shift()}->{"$app_version"};
}
sub fill_defaults {
    my $hash_ref = shift;
    for my $app_ver (keys %$hash_ref) {
        for my $test (keys %{$hash_ref->{$app_ver}}) {
            if ($hash_ref->{$app_ver}->{$test} eq "") {
                print "Assuming default run number of $DEFAULT_NUM_RUNS",
                      " for $test of $app_ver\n";
                $hash_ref->{$app_ver}->{$test} = $DEFAULT_NUM_RUNS } } }
}
#}}}
#{{{ Concurrency stuff
sub aquire_read_lock {
    unless (flock(
            $db_fhandles{shift()},
            $LOCK_SH))
    { die "flock error\n"; }
}
sub aquire_write_lock {
    unless (flock(
            $db_fhandles{shift()},
            $LOCK_EX))
    { die "flock error\n"; }
}
sub release_lock {
    $db_objs{$_[0]}->sync;
    flock $db_fhandles{$_[0]}, $LOCK_UN;
}
#}}}
#}}}
#{{{ Actions
#{{{ action_demand_tests
sub action_demand_tests {
	my $hash = shift();
	$hash //= decode_json($q->param('POSTDATA'));
    fill_defaults($hash);
	print "<ul>";
    for my $app_ver (keys %$hash) {
		print "<li>";
        aquire_write_lock('queue');
        print "Queuing tests for $app_ver; ",
              write_entry($app_ver, $hash->{$app_ver}, 'queue'),
              "\n";
        release_lock('queue');
		print "</li>";
    }
	print "</ul>";
}
#}}}

sub action_add_tests {
	my @tests = $q->param('test');
	my $app = $q->param('app');
	my $repetitions = $q->param('repetitions');
	my $batch_name = $q->param('batch_name');
	$batch_name =~ s/[#:]/ /g; # strip out chars that might trip us up
	my @versions = split(' ', $q->param('versions'));
	my $how = $q->param('how');
	my $n_versions = scalar (@versions);

	my %hash = ();
	for my $version (@versions) {
		for my $test (@tests) {
			$hash{"$app:$version"}{"$test#$batch_name"} = $repetitions;
		}
	}

	print "<p>adding $repetitions repetitions of ";
	print join(", ", @tests) . " to $n_versions versions...</p>";

	if ($how eq "suggest") {
		action_suggest_tests(\%hash);
	} elsif ($how eq "demand") {
		action_demand_tests(\%hash);
	}
}

#{{{ action_suggest_tests
sub action_suggest_tests {
	my $hash = shift();
	$hash //= decode_json($q->param('POSTDATA'));
    fill_defaults($hash);
	print "<ul>";
    for my $app_version (keys %$hash) {
        if (defined $db{queue}->{$app_version}) {
            # The version already exists in the database
            aquire_read_lock('queue');
            my $entry_hash = get_entry($app_version, 'queue');
            release_lock('queue');
            my $incomming_entry = $hash->{$app_version};
            for my $test (keys %$incomming_entry) {
                if(defined $entry_hash->{$test}) {
                    print "<li>Skipping already-present test $test for $app_version\n</li>";
                } else {
                    print "<li>Adding test $test for $app_version\n</li>";
                    $entry_hash->{$test} = $hash->{$app_version}->{$test};
                    aquire_write_lock('queue');
                    write_entry($app_version, $entry_hash, 'queue');
                    release_lock('queue');
                }
            }
        } else {
            # The version doesn't yet exist in the database
			print '<li>';
            print "Queuing tests for $app_version: ";
            aquire_write_lock('queue');
            print write_entry($app_version, $hash->{$app_version}, 'queue');
            release_lock('queue');
            print "</li>\n";
        }
    }

	print "</ul>";
}
#}}}
#{{{ action_report_finished
sub action_report_finished {
    # Expecting something of the form
    #   ["APP:Version", "TEST#Batch"]
    # indicating that one run of TEST has been performed on COMMIT

    my $list = decode_json($q->param('POSTDATA'));
    if(ref($list) ne "ARRAY") {
        print(
            "Error: expecting a list; first element should be a " .
            "version, second element should be a test name.\n");
        return;
    }

    my ($app_version, $test) = @$list;

    aquire_write_lock('queue');
    if (defined $db{queue}->{$app_version}) {
        my $entry = get_entry($app_version, 'queue');

        if (defined $entry->{$test}) {
            $entry->{$test}--;
            print(
                "Decrementing run count for $app_version, $test: " .
                "$entry->{$test} runs left\n");

            if ($entry->{$test} <= 0) {
                delete $entry->{$test};
            }

			if (scalar(keys(%{$entry})) <= 0) {
				print("No tests remaining for $app_version\n");
				delete_entry($app_version, 'queue');
			} else {
				print("writing entry\n");
            	write_entry($app_version, $entry, 'queue');
			}

            # Reflect changes in the other database
            aquire_write_lock('done');
            my $fentry = get_entry($app_version, 'done');
            $fentry = {} unless defined $fentry;
            $fentry->{$test}++;
            write_entry($app_version, $fentry, 'done');
            release_lock('done');
        }
        else {
            print "Error: there is no test \"$test\" " .
                  "queued for app version \"$app_version\"\n";
        }
    }
    else {
        print "Error: $app_version is not in the queue\n";
    }
    release_lock('queue');
}
#}}}
#{{{ action_show
sub action_show {
    my $db_name = shift;
    aquire_read_lock($db_name);
    print "{\n";
    my $first = 1;
    for my $app_version (keys %{$db{$db_name}}) {
        $first ? $first = 0 : print ",\n";
        print "\"$app_version\": ",
              encode_json(get_entry($app_version, $db_name));
    }
    print "\n}\n";
    release_lock($db_name);
}
#}}}
#}}}
#{{{ Action dispatcher
my $query = $ENV{QUERY_STRING};
if    ($query =~ m/^show_queue\z/)      { action_show('queue')     }
elsif ($query =~ m/^show_finished\z/)   { action_show('done')      }
elsif ($query =~ m/^suggest_tests\z/)   { action_suggest_tests()   }
elsif ($query =~ m/^demand_tests\z/)    { action_demand_tests()    }
elsif ($query =~ m/^report_finished\z/) { action_report_finished() }
elsif ($query =~ m/^add_tests\z/)		{ action_add_tests() }
else { print "Error: Unknown action"; }
#}}}
#{{{ Uninit
untie %{$db{queue}};
untie %{$db{done}};
#}}}
