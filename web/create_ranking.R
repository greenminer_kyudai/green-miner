#
# Copyright (c) 2013 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
v <- read.csv('partitions.csv')

apps <- unique(v$app)
components <- unique(v$component)
components <- components[components!='Delete Note']


# TODO: weighting
app_scores <- rep(0, length(apps))


# correct alpha for the number of tests
alpha <- 0.10 / (length(apps) * length(apps) / 2) / length(components)
scores <- sapply(apps, function(app) {
	sum(sapply(components, function(component) {
		app_joules <- v[v$app==app & v$component==component, 'meanwatts']

		sum(sapply(apps, function(other_app) {
			if (app == other_app) 0

			p <- t.test(app_joules, v[v$app==other_app & v$component==component, 'meanwatts'], alternative='less')$p.value

			if (p < alpha) print(paste(app, "beat", other_app, "in", component, p))

			if (p < alpha) 1 else 0
		}))
	}))
});

mapply(paste, apps, scores)
