/*
 11-14-2013
 SparkFun Electronics 2013
 Shawn Hymel

 This code is public domain but you buy me a beer if you use this
 and we meet someday (Beerware license).

 Description:

 This sketch shows how to use the SparkFun INA169 Breakout
 Board. As current passes through the shunt resistor (Rs), a
 voltage is generated at the Vout pin. Use an analog read and
 some math to determine the current. The current value is
 displayed through the Serial Monitor.

 Hardware connections:

 Uno Pin    INA169 Board    Function

 +5V        VCC             Power supply
 GND        GND             Ground
 A0         VOUT            Analog voltage measurement

 VIN+ and VIN- need to be connected inline with the positive
 DC power rail of a load (e.g. an Arduino, an LED, etc.).

 */


void setup() {
  // Initialize serial monitor
  Serial.begin(115200);
}

int getRawSensorValue(const int SENSOR_PIN){
  int rawSensorValue;   // Variable to store value from analog read

  // Read a value from the INA169 board
  rawSensorValue = analogRead(SENSOR_PIN);

  return rawSensorValue;
}



void sendPacket(){
  const int DATA_SIZE = 10;
  const int BUFFER_SIZE = DATA_SIZE * 4 + 4;
  byte buffer[BUFFER_SIZE];


  buffer[0] = 0xff;
  buffer[1] = DATA_SIZE;
  for(int i = 0; i < DATA_SIZE; i++){
    int val_A0 = getRawSensorValue(A0);
    int val_A1 = getRawSensorValue(A1);
    buffer[2 + i * 4]     = val_A0 >> 8;
    buffer[2 + i * 4 + 1] = val_A0;
    buffer[2 + i * 4 + 2] = val_A1 >> 8;
    buffer[2 + i * 4 + 3] = val_A1;
  }
  buffer[2 + DATA_SIZE * 4] = 0xff;
  buffer[2 + DATA_SIZE * 4 + 1] = 0xff;

  Serial.write(buffer, BUFFER_SIZE);
}

void loop() {
  sendPacket();
}
