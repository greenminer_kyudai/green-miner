import serial
import time
import struct
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
import os

def transform_amperage(value_A0, count):
    RS = 0.1
    REF_VOLTAGE = 5.0
    value_A0 = int.from_bytes(value_A0, byteorder='big')
 
    sensorValue = (value_A0 * REF_VOLTAGE) / 1023.0
    current = sensorValue / (10 * RS)
    # print(str(raw_data) + " A: " +  str(current) + " count: " + str(count))
    return current * 1000

def transform_voltage(value_A1):
    REF_VOLTAGE = 5.0
    value_A1 = int.from_bytes(value_A1, byteorder='big')
    # load_voltage is voltage between Vin+ ~ GND
    load_voltage = (value_A1 * REF_VOLTAGE) / 1023.0
    return load_voltage

def wattlog(amperage, voltage, time):
    wattage = voltage * amperage
    return ("%.3f,%.3f,%.3f,%.3f" %(time, amperage, voltage, wattage))

def zscore(x):
    xmean = x.mean()
    xstd  = np.std(x)

    zscore = (x-xmean)/xstd
    return zscore

def boxPlot(data, dir_path):
    # standardization data
    #y = zscore(data)
    y = data

    fig = plt.figure(figsize=(8, 4.5), dpi=100)
    ax = fig.add_subplot(111)
    ax.boxplot(y, 0, '')

    #xlabel("", fontsize=20, fontname='serif') # x軸のタイトル
    plt.ylabel("Ampere [A]", fontsize=20, fontname='serif') # y軸
    plt.xlabel("time [s]", fontsize=20, fontname='serif') # x軸
    plt.title("Readed 60s data (smartphone idling state consumption) Exclude outliers", fontsize=9, fontname='serif') # タイトル

    #plt.ylim([0.0460, 0.0465])

    plt.savefig(dir_path + "/box.png")

    #plt.show()

def scatterPlot(data, time, dir_path):
    # standardization data
    x = time
    y = data
    #y = zscore(data)

    plt.figure(figsize=(8, 4.5), dpi=100)
    plt.scatter(x, y, marker='.', s=10)
    plt.ylabel("Ampare [A]", fontsize=20, fontname='serif') # y軸
    plt.xlabel("time [s]", fontsize=20, fontname='serif') # x軸
    plt.title("Readed 60s data (smartphone idling state consumption)", fontsize=15, fontname='serif') # タイトル

    #plt.ylim([-2, 5])

    plt.savefig(dir_path + "/scat.png")

    #plt.show()



def main():
    count = 0
    data_array = np.array([])
    time_array = np.array([])
    with serial.Serial('/dev/cu.usbmodem1411', 57600, timeout=None) as ser:
        ser.reset_input_buffer()
        start_time = time.time()
        while True:
            elapsed_time = time.time() - start_time
            if ser.read(1).hex() == "ff":
                if ser.read(1).hex() == "ff":
                    if ser.read(1).hex() == "ff":
                        packet_length = int.from_bytes(ser.read(1), byteorder='big')
                        # data = ser.read(packet_length * 2)
                        for i in range(packet_length):
                            rawValue_A0 = ser.read(2)
                            rawValue_A1 = ser.read(2)
                            amperage = transform_amperage(rawValue_A0, count)
                            voltage = transform_voltage(rawValue_A1)
                            if(amperage > 1.5):
                                continue
                            data_array = np.append(data_array, amperage)
                            time_array = np.append(time_array, elapsed_time)
                            print(wattlog(amperage, voltage, elapsed_time))
                        count += packet_length
                        # print(data)
            if (elapsed_time > 60):
                print("readed 60s data")
                #print(str(elapsed_time) + "s")
                #print(str(70000 / elapsed_time) + " rps")
                print("count: " + str(count))
                count = 0
                #start_time = time.time()

                print("stdev: " + str(np.std(data_array)))
                print("var:   " + str(np.var(data_array)))
                dir_path = datetime.now().strftime("./result/%Y%m%d-%H%M%S")
                os.mkdir(dir_path)
                boxPlot(data_array, dir_path)
                scatterPlot(data_array, time_array, dir_path)
                break

if __name__ == '__main__':
    main()
