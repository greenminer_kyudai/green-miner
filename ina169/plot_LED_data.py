import numpy as np
import matplotlib.pyplot as plt

# generate data
x = np.random.rand(100)
y = np.random.rand(100)

df = [36,62,78,50,65,40]
data = (df)

fig = plt.figure()
ax = fig.add_subplot(111)

# 箱ひげ図をつくる
bp = ax.boxplot(data)

plt.grid()
plt.ylim([0,100])
plt.show()
