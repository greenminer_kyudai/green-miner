import serial
import time
import numpy as np

def transform_amperage(value_A0):
    RS = 0.1
    REF_VOLTAGE = 5.0
    value_A0 = int.from_bytes(value_A0, byteorder='big')
    sensorValue = (value_A0 * REF_VOLTAGE) / 1023.0
    current = sensorValue / (10 * RS)
    return current * 1000

def transform_voltage(value_A1):
    REF_VOLTAGE = 5.0
    value_A1 = int.from_bytes(value_A1, byteorder='big')
    # load_voltage is voltage between Vin+ ~ GND
    load_voltage = (value_A1 * REF_VOLTAGE) / 1023.0
    return load_voltage

def wattlog(amperage, voltage, time):
    wattage = voltage * amperage
    return ("%.3f,%.3f,%.3f,%.3f" %(time, amperage, voltage, wattage))

def main():
    m_A_array = np.array([])
    V_array = np.array([])
    with serial.Serial('/dev/cu.usbmodem1411', 115200, timeout=None) as ser:

        # flush buffer
        while ser.in_waiting > 1:
            ser.read(1)

        start_time = time.time()
        while True:
            elapsed_time = time.time() - start_time
            # read packet
            if ser.read(1).hex() == "ff":
                if ser.read(1).hex() == "ff":
                    if ser.read(1).hex() == "ff":
                        packet_length = int.from_bytes(ser.read(1), byteorder='big')
                        for i in range(packet_length):
                            rawValue_A0 = ser.read(2)
                            rawValue_A1 = ser.read(2)
                            amperage = transform_amperage(rawValue_A0)
                            voltage = transform_voltage(rawValue_A1)
                            m_A_array = np.append(m_A_array, amperage)
                            V_array = np.append(V_array, voltage)
                            print(wattlog(amperage, voltage, elapsed_time))
            if (elapsed_time > 10):
                print("readed 60s data")
                print("mA_median: " + str(np.median(m_A_array)))
                print("vol_median: " + str(np.median(V_array)))
                break

if __name__ == '__main__':
    main()
