
import libgreenminer, subprocess

class Test(libgreenminer.AndroidTest):
    def before(self, run):
        # install junit tests apk
        path = "/home/pi/green-star/tests/pwdhash_10_method_suites"
        run.phone.install_apk(path + "/junit_test/PwdHashAppTest-debug.apk")

        # push scripts onto phone
        subprocess.call(["adb", "push", path + "/suites/", "/sdcard/suites/"])

    def after(self, run):
        run.phone.shell("pm uninstall com.uploadedlobster.PwdHash.test")

        # rm suites that were pushed onto phone
        subprocess.call("adb shell rm -rf /sdcard/suites/", shell=True)

