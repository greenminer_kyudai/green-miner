# Wait for wattlog
microsleep 8000000
 
# Account setup
{{{timing}}}
sh /sdcard/suites/suite_createaccount.sh >> /sdcard/testlog 2>&1
microsleep 2000000

# Insert 10 drafts
{{{timing}}}
sh /sdcard/suites/suite_guitests.sh >> /sdcard/testlog 2>&1
microsleep 3000000 

# Search emails
{{{timing}}}
sh /sdcard/suites/suite_search.sh >> /sdcard/testlog 2>&1
microsleep 2000000 
sh /sdcard/suites/suite_search.sh >> /sdcard/testlog 2>&1
microsleep 2000000 
sh /sdcard/suites/suite_search.sh >> /sdcard/testlog 2>&1
microsleep 2000000 
sh /sdcard/suites/suite_search.sh >> /sdcard/testlog 2>&1
microsleep 2000000 
sh /sdcard/suites/suite_search.sh >> /sdcard/testlog 2>&1
microsleep 2000000 

# idle time 
{{{timing}}}

