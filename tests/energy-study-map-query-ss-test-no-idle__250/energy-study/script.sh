# Wait for wattlog
microsleep 10000000
 
# Just setup and teardown overhead
{{{timing}}}
sh /sdcard/suites/suite_setup_and_teardown.sh >> /sdcard/testlog 2>&1 

# Querying on HashMap
{{{timing}}}
sh /sdcard/suites/suite_hm_query.sh >> /sdcard/testlog 2>&1 

# Querying on HashedMap
{{{timing}}}
sh /sdcard/suites/suite_hashedm_query.sh >> /sdcard/testlog 2>&1 

# Querying on LinkedHashMap
{{{timing}}}
sh /sdcard/suites/suite_lhm_query.sh >> /sdcard/testlog 2>&1 

# Querying on LinkedMap
{{{timing}}}
sh /sdcard/suites/suite_lm_query.sh >> /sdcard/testlog 2>&1 

# Querying on TreeMap
{{{timing}}}
sh /sdcard/suites/suite_tm_query.sh >> /sdcard/testlog 2>&1 

# idle time 
{{{timing}}}

