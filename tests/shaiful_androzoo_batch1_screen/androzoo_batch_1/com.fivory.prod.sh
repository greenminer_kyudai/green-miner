# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.fivory.prod/com.ei.bluemium.client.controls.activities.StartActivity
microsleep 8000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent 61
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
tapnswipe /dev/input/event1 tap 200 400 300
microsleep 2000000
tapnswipe /dev/input/event1 tap 300 500 1000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 20 100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 312 1056 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 180 754 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 618 509 1000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 20 1000 2000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 594 270 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 525 432 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 577 627 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 216 917 300
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME