# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.yinzcam.nfl.eagles/com.yinzcam.nfl.mobile.home.HomeActivity
microsleep 8000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent 61
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
tapnswipe /dev/input/event1 tap 200 400 300
microsleep 2000000
tapnswipe /dev/input/event1 tap 300 500 1000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 20 100 2000
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 166 483 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 29 758 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 80 357 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 301 147 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 415 589 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 468 756 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 496 135 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 263 887 300
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 217 400 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 562 523 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100 300
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 407 432 300
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 tap 20 100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 246 701 300
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME