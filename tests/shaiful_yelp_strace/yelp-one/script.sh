# Wait for Wattlog
microsleep 10000000

# Load app and setup
{{{timing}}}
am start -n com.yelp.android/.ui.activities.RootActivity
microsleep 10000000

# Close login screen
input keyevent BACK
microsleep 3000000

# Search for a place to eat tacos
{{{timing}}}
tapnswipe /dev/input/event1 tap 454 214   # click on search bar
microsleep 2000000
input text "tacos"
microsleep 3000000

tapnswipe /dev/input/event1 tap 257 309   # Choose location of search
microsleep 5000000
input keyevent DEL  # clear location box
microsleep 2000000
input text "University%sof%sAlberta"
microsleep 2000000

tapnswipe /dev/input/event1 tap 660 248   # click search button
microsleep 5000000

# look at restaurant
{{{timing}}}
tapnswipe /dev/input/event1 tap 430 444  # click a restaurant
microsleep 10000000

tapnswipe /dev/input/event1 swipe 712 1000 712 300 600  # scroll down
microsleep 4000000
tapnswipe /dev/input/event1 swipe 712 1000 712 300 600  # scroll down
microsleep 4000000

# Exit app
{{{timing}}}
microsleep 5000000
input keyevent HOME


