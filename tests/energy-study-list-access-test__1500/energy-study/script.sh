# Wait for wattlog
microsleep 10000000
 
# Just setup and teardown overhead
{{{timing}}}
sh /sdcard/suites/suite_setup_and_teardown.sh >> /sdcard/testlog 2>&1 

# Random access on ArrayList
{{{timing}}}
sh /sdcard/suites/suite_al_rnd_acc.sh >> /sdcard/testlog 2>&1 

# Random access on Trove Int ArrayList
{{{timing}}}
sh /sdcard/suites/suite_t_al_rnd_acc.sh >> /sdcard/testlog 2>&1 

# Random access on LinkedList
{{{timing}}}
sh /sdcard/suites/suite_ll_rnd_acc.sh >> /sdcard/testlog 2>&1

# Random access on Trove Int Linked list
{{{timing}}}
sh /sdcard/suites/suite_t_ll_rnd_acc.sh >> /sdcard/testlog 2>&1  

# Random access on TreeList
{{{timing}}}
sh /sdcard/suites/suite_tl_rnd_acc.sh >> /sdcard/testlog 2>&1 


# idle time 
{{{timing}}}

