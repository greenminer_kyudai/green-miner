import libgreenminer, subprocess

class Test(libgreenminer.AndroidTest):
    def before(self, run):
        # install junit tests apk
        path = "/home/pi/green-star/tests/cpu-use"
        run.phone.install_apk(path + "/junit_test/ListInterface.apk")

    def after(self, run):
        # uninstall test apk
        run.phone.shell("pm uninstall com.example.helloworld.test")

