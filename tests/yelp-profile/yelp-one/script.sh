# Wait for Wattlog
microsleep 10000000

# Load app and setup
{{{timing}}}
am start -n com.yelp.android/.ui.activities.RootActivity
microsleep 15000000

# Close login screen
input keyevent BACK
microsleep 6000000

# Search for a place to eat tacos
{{{timing}}}
tapnswipe /dev/input/event1 tap 454 214   # click on search bar
microsleep 2000000
input text "tacos"
microsleep 3000000

tapnswipe /dev/input/event1 tap 257 309   # Choose location of search
microsleep 5000000
input keyevent DEL  # clear location box
microsleep 2000000
input text "University%sof%sAlberta"
microsleep 2000000

tapnswipe /dev/input/event1 tap 660 248   # click search button
microsleep 18000000

# look at restaurant
{{{timing}}}
tapnswipe /dev/input/event1 tap 430 444  # click a restaurant
microsleep 8000000

tapnswipe /dev/input/event1 swipe 712 1000 712 300 600  # scroll down
microsleep 9000000
tapnswipe /dev/input/event1 swipe 712 1000 712 300 600  # scroll down
microsleep 9000000

# Click and read comment
{{{timing}}}
tapnswipe /dev/input/event1 tap 410 493   # click on first comment 
microsleep 5000000   # reading comment
tapnswipe /dev/input/event1 tap 410 493   # click on first comment 
microsleep 5000000   # reading comment

tapnswipe /dev/input/event1 swipe 712 1000 712 300 600  # scroll down to read more of comment
microsleep 5000000   # finish reading comment

tapnswipe /dev/input/event1 swipe 712 300 712 1000 600  # scroll back up
microsleep 4000000
tapnswipe /dev/input/event1 swipe 712 300 712 1000 600  # scroll back up
microsleep 4000000

input keyevent BACK   #  hit back button
microsleep 6000000

# read more comments
{{{timing}}}
tapnswipe /dev/input/event1 swipe 712 1000 712 300 600  # scroll down to read more of comments
microsleep 3000000

tapnswipe /dev/input/event1 swipe 712 1000 712 300 600  # scroll down to read more of comments
microsleep 1500000

tapnswipe /dev/input/event1 swipe 712 1000 712 300 600  # scroll down till end of comments
microsleep 7000000

# Go back to result list
{{{timing}}}
tapnswipe /dev/input/event1 tap 20 105   #  hit yelp back button
microsleep 5000000   # read other restaurants

# View restaurant 2
{{{timing}}}
tapnswipe /dev/input/event1 tap 413 603   # click on restaurant
microsleep 5000000

tapnswipe /dev/input/event1 swipe 712 1000 712 300 600 # scroll to comments 
microsleep 5000000   # read comments

tapnswipe /dev/input/event1 swipe 712 1000 712 300 600  # more comments 
microsleep 5000000   # read comments

tapnswipe /dev/input/event1 swipe 712 1000 712 300 600  # more comments 
microsleep 5000000   # read comments

tapnswipe /dev/input/event1 swipe 712 1000 712 300 600  # end of comments
microsleep 5000000   # read comments

tapnswipe /dev/input/event1 swipe 712 300 712 1000 600   # scroll up
microsleep 3000000

# Exit app
{{{timing}}}

tapnswipe /dev/input/event1 tap 21 103  # go back to taco places result
microsleep 8000000

tapnswipe /dev/input/event1 tap 21 103  # go back to main yelp page
microsleep 8000000

input keyevent BACK  # go back 
microsleep 7000000

input keyevent BACK  # go back 
microsleep 7000000

input keyevent BACK  # go back 
microsleep 7000000

input keyevent BACK  # exit app
microsleep 7000000

