# Wait for Wattlog
microsleep 20000000

# Load App
{{{timing}}}
am start -n net.fred.feedex/.activity.MainActivity
microsleep 7000000

##delete previous feeds 
#{{{timing}}}
#tapnswipe /dev/input/event1 tap 154 294 1000
#tapnswipe /dev/input/event1 tap 658 1147
#tapnswipe /dev/input/event1 tap 517 743 


# Add feed
{{{timing}}}
tapnswipe /dev/input/event1 tap 77 1124
microsleep 2000000
input text 'NYtimes'
microsleep 2000000

tapnswipe /dev/input/event1 tap 87 442
microsleep 2000000

input text 'http://pizza.cs.ualberta.ca/gm/tests/_web/nytimes/rss.nytimes.com/services/xml/rss/nyt/HomePage.xml'
microsleep 2000000

tapnswipe /dev/input/event1 tap 540 1139
microsleep 2000000

tapnswipe /dev/input/event1 tap 172 293
microsleep 8000000

## read feed 1
{{{timing}}}

tapnswipe /dev/input/event1 tap 230 218
microsleep 10000000
tapnswipe /dev/input/event1 tap 145 1222
microsleep 4000000

## read feed 2
{{{timing}}}
tapnswipe /dev/input/event1 tap 243 386
microsleep 10000000

tapnswipe /dev/input/event1 tap 145 1222
microsleep 4000000


# "Exit" Process
{{{timing}}}
microsleep 2000000
tapnswipe /dev/input/event1 tap 339 1240
