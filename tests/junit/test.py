import libgreenminer, subprocess

class Test(libgreenminer.AndroidTest):
    """ Runs a junit test suite.

        This allows users to upload a pair of apks (an app and its tests) and
        then have them run with no special knowledge of green miner.

        The apk should be for the app (in greenminer) "junit_service", it is
        identified by a uuid (which is the app version). The junit suite apk
        should have the same app name (junit_service), but the version should
        be the uuid + "_suite".

        TODO: clear files from /sdcard
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.test_packages = []

    def before(self, run):
        self.clear_test_data(run.phone)

    def push_files(self, run):
        # install junit tests apk
        self.test_apk = run.phone.find_apk(run.app, run.version + "_suite")
        self.test_packages = run.phone.install_apk(self.test_apk)

        super().push_files(run)

    def clear_test_data(self, phone):
        for package in self.test_packages:
            phone.shell('pm clear ' + package)

    def get_template_data(self, phone, packages):
        # add test packages info
        return {'TEST_PACKAGES': [{'TEST_PACKAGE': p} for p in self.test_packages]}

    def after(self, run):
        # uninstall test apk        
        for package in self.test_packages:
            run.phone.shell('am force-stop ' + package)
            run.phone.shell("pm uninstall " + package)
