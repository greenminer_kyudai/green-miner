import libgreenminer,time,subprocess
class Test(libgreenminer.AndroidTest):
	def before(self, run):
		# Set Screen Timeout
		self.timeout = run.phone.shell('settings get system screen_off_timeout').strip()
		run.phone.shell('settings put system screen_off_timeout 1250000').strip()
		run.phone.adb("push traceTools/strc.sh /sdcard/")
		run.phone.shell("su -c 'cp /sdcard/strc.sh /data/local/' ")
		run.phone.adb("push traceTools/jtrc.sh /sdcard/")
		run.phone.adb("push traceTools/strace /sdcard/")
		run.phone.shell("su -c 'cp /sdcard/jtrc.sh /data/local/' ")
		run.phone.shell("su -c 'cp /sdcard/strace /data/local/' ")
		run.phone.shell("su -c 'rm /sdcard/jtrc.sh' ")
		run.phone.shell("su -c 'rm /sdcard/strace' ")
		run.phone.shell("su -c 'rm /sdcard/strc.sh' ")
		run.phone.shell("su -c 'chmod 0777 strc.sh'")
		run.phone.shell("su -c 'chmod 0777 strace'")
		run.phone.shell("su -c 'chmod 0777 jtrc.sh'")
		#run.phone.shell("sh /data/local/jtrc.sh&")
                run.phone.shell("sh   /data/local/strc.sh&")
                subprocess.call("adb shell busybox nohup sh /data/local/jtrc.sh& ", shell=True)
                time.sleep(2)




	def after(self, run):
		# Reset Screen Timeout
		run.phone.shell('settings put system screen_off_timeout ' + self.timeout)
		pid = run.phone.shell("ps | grep calculator").split()[1]
		run.phone.shell("am profile " + pid + " stop")

	def before_upload(self,run):
		#Put the files into the folder before uploading
		try:
			pid = run.phone.shell("ps | grep calculator").split()[1]
			run.phone.shell("su -c 'kill "+ pid + "'")
		except:
			pass

		run.phone.adb("pull /data/local/trc.txt "+run.wattlog_file+"_strace.txt")
		run.phone.adb("pull /sdcard/java.trace "+run.wattlog_file+"_java.trace")
