# Paintastic drawing test for black background

# Wait for Wattlog
microsleep 10000000

# Take idle measurement
{{{timing}}}
microsleep 10000000

# Launch App
{{{timing}}}
am start -n com.paintastic/.main.MainActivity
microsleep 7000000

# Change background color to black
tapnswipe /dev/input/event1 swipe 650 1130 100 1130 2000
microsleep 3000000
tapnswipe /dev/input/event1 swipe 650 1130 100 1130 2000
microsleep 3000000 
tapnswipe /dev/input/event1 tap 560 1130
microsleep 2000000
tapnswipe /dev/input/event1 tap 300 575
microsleep 500000
tapnswipe /dev/input/event1 tap 515 1120
microsleep 1000000


# Change size of brush
tapnswipe /dev/input/event1 tap 330 1140
microsleep 3000000
tapnswipe /dev/input/event1 tap 199 284
tapnswipe /dev/input/event1 tap 199 330
microsleep 1000000
tapnswipe /dev/input/event1 tap 500 1050
microsleep 1000000

# Change color of paint
tapnswipe /dev/input/event1 tap 450 1140
microsleep 2000000
tapnswipe /dev/input/event1 tap 225 448
microsleep 500000
tapnswipe /dev/input/event1 tap 500 1130
microsleep 1000000

# Draw image
{{{timing}}}
tapnswipe /dev/input/event1 swipe 320 300 420 310 1000
microsleep 1000000
tapnswipe /dev/input/event1 swipe 420 310 470 360 1000
microsleep 1000000
tapnswipe /dev/input/event1 swipe 470 360 610 370 1000
microsleep 1000000
tapnswipe /dev/input/event1 swipe 610 370 480 450 1000
microsleep 1000000
tapnswipe /dev/input/event1 swipe 480 450 260 450 1000
microsleep 1000000
tapnswipe /dev/input/event1 swipe 260 450 130 350 1000
microsleep 1000000
tapnswipe /dev/input/event1 swipe 130 350 270 350 1000
microsleep 1000000
tapnswipe /dev/input/event1 swipe 270 350 320 300 1000
microsleep 1000000
tapnswipe /dev/input/event1 swipe 260 460 120 800 1000
microsleep 1000000
tapnswipe /dev/input/event1 swipe 370 460 320 900 1000
microsleep 1000000
tapnswipe /dev/input/event1 swipe 480 460 560 700 1000
microsleep 2000000

# Take screencap of image
#screencap -p /sdcard/saucer.png

# Let app idle
{{{timing}}}
microsleep 20000000

# Clear image
{{{timing}}}
tapnswipe /dev/input/event1 tap 670 55
microsleep 500000
tapnswipe /dev/input/event1 tap 500 150
microsleep 500000
tapnswipe /dev/input/event1 tap 360 650
microsleep 3000000

# Return home
{{{timing}}}
input keyevent HOME
