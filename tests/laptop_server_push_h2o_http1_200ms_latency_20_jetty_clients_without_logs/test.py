import libgreenminer,subprocess, logging

logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
fileHandler = logging.FileHandler('/tmp/python.log')
fileHandler.setFormatter(logFormatter)
logger.addHandler(fileHandler)


class Test(libgreenminer.RaspberryPiTest):

    def before(self,run):
#        logger.info('Before function started:')
        # Kill httpclient pid process if running. Delete the pid file.
        subprocess.call("nohup /home/intel/imp_files/kill_pid_file.sh >/dev/null 2>&1 &",shell =True)
        subprocess.call("nohup sudo tc qdisc add dev eth0 root netem delay 200ms >/dev/null 2>&1 &",shell =True)
#        logger.info('Call to parallel_script placed:')
        subprocess.call("nohup /home/intel/green_star_test_repo/green-star/tests/laptop_server_push_h2o_http1_200ms_latency_20_jetty_clients_without_logs/parallel_script.sh >/dev/null 2>&1 &",shell =True)
#        logger.info('Exiting from before method')

    def after(self,run):

#        logger.info('After function started:')
        subprocess.call("nohup sudo tc qdisc del dev eth0 root netem >/dev/null 2>&1 &",shell =True)
 #       logger.info('After function, placed call to kill_pid_file')
        subprocess.call("nohup /home/intel/imp_files/kill_pid_file.sh >/dev/null 2>&1 &",shell =True)
  #      logger.info('Exiting After function')

 
