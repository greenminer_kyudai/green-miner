# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.creatorfactory.swimmyfishGP/com.unity3d.player.UnityPlayerProxyActivity
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 4000000
input keyevent ENTER
microsleep 4000000
input keyevent 61
microsleep 4000000
input keyevent ENTER
microsleep 4000000
input keyevent ENTER
microsleep 4000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 4000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 4000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 62 805
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 151 903
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 51 910
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 675 1008
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 438 456
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 653 32
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 402 273
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 508 804
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 38 186
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 20 695 20
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 179 942
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 591 399
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 34 530
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 668 542
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 605 1077
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 261 1073
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME