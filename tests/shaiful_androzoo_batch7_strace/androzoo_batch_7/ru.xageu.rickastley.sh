# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n ru.xageu.rickastley/ru.xageu.rickastley.RunnerActivity
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 4000000
input keyevent ENTER
microsleep 4000000
input keyevent 61
microsleep 4000000
input keyevent ENTER
microsleep 4000000
input keyevent ENTER
microsleep 4000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 4000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 4000000
###### tap ##########
tapnswipe /dev/input/event1 tap 316 527
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 20 20 20
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1150 695 1150
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 463 1098
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 638 512
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 507 470
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 20 20 1150
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1150 20 20
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 652 27
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 675 834
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 20 695 1150
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 449 568
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME