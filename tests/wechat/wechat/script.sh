# WeChat instant messaging test for 15 messages

# Wait for Wattlog
microsleep 10000000

# Launch app 
{{{timing}}}
am start -n com.tencent.mm/.ui.LauncherUI
microsleep 15000000

# Close popup
tapnswipe /dev/input/event1 tap 225 770
microsleep 10000000

# Login
tapnswipe /dev/input/event1 tap 170 1080
microsleep 10000000

# Click Region
tapnswipe /dev/input/event1 tap 620 220
microsleep 3000000

# Search for region
tapnswipe /dev/input/event1 tap 670 100
microsleep 1000000

# Each phone must be tested with its' own WeChat account
# (WeChat only allows one account to be active on one device at a time)
SERIAL={{{serial}}}

# Location of phones D and B
if [ "$SERIAL" = "01498B1C0100F00B" ] || [ "$SERIAL" = "0149AE240F01D012" ]; then
	input text 'Canada'
fi

# Location of phones C and A
if ["$SERIAL" = "0149B2E80501B00A"] || [ "$SERIAL" = "014E0F501001101C" ]; then 
	input text 'United%sStates'
fi
#microsleep 1000000

# Select country
tapnswipe /dev/input/event1 tap 140 270
microsleep 1000000

# Clear number
tapnswipe /dev/input/event1 tap 650 325
microsleep 1000000

###### different cases
case "$SERIAL" in
        
	"014E0F501001101C")
        # phone A: login A:
	# Enter phone number
	input text '9102906772'
	;;
	
	"0149AE240F01D012")
        # phone B: login B:
	# Enter phone number
	input text '6133191780'
	;;

	"0149B2E80501B00A")
        # phone C: login C:
	# Enter phone number
	input text '4782394595'
	;;

	"01498B1C0100F00B")
        # phone D: login D:
	# Enter phone number 
	input text '7802930217'
	;;
esac

microsleep 1000000

# Click password
tapnswipe /dev/input/event1 tap 330 420
microsleep 1000000

# Enter password
input text 'password'
microsleep 1000000

# Click login
tapnswipe /dev/input/event1 tap 360 570
microsleep 25000000

# Close popup
tapnswipe /dev/input/event1 tap 230 810
microsleep 25000000

# Move through some sort of preview/review thing
tapnswipe /dev/input/event1 tap 405 1150
microsleep 2000000
tapnswipe /dev/input/event1 tap 405 1150
microsleep 2000000
tapnswipe /dev/input/event1 tap 405 1150
microsleep 2000000
tapnswipe /dev/input/event1 tap 365 945
microsleep 5000000

# View contacts
tapnswipe /dev/input/event1 tap 265 1130
microsleep 3000000

# Click tim tam
tapnswipe /dev/input/event1 tap 360 820 
microsleep 2000000

# Click "Message"
tapnswipe /dev/input/event1 tap 360 440
microsleep 5000000

# Click text box
tapnswipe /dev/input/event1 tap 200 1136
microsleep 1000000

# Send some messages to tim tam
{{{timing}}}
# Message 1
input text 'IM%sis%sa%stype%sof%sonline%schat%swhich%soffers%sreal-time...'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1130
microsleep 2000000

# Message 2 
input text '...text%stransmission%sover%sthe%sInternet.%sShort%smsgs%sare...'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1130
microsleep 2000000

# Message 3
input text '...transmitted%sbi-directionally%sbetween%stwo%sparties.'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1130
microsleep 2000000

# Message 4
input text 'Some%sexample%stexts:'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1130
microsleep 2000000

# Message 5
input text 'Omg!!!!%sAre%syou%sserious'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1130
microsleep 2000000

# Message 6
input text 'lol'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1130
microsleep 2000000

# Message 7
input text 'Okay%shaha'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1130
microsleep 2000000

# Message 8
input text 'No%sworries!!!!!'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1130
microsleep 2000000

# Message 9
input text 'I%sjust%snoticed%sthat%syour%semail%ssaid%s2%ssorry'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1130
microsleep 2000000

# Message 10
input text 'false%salarm.%sDont%stell%shillary%sanything!'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1130
microsleep 2000000

# Message 11 
input text 'Oh%sno%ssorry%sI%sthink%sI%ssent%sa%sblank%stext'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1130
microsleep 2000000

# Message 12
input text 'k'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1130
microsleep 2000000

# Message 13
input text 'Argh%sI%sforgot%sto%sreply%sfor%ssure%sthis%ssounds%sgreat'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1130
microsleep 2000000

# Message 14
input text 'Oh%sgeez%sthat%sis%sso%strue...%sYugh'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1130
microsleep 2000000

# Message 15
input text 'bye'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1130
microsleep 2000000

# Wait
{{{timing}}}
microsleep 10000000

# Return to home
{{{timing}}}
input keyevent HOME
