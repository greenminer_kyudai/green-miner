# Exit and leave if there is an error
set -e

LOGFILE=/tmp/script_log.txt
SECONDS=0
# Debug comment
#echo "$(date "+%m%d%Y %T") :Going to wait for wattlog - 10 seconds" >> $LOGFILE 2>&1
# Wait for Wattlog - 10 seconds
/home/pi/bin/microsleep 10000000
#echo "$(date "+%m%d%Y %T") :Wattlog 10 seconds over" >> $LOGFILE 2>&1
{{{timing}}}

# Debug comment
#echo "$(date "+%m%d%Y %T") : switching to h2o directory" >> $LOGFILE 2>&1

# Change to the h2o directory
cd h2o-1.7.0/

# Debug comment
#echo "$(date "+%m%d%Y %T") : switched to h2o directory" >> $LOGFILE 2>&1
#echo "$(date "+%m%d%Y %T") : The current working directory: $PWD" >> $LOGFILE 2>&1

#Check whether the server is already running. If yes, Kill it.
if [ -e /tmp/h20_pid_file.pid ]; then

	# Debug comment
#	echo "$(date "+%m%d%Y %T") : First if condition to kill the server pid" >> $LOGFILE 2>&1
	sudo kill -TERM `cat /tmp/h20_pid_file.pid` > /dev/null 2>&1
    #Just in case, pid doesn't delete on its own.
    rm -f /tmp/h20_pid_file
fi

# Debug comment
#echo "$(date "+%m%d%Y %T") : After if condition going to start the server" >> $LOGFILE 2>&1

# Start server
sudo h2o -m daemon -c ./examples/h2o/h2o.conf &

# Debug comment
#echo "$(date "+%m%d%Y %T") : Start server command issued. Wait 10 seconds." >> $LOGFILE 2>&1

# Wait for it to be ready to serve - 10 seconds. Another 20 seconds to stabilize it. If Jetty, wait 30 seconds
/home/pi/bin/microsleep 40000000

# Debug comment
#echo "$(date "+%m%d%Y %T") : After 30 seconds, server should be started by now. Going to sleep for 150 seconds" >> $LOGFILE 2>&1

{{{timing}}}

#Configure this time in proportion to number of repetitions and concurrent clients.
sleep 195

# Debug comment
#echo "$(date "+%m%d%Y %T") : 150 seconds over, Going to kill the server" >> $LOGFILE 2>&1

{{{timing}}}

# Kill h2o server.
sudo kill -TERM `cat /tmp/h20_pid_file.pid` > /dev/null 2>&1

#echo "$(date "+%m%d%Y %T") : Killed server process.Going to delete h20_pid_file" >> $LOGFILE 2>&1
rm -f /tmp/h20_pid_file.pid

duration=$SECONDS
#echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed." >> $LOGFILE 2>&1
