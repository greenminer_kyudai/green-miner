import libgreenminer

class Test(libgreenminer.AndroidTest):
	def before(self, run):
		# Set Screen Timeout
		self.timeout = run.phone.shell('settings get system screen_off_timeout').strip()
		run.phone.shell('settings put system screen_off_timeout 1800000').strip()

		### Setup Hosts File
		run.phone.shell("su -c mount -o remount,rw /system")
		# Backup host file on android device
		run.phone.shell("cp /etc/hosts /sdcard/hosts_backup")
		# Push new host file
		run.phone.adb("push tests/adblock_hosts_mvps_no_comment/hosts /sdcard/hosts")
		run.phone.shell("su -c cp /sdcard/hosts /etc/hosts")
		run.phone.shell("rm /sdcard/hosts")
		run.phone.shell("su -c mount -o remount,ro /system")

	def after(self, run):
		# Reset Screen Timeout
		run.phone.shell('settings put system screen_off_timeout ' + self.timeout)

		### Takedown Hosts File
		run.phone.shell("su -c mount -o remount,rw /system")
		# Push backed up host file
		run.phone.shell("su -c cp /sdcard/hosts_backup /etc/hosts")
		run.phone.shell("rm /sdcard/hosts_backup")
		run.phone.shell("su -c mount -o remount,ro /system")

		### Get Screenshots
		# Google
		run.phone.adb("pull /sdcard/screen_google.png " + run.wattlog_file + "_screen_google.png")
		run.phone.shell("rm /sdcard/screen_google.png")
		# Wikipedia
		run.phone.adb("pull /sdcard/screen_million.png " + run.wattlog_file + "_screen_million.png")
		run.phone.shell("rm /sdcard/screen_million.png")
		# Filestube
		run.phone.adb("pull /sdcard/screen_filestube.png " + run.wattlog_file + "_screen_filestube.png")
		run.phone.shell("rm /sdcard/screen_filestube.png")
		# Guardian
		run.phone.adb("pull /sdcard/screen_guardian.png " + run.wattlog_file + "_screen_guardian.png")
		run.phone.shell("rm /sdcard/screen_guardian.png")
		# Wikipedia
		run.phone.adb("pull /sdcard/screen_wikipedia.png " + run.wattlog_file + "_screen_wikipedia.png")
		run.phone.shell("rm /sdcard/screen_wikipedia.png")
		# Engadget
		run.phone.adb("pull /sdcard/screen_engadget.png " + run.wattlog_file + "_screen_engadget.png")
		run.phone.shell("rm /sdcard/screen_engadget.png")
