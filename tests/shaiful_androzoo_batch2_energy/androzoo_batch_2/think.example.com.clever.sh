# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n think.example.com.clever/think.example.com.clever.StartScreen2
microsleep 10000000
{{{timing}}}
###### tap ##########
tapnswipe /dev/input/event1 tap 134 911
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 695 100
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 695 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 667 822
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 508 1026
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 591 380
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 20 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 618 252
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 402 662
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 164 1008
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 252 105
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 88 923
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 695 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 598 954
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 52 588
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 20 1000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 695 1000
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 121 259
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 544 534
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 515 487
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 51 376
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 591 709
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 695 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 555 129
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME
