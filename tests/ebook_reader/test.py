import libgreenminer

class Test(libgreenminer.AndroidTest):
    
    def before(self, run):

        # Set Screen Timeout
        self.timeout = run.phone.shell('settings get system screen_off_timeout').strip()
        run.phone.shell('settings put system screen_off_timeout 1800000')

	# Remove epub from sdcard
        run.phone.shell("rm -r /sdcard/Books")

        # Put epub on sdcard
        run.phone.adb("push tests/ebook_reader/aesop.epub /sdcard/Books/aesop.epub")

    def after(self, run):

        # Reset Screen Timeout
        run.phone.shell('settings put system screen_off_timeout ' + self.timeout)

        # Remove PDF from sdcard
        run.phone.shell("rm -r /sdcard/Books")
