# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.suration.app/com.suration.activity.WebViewActivity
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent 61
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 2000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 333 633
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 130 138
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 547 991
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 647 641
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 378 403
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 611 362
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 403 948
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 661 569
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 634 424
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 640 1023
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 695 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 333 843
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 636 551
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 76 744
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 260 364
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 315 975
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 167 523
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 640 746
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME