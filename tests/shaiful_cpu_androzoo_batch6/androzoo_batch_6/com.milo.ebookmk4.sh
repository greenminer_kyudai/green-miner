# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.milo.ebookmk4/com.milo.ebookmk4.EbookActivity
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent 61
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 2000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 20 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 295 1023
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 202 649
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 248 1029
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 695 1000
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 353 190
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 97 427
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 695 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 102 282
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 210 415
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 652 875
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 151 172
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 270 531
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 695 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 90 305
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 580 1081
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 330 115
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 549 224
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 426 216
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME