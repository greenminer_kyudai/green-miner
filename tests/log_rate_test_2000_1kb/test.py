
import libgreenminer, subprocess

class Test(libgreenminer.AndroidTest):
    def before(self, run):
        #clear log
        run.phone.shell("su -c 'rm /sdcard/logcat.txt' ")
        run.phone.adb('logcat -c')
        # install junit tests apk
        path = "/home/pi/green-star/tests/log_rate_test_2000_1kb"
        run.phone.install_apk(path + "/junit_test/LogApplicationTest_2000_1kb.apk")
        # push scripts onto phone
        subprocess.call(["adb", "push", path + "/suites/", "/sdcard/suites/"])

    def after(self, run):
        # uninstall test apk
        run.phone.shell("pm uninstall com.log.logapplication.test")
        # rm suites that were pushed onto phone
        subprocess.call("adb shell rm -rf /sdcard/suites/", shell=True)

    def before_upload(self,run):
        # Put the files into the folder before uploading
        #run.phone.adb("pull /sdcard/logcat.txt "+run.wattlog_file+"_logcat.txt")
        run.phone.shell("su -c 'rm /sdcard/logcat.txt' ")
