# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.necessary.pillorganizer/com.necessary.pillorganizer.activity.MainMenuActivity
microsleep 8000000
{{{timing}}}
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 161 633 300
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100 300
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 38 324 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 384 789 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 20 1000 2000
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 269 800 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 367 509 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 447 540 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 562 920 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 562 919 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 161 696 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 434 223 300
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 365 374 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 515 895 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 481 926 300
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME