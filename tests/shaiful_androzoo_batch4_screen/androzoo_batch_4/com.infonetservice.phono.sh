# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.infonetservice.phono/com.infonetservice.phono.phono
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent 61
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 2000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 314 703
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 695 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 407 692
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 129 421
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 554 894
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 188 499
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 92 996
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 581 523
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 682 245
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 247 263
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 287 718
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 632 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 129 702
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 280 971
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 175 620
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 100 750
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME