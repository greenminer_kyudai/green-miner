# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.mapi.monsterdrop/com.gamesalad.player.GSGameWrapperActivity
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent 61
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 2000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 486 900
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 251 1012
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 346 564
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 20 100
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 385 844
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 600 567
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 632 316
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 212 124
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 618 270
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 312 265
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 164 714
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 329 739
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 442 523
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 477 220
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 695 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 650 299
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 581 812
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 180 214
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 575 401
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME