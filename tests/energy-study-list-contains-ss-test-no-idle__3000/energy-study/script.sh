# Wait for wattlog
microsleep 5000000  

# Just setup and teardown overhead
{{{timing}}}
sh /sdcard/suites/suite_setup_and_teardown.sh >> /sdcard/testlog 2>&1 


# Contains on Integers in ArrayList
{{{timing}}}
sh /sdcard/suites/suite_al_cont.sh >> /sdcard/testlog 2>&1 


# Contains on Integers in LinkedList
{{{timing}}}
sh /sdcard/suites/suite_ll_cont.sh >> /sdcard/testlog 2>&1 


# Contains on Integers in TreeList
{{{timing}}}
sh /sdcard/suites/suite_tl_cont.sh >> /sdcard/testlog 2>&1  


# idle time 
{{{timing}}}

