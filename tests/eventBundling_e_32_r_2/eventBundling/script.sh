#
# eventBundling test
#
# Copyright (c) 2015 Shaiful Chowdhury
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundationeither version 2 of the Licenseor
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If notsee <http://www.gnu.org/licenses/>.
#

# Wait for Wattlog
microsleep 10000000

# Load App, delete and write url
{{{timing}}}

am start -n com.example.shaiful.myapplication/com.example.shaiful.eventbundling.view.MainActivity
microsleep 5000000

######### input##########
{{{timing}}}
tapnswipe /dev/input/event1 tap 337 227
microsleep 1000000
input text 32 ### model
microsleep 1000000

tapnswipe /dev/input/event1 tap 299 302
microsleep 1000000
input text 2 ### rate
microsleep 1000000

tapnswipe /dev/input/event1 tap 353 406
microsleep 1000000
input text 20 ### duration
microsleep 1000000

##enter
tapnswipe /dev/input/event1 tap 271 982

#### wait 10 seconds #################
{{{timing}}}
microsleep 10000000


##### run #####################

{{{timing}}}
microsleep 22000000


# "Exit" Process
{{{timing}}}
microsleep 2000000
input keyevent HOME
