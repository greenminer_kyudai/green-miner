# Wait for Wattlog
microsleep 5000000

sh /sdcard/timing.sh "Load App"
am start -n org.mozilla.fennec/org.mozilla.fennec.App
microsleep 10000000

sh /sdcard/timing.sh "Screencap"
screencap -p /sdcard/screen.png

sh /sdcard/timing.sh "Exit & Wait"
microsleep 2000000
input keyevent HOME
