logcat > /sdcard/logcat.txt &
PID=$!
# Wait for wattlog (10s)
microsleep 10000000

# Load app
{{{timing}}}
am start -n com.example.andrea.wekaandroidport/.MainActivity
# Choose SMO
tapnswipe /dev/input/event1 tap 650 630
microsleep 2000000

# Read data
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 120
microsleep 8000000

# Train CV10
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 360
microsleep 590000000

# Validate CV10
# Split sleep to avoid overflow
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 600
microsleep 2000000000
microsleep 2000000000
microsleep 1670000000

# Idle time
kill $PID
{{{timing}}}
