#
# PDF Reading Test
#	Loads a PDF and scrolls through it
#
# Copyright (c) 2013 Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundationeither version 2 of the Licenseor
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If notsee <http://www.gnu.org/licenses/>.
#

# redirect stdout(1) and stderr(2) to null:
exec 1>/dev/null 2>/dev/null

# Wait for Wattlog
microsleep 10000000

# Load App
sh /sdcard/timing.sh "Load App & Navigate to PDF"
am start -n com.artifex.mupdfdemo/com.artifex.mupdfdemo.ChoosePDFActivity
microsleep 5000000

# Open PDF
sh /sdcard/timing.sh "Open PDF"
tapnswipe /dev/input/event1 tap 150 325
microsleep 10000000

# Setup One Page View
sh /sdcard/timing.sh "Setup One Page View"
# Set up by default

# Scroll Through Pages (Back & Forth Twice)
sh /sdcard/timing.sh "Read PDF"
# 16 times to the right 1/2
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
# 16 times to the left
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
# 16 times to the right 2/2
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
# 16 times to the left
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000

# "Exit" Process
sh /sdcard/timing.sh "Exit & Wait"
microsleep 2000000
input keyevent HOME
