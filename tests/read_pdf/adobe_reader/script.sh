#
# PDF Reading Test
#	Loads a PDF and scrolls through it
#
# Copyright (c) 2013 Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundationeither version 2 of the Licenseor
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If notsee <http://www.gnu.org/licenses/>.
#

# Wait for Wattlog
microsleep 10000000

# Load App
{{{timing}}}
sh /sdcard/timing.sh "Load App & Navigate to PDF"
am start -n com.adobe.reader/com.adobe.reader.AdobeReader
microsleep 5000000
# Don't share
tapnswipe /dev/input/event1 tap 523 725
microsleep 1000000
# Click anywhere
tapnswipe /dev/input/event1 tap 377 522
microsleep 1000000
# Go home
tapnswipe /dev/input/event1 tap 91 47
microsleep 1000000
# Open Menu
tapnswipe /dev/input/event1 tap 35 96
microsleep 1000000
# Open Documents
tapnswipe /dev/input/event1 tap 211 292
microsleep 1000000

# Open PDF
{{{timing}}}
tapnswipe /dev/input/event1 tap 358 246
microsleep 10000000

# Setup One Page View
{{{timing}}}
# Open Document View Settings
tapnswipe /dev/input/event1 tap 442 54
microsleep 1000000
tapnswipe /dev/input/event1 tap 442 54
microsleep 1000000
tapnswipe /dev/input/event1 tap 613 522
microsleep 2500000

# Scroll Through Pages (Back & Forth Twice)
{{{timing}}}
# 16 times to the right 1/2
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
# 16 times to the left
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
# 16 times to the right 2/2
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 700 300
microsleep 3000000
# 16 times to the left
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 600
microsleep 3000000
tapnswipe /dev/input/event1 tap 15 300
microsleep 3000000

# "Exit" Process
{{{timing}}}
microsleep 2000000
input keyevent HOME
