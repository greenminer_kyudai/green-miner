import libgreenminer

class Test(libgreenminer.AndroidTest):
	def push_files(self, testRun):
		super().push_files(testRun)
		# Add Manual
		# NOTE: From directory being run (green-star base)
		testRun.phone.push("tests/read_pdf/gnu_manual.pdf", "/sdcard/Download/")
		# Disable Accelerometer Rotation
		testRun.phone.shell("adb shell content insert --uri content://settings/system --bind name:s:accelerometer_rotation --bind value:i:0")
		# Force Portrait Mode
		testRun.phone.shell("content insert --uri content://settings/system --bind name:s:user_rotation --bind value:i:0")
	def clean_files(self, phone):
		super().clean_files(phone)
		# Remove Manual
		phone.shell('rm /sdcard/Download/gnu_manual.pdf')
