# Cool Reader e-reader test 

# Wait for Wattlog
microsleep 10000000

# Take idle measurement
{{{timing}}}
microsleep 10000000

# Load App
{{{timing}}}
am start -n org.coolreader/.CoolReader
microsleep 10000000

# Close warning
tapnswipe /dev/input/event1 tap 125 500
microsleep 2000000

# Find epub in Books folder
tapnswipe /dev/input/event1 tap 230 600
microsleep 2000000

# Open epub
tapnswipe /dev/input/event1 tap 630 300
microsleep 5000000

# Make sure first page is loaded
# Click settings
tapnswipe /dev/input/event1 tap 670 95
microsleep 1000000
# Click search for page
tapnswipe /dev/input/event1 tap 85 360
microsleep 1000000
# Search for page 1
input text '1'
microsleep 1000000
# Click to search
tapnswipe /dev/input/event1 tap 660 110
microsleep 2000000

# Change background color
# Click Options
tapnswipe /dev/input/event1 tap 370 105
microsleep 1000000
# Click page settings
tapnswipe /dev/input/event1 tap 420 105
microsleep 1000000
# Scroll down
tapnswipe /dev/input/event1 swipe 360 900 360 200 1000
microsleep 1000000
# Click background texture
tapnswipe /dev/input/event1 tap 360 785
microsleep 1000000
# Scroll up
tapnswipe /dev/input/event1 swipe 360 250 360 750 1000
microsleep 1000000
# Click Solid color
tapnswipe /dev/input/event1 tap 45 250
microsleep 1000000
# Click to go back
tapnswipe /dev/input/event1 tap 60 110
microsleep 4000000

### Let app idle
{{{timing}}}
microsleep 20000000

# Search for a word
{{{timing}}}
tapnswipe /dev/input/event1 tap 270 105
microsleep 2000000
input text 'attired'
microsleep 1000000
# Click to search
# Displays result in context
tapnswipe /dev/input/event1 tap 660 110
microsleep 4000000

# Prepare to read
{{{timing}}}
# Click to enter fullscreen mode
tapnswipe /dev/input/event1 tap 360 650
microsleep 2000000
# Click to turn the page
tapnswipe /dev/input/event1 tap 680 630
microsleep 4000000

# Read for a while
{{{timing}}}
# The Stag at the Pool
microsleep 25000000
tapnswipe /dev/input/event1 tap 680 630
# The Stag at the Pool cont.
microsleep 15000000
tapnswipe /dev/input/event1 tap 680 630
# The Fox and the Mask
microsleep 23000000
tapnswipe /dev/input/event1 tap 680 630
# The Bear and the Fox
microsleep 23000000

# Return home
{{{timing}}}
input keyevent HOME
