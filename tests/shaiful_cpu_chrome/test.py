import libgreenminer,time,subprocess
class Test(libgreenminer.AndroidTest):
	def before(self, run):
		# Set Screen Timeout
		self.timeout = run.phone.shell('settings get system screen_off_timeout').strip()
		run.phone.shell('settings put system screen_off_timeout 1250000').strip()
		
		run.phone.adb("push traceCPU/sysInfo_before.sh /sdcard/sysInfo_before.sh")
		run.phone.shell("su -c 'cp /sdcard/sysInfo_before.sh /data/local/' ")
		run.phone.adb("push traceCPU/sysInfo_after.sh /sdcard/sysInfo_after.sh")
		run.phone.shell("su -c 'cp /sdcard/sysInfo_after.sh /data/local/' ")

		run.phone.shell("su -c 'rm /sdcard/sysInfo_before.sh' ")
		run.phone.shell("su -c 'rm /sdcard/sysInfo_after.sh' ")

		run.phone.shell("su -c 'chmod 0777 /data/local/sysInfo_before.sh'")
		run.phone.shell("su -c 'chmod 0777 /data/local/sysInfo_after.sh'")
		
		run.phone.shell("su -c 'touch /data/local/sysInfo_before.txt' ")
		run.phone.shell("su -c 'touch /data/local/cpuLoad_before.txt' ")
		run.phone.shell("su -c 'touch /data/local/sysInfo_after.txt' ")
		run.phone.shell("su -c 'touch /data/local/cpuLoad_after.txt' ")

		run.phone.shell("su -c 'touch /data/local/processInfo.txt' ")

		run.phone.shell("su -c 'touch /data/local/ioInfo.txt' ")
		run.phone.shell("su -c 'touch /data/local/netInfo_before.txt' ")
		run.phone.shell("su -c 'touch /data/local/netInfo_after.txt' ")
		run.phone.shell("su -c 'touch /data/local/memInfo.txt' ")
		run.phone.shell("su -c 'touch /data/local/diskstats_before.txt' ")
		run.phone.shell("su -c 'touch /data/local/diskstats_after.txt' ")
		run.phone.shell("su -c 'touch /data/local/netStat.txt' ")
		run.phone.shell("su -c 'touch /data/local/packet.txt' ")
		run.phone.shell("su -c 'touch /data/local/tcp.txt' ")
		run.phone.shell("su -c 'touch /data/local/tcp6.txt' ")
		run.phone.shell("su -c 'touch /data/local/udp.txt' ")
		run.phone.shell("su -c 'touch /data/local/udp6.txt' ")

		subprocess.call(" adb shell sh /data/local/sysInfo_before.sh org.chromium.chrome.shell", shell=True)


	def after(self, run):

	
		# Reset Screen Timeout
		run.phone.shell('settings put system screen_off_timeout ' + self.timeout)
		subprocess.call(" adb shell sh /data/local/sysInfo_after.sh org.chromium.chrome.shell", shell=True)
		   	

			

	def before_upload(self,run):
		#Put the files into the folder before uploading
		
		run.phone.adb("pull /data/local/sysInfo_before.txt "+run.wattlog_file+"_sysinfo_before.txt")
		run.phone.adb("pull /data/local/sysInfo_after.txt "+run.wattlog_file+"_sysinfo_after.txt")
		run.phone.adb("pull /data/local/cpuLoad_before.txt "+run.wattlog_file+"_cpuLoad_before.txt")
		run.phone.adb("pull /data/local/cpuLoad_after.txt "+run.wattlog_file+"_cpuLoad_after.txt")
		run.phone.adb("pull /data/local/processInfo.txt "+run.wattlog_file+"_processinfo.txt")


		#try:
		run.phone.adb("pull /data/local/ioInfo.txt "+run.wattlog_file+"_ioInfo.txt")
		#except: 
		#	pass #### no io operations
		#try:
		run.phone.adb("pull /data/local/netInfo_before.txt "+run.wattlog_file+"_netInfo_before.txt")
		run.phone.adb("pull /data/local/netInfo_after.txt "+run.wattlog_file+"_netInfo_after.txt")
		#except:
		#	pass ### no network operations. 
		
		run.phone.adb("pull /data/local/netStat.txt "+run.wattlog_file+"_netStat.txt")
		run.phone.adb("pull /data/local/tcp.txt "+run.wattlog_file+"_tcp.txt")
		run.phone.adb("pull /data/local/tcp6.txt "+run.wattlog_file+"_tcp6.txt")
		run.phone.adb("pull /data/local/udp.txt "+run.wattlog_file+"_udp.txt")
		run.phone.adb("pull /data/local/udp6.txt "+run.wattlog_file+"_udp6.txt")
		run.phone.adb("pull /data/local/packet.txt "+run.wattlog_file+"_packet.txt")
		run.phone.adb("pull /data/local/memInfo.txt "+run.wattlog_file+"_memInfo.txt")
		run.phone.adb("pull /data/local/diskstats_before.txt "+run.wattlog_file+"_diskstats_before.txt")
		run.phone.adb("pull /data/local/diskstats_after.txt "+run.wattlog_file+"_diskstats_after.txt")

		## delete the files
		run.phone.shell("su -c 'rm /data/local/sysInfo_before.txt' ")	
		run.phone.shell("su -c 'rm /data/local/cpuLoad_before.txt' ")
		run.phone.shell("su -c 'rm /data/local/sysInfo_after.txt' ")	
		run.phone.shell("su -c 'rm /data/local/cpuLoad_after.txt' ")
		run.phone.shell("su -c 'rm /data/local/processInfo.txt' ")	
	
		run.phone.shell("su -c 'rm /data/local/sysInfo_before.sh' ")
		run.phone.shell("su -c 'rm /data/local/sysInfo_after.sh' ")
		#try:	
		run.phone.shell("su -c 'rm /data/local/ioInfo.txt' ")
		#except:
		#	pass
		#try:
		run.phone.shell("su -c 'rm /data/local/netInfo_before.txt' ")
		run.phone.shell("su -c 'rm /data/local/netInfo_after.txt' ")
		#except:
		#	pass	
		run.phone.shell("su -c 'rm /data/local/diskstats_before.txt' ")
		run.phone.shell("su -c 'rm /data/local/diskstats_after.txt' ")
		run.phone.shell("su -c 'rm /data/local/netStat.txt' ")
		run.phone.shell("su -c 'rm /data/local/packet.txt' ")
		run.phone.shell("su -c 'rm /data/local/tcp.txt' ")
		run.phone.shell("su -c 'rm /data/local/tcp6.txt' ")
		run.phone.shell("su -c 'rm /data/local/udp.txt' ")
		run.phone.shell("su -c 'rm /data/local/udp6.txt' ")
