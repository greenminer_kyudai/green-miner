logcat > /sdcard/logcat.txt &
PID=$!
# Wait for wattlog (10s)
microsleep 10000000

# Load app
{{{timing}}}
am start -n com.example.andrea.wekaandroidport/.MainActivity
# Choose SMO
tapnswipe /dev/input/event1 tap 450 1050
microsleep 2000000

# Read data
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 120
microsleep 6500000

# Train CV10
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 360
microsleep 300000

# Validate CV10
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 600
# microsleep 1500000
microsleep 15000000

# Idle time
{{{timing}}}
kill $PID
