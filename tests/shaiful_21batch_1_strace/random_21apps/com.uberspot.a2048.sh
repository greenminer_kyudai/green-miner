# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.uberspot.a2048/com.uberspot.a2048.MainActivity
microsleep 8000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent 61
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
tapnswipe /dev/input/event1 tap 200 400 300
microsleep 2000000
tapnswipe /dev/input/event1 tap 300 500 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 258 756 1000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 615 1100 100 1100 2000
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 102 924 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 565 901 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 613 1090 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 215 832 300
microsleep 2000000
###### tap menu ##########
tapnswipe /dev/input/event1 tap 682 1236 300
microsleep 2000000
###### tap menu ##########
tapnswipe /dev/input/event1 tap 682 1236 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 100 1100 615 100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 575 683 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 198 514 300
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME