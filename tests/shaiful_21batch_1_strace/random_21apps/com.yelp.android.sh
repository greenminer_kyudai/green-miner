# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.yelp.android/com.yelp.android.ui.activities.RootActivity
microsleep 8000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent 61
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
tapnswipe /dev/input/event1 tap 200 400 300
microsleep 2000000
tapnswipe /dev/input/event1 tap 300 500 1000
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 601 259 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 476 560 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 445 962 300
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 387 477 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 340 726 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 224 525 1000
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME