import libgreenminer

class Test(libgreenminer.AndroidTest):

    def before(self, run):
        
        # Set Screen Timeout
        self.timeout = run.phone.shell('settings get system screen_off_timeout').strip()
        run.phone.shell('settings put system screen_off_timeout 1800000').strip()
        
        # Remove surespot folder from sdcard 
        run.phone.shell("rm -r /sdcard/surespot")
        
        # Put identity file on phone
        run.phone.adb("push tests/surespot/identities/alannabanana666.ssi /sdcard/surespot/identities/alannabanana666.ssi")

    def after(self, run):

        # Reset Screen Timeout
        run.phone.shell('settings put system screen_off_timeout ' + self.timeout)

        # Remove surespot folder from sdcard
        run.phone.shell("rm -r /sdcard/surespot")

    def with_run(self,run):
        self.phone = run.phone.adb("get-serialno").strip()

    def get_template_data(self, phone, packages):
        serial_no = self.phone
        return {"serial": serial_no}
