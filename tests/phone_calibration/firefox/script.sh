# Wait for Wattlog
settings put system screen_off_timeout 180000
microsleep 10000000

# DO NOTHING (DIM SCREEN) (PART 1)
for brightness in 10 70 130 190 255; do
	{{{timing}}}
	settings put system screen_brightness $brightness
	tapnswipe /dev/input/event1 tap 380 640
	microsleep 5000000

	{{{timing}}}
	microsleep 60000000
done

{{{timing}}}
# Reset to our default values
settings put system screen_brightness 120

for waitTime in 500000 1000000 2000000 5000000 10000000; do
	{{{timing}}}
	taps=$((50000000 / $waitTime))

	while [ $taps -gt 0 ]; do
		taps=$(( $taps - 1))
		tapnswipe /dev/input/event1 tap 380 640
		microsleep $waitTime
	done
done

for waitTime in 500000 1000000 2000000 5000000 10000000; do
	{{{timing}}}
	swipes=$((50000000 / $waitTime))

	while [ $swipes -gt 0 ]; do
		swipes=$(( $swipes - 1))
		tapnswipe /dev/input/event1 swipe 380 640 380 650 100
		microsleep $waitTime
	done
done


{{{timing}}}
cat /dev/urandom &
CAT_PID=$!
microsleep 20000000
kill -9 $CAT_PID

{{{timing}}}
am start -n {{APP}}/.App
microsleep 7000000

for page in 'white_on_black' 'black_on_white' 'red' 'blue' 'green'; do
	{{{timing}}}
	tapnswipe /dev/input/event1 tap 400 100
	microsleep 1000000

	# Load Idle Page
	input text "http://http://pizza.cs.ualberta.ca/gm/tests/$page.html"
	tapnswipe /dev/input/event1 tap 667 119
	microsleep 5000000

	{{{timing}}}
	microsleep 20000000
done

{{{timing}}}
# Reset to our default values
settings put system screen_brightness 120
tapnswipe /dev/input/event1 tap 380 640
settings put system screen_off_timeout 30000
microsleep 5000000

# exit and wait
exit
