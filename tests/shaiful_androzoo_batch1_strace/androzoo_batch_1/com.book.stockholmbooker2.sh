# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.book.stockholmbooker2/com.book.stockholmbooker2.MainActivity
microsleep 8000000
{{{timing}}}
###### tap ##########
tapnswipe /dev/input/event1 tap 132 462 300
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 695 1000 2000
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 591 726 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 90 637 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 215 752 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 480 744 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 556 421 1000
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 351 947 1000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 20 1000 2000
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME