# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.lrhsoft.shiftercalendar/com.lrhsoft.shiftercalendar.SplashScreen
microsleep 8000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent 61
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
tapnswipe /dev/input/event1 tap 200 400 300
microsleep 2000000
tapnswipe /dev/input/event1 tap 300 500 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 383 985 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 33 208 300
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 695 1000 2000
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 695 1000 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 56 113 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 234 257 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100 300
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 120 945 300
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 226 916 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 188 609 1000
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 176 521 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 196 1067 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 470 673 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 137 980 300
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME