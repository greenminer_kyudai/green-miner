# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.mycompany.animalsoundsfortoddlers/com.mycompany.animalsoundsfortoddlers.MainActivity
microsleep 8000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent 61
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
tapnswipe /dev/input/event1 tap 200 400 300
microsleep 2000000
tapnswipe /dev/input/event1 tap 300 500 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 488 333 1000
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 633 721 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 522 512 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 315 887 300
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 594 872 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 420 707 1000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 695 100 2000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 440 406 1000
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME