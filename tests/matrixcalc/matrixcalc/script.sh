# Application idle state for 5 seconds.
PACKAGE="com.alexkang.x3matrixcalculator"
ACTIVITY="com.alexkang.x3matrixcalculator.MainActivity"

# Wait for Wattlog
microsleep 10000000

# Launch app ("Launch app" entry in partition_info.csv file)
{{{timing}}}
am start -n $PACKAGE/$ACTIVITY
microsleep 10000000

# Interaction ("interaction" entry in partition_info.csv file)
{{{timing}}}

# Complete 1st Matrix
tapnswipe /dev/input/event1 tap 118 259
microsleep 250000
input text "5"
microsleep 250000
tapnswipe /dev/input/event1 tap 627 1139
microsleep 250000
input text "5"
microsleep 250000
tapnswipe /dev/input/event1 tap 627 1139
microsleep 250000
input text "5"
microsleep 250000
tapnswipe /dev/input/event1 tap 627 1139
microsleep 250000
input text "5"
microsleep 250000
tapnswipe /dev/input/event1 tap 627 1139
microsleep 250000
input text "5"
microsleep 250000
tapnswipe /dev/input/event1 tap 627 1139
microsleep 250000
input text "5"
microsleep 250000
tapnswipe /dev/input/event1 tap 627 1139
microsleep 250000
input text "5"
microsleep 250000
tapnswipe /dev/input/event1 tap 627 1139
microsleep 250000
input text "5"
microsleep 250000
tapnswipe /dev/input/event1 tap 627 1139
microsleep 250000
input text "5"
microsleep 750000

# Complete 2nd Matrix
tapnswipe /dev/input/event1 tap 627 1139
microsleep 250000
input text "5"
microsleep 250000
tapnswipe /dev/input/event1 tap 627 1139
microsleep 250000
input text "5"
microsleep 250000
tapnswipe /dev/input/event1 tap 627 1139
microsleep 250000
input text "5"
microsleep 250000
tapnswipe /dev/input/event1 tap 627 1139
microsleep 250000
input text "5"
microsleep 250000
tapnswipe /dev/input/event1 tap 627 1139
microsleep 250000
input text "5"
microsleep 250000
tapnswipe /dev/input/event1 tap 627 1139
microsleep 250000
input text "5"
microsleep 250000
tapnswipe /dev/input/event1 tap 627 1139
microsleep 250000
input text "5"
microsleep 250000
tapnswipe /dev/input/event1 tap 627 1139
microsleep 250000
input text "5"
microsleep 250000
tapnswipe /dev/input/event1 tap 627 1139
microsleep 250000
input text "5"
microsleep 750000

# Click On Calculate
tapnswipe /dev/input/event1 tap 627 1139
microsleep 2000000
tapnswipe /dev/input/event1 tap 333 700
microsleep 2000000

# Click Region to show opened Applications
tapnswipe /dev/input/event1 tap 553 1250
microsleep 2000000

# swipe from one point to another (duration in milliseconds) : shut down application
tapnswipe /dev/input/event1 swipe 225 1030 587 1030 1000
microsleep 3000000

# begin exit.
{{{timing}}}
am force-stop $PACKAGE
pm clear $PACKAGE