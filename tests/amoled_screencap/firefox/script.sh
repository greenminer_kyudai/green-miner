#
# White on Black screencaps
#	Loads a page and takes a screencap
#
# Copyright (c) 2013 Jed Barlow, Kent Rasmussen, Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Wait for Wattlog
microsleep 10000000

# Load App
{{{timing}}}
am start -n {{APP}}/.App
microsleep 12000000

page='black_on_white.html'
{{{timing}}}
# Click on Adress Bar
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
# Make sure it's clicked!
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
tapnswipe /dev/input/event1 tap 400 100

# Erase the current url
for i in 1 2 3 4 ; do
	input keyevent DEL
	input keyevent DEL
	input keyevent DEL
	input keyevent DEL
	input keyevent DEL
	tapnswipe /dev/input/event1 tap 400 100
done

microsleep 1000000
# Load Idle Page
input text 'http://pizza.cs.ualberta.ca/gm/tests/'$page
input keyevent ENTER


# Tap in 5s to remove possible data sending to firefox
microsleep 5000000
tapnswipe /dev/input/event1 tap 553 376

# wait 11s for page to load
tapnswipe /dev/input/event1 tap 0 0
microsleep 11000000

{{{timing}}}
screencap -p /sdcard/screen.png
microsleep 1000000

# "Exit" Process
{{{timing}}}
microsleep 2000000
input keyevent HOME
