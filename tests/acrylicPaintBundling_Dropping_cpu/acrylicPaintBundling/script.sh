# Acrylic Paint drawing test

# Wait for Wattlog
microsleep 10000000

# Launch App
{{{timing}}}
am start -n anupam.acrylic/.Splash
microsleep 5000000

# double enter to continue
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
## change brush size
tapnswipe /dev/input/event1 tap 686 68 100
microsleep 1000000
tapnswipe /dev/input/event1 tap 425 165 100
microsleep 1000000

tapnswipe /dev/input/event1 tap 455 657 100
microsleep 1000000
tapnswipe /dev/input/event1 tap 455 800 100


# Draw Image
{{{timing}}}
tapnswipe /dev/input/event1 swipe 320 300 420 310 200
tapnswipe /dev/input/event1 swipe 420 310 470 360 200
tapnswipe /dev/input/event1 swipe 470 360 610 370 200
tapnswipe /dev/input/event1 swipe 610 370 480 450 200
tapnswipe /dev/input/event1 swipe 480 450 260 450 200
tapnswipe /dev/input/event1 swipe 260 450 130 350 200
tapnswipe /dev/input/event1 swipe 130 350 270 350 200
tapnswipe /dev/input/event1 swipe 270 350 320 300 200
tapnswipe /dev/input/event1 swipe 260 460 120 800 200
tapnswipe /dev/input/event1 swipe 340 460 320 900 200
tapnswipe /dev/input/event1 swipe 480 460 560 700 200

### now draw some random line
tapnswipe /dev/input/event1 swipe 460 460 475 700 200
tapnswipe /dev/input/event1 swipe 440 460 455 700 200
tapnswipe /dev/input/event1 swipe 420 460 435 700 200
tapnswipe /dev/input/event1 swipe 400 460 415 700 200
tapnswipe /dev/input/event1 swipe 380 460 400 700 200

microsleep 3000000 ### so that the last drawing is finished in case of drawing

# Return home
{{{timing}}}
input keyevent HOME
microsleep 2000000

