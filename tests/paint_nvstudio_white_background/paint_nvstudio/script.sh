# Paint (by nvstudio) drawing test for white background

# Wait for Wattlog
microsleep 10000000

# Take idle measurement
{{{timing}}}
microsleep 10000000

# Launch App
{{{timing}}}
am start -n com.tieu.thien.paint/.activity.MenuActivity
microsleep 6000000

# Close ad
tapnswipe /dev/input/event1 tap 630 180
microsleep 1000000

# Create new drawing
tapnswipe /dev/input/event1 tap 360 400
tapnswipe /dev/input/event1 tap 360 340
tapnswipe /dev/input/event1 tap 360 440
microsleep 2000000

# Change background color to white
tapnswipe /dev/input/event1 tap 60 100
microsleep 500000
tapnswipe /dev/input/event1 swipe 700 830 700 430 2000 
microsleep 3000000
tapnswipe /dev/input/event1 swipe 700 830 700 430 2000
microsleep 3000000
tapnswipe /dev/input/event1 swipe 700 830 700 430 2000
microsleep 3000000
tapnswipe /dev/input/event1 tap 73 1155
microsleep 500000
tapnswipe /dev/input/event1 tap 150 1250
microsleep 2000000

# Adjust brush settings
tapnswipe /dev/input/event1 tap 60 100
microsleep 500000
# Change size of brush
tapnswipe /dev/input/event1 tap 110 580
microsleep 500000
# Change brush color
tapnswipe /dev/input/event1 swipe 700 830 700 430 2000
microsleep 3000000
tapnswipe /dev/input/event1 swipe 700 830 700 430 2000
microsleep 3000000
tapnswipe /dev/input/event1 tap 320 600
microsleep 1000000
tapnswipe /dev/input/event1 swipe 360 360 295 365 2000
microsleep 3000000
tapnswipe /dev/input/event1 tap 380 920
microsleep 1000000
tapnswipe /dev/input/event1 tap 150 1250
microsleep 2000000

# Draw image
{{{timing}}}
tapnswipe /dev/input/event1 swipe 320 300 420 310 1000
microsleep 1000000
tapnswipe /dev/input/event1 swipe 420 310 470 360 1000
microsleep 1000000
tapnswipe /dev/input/event1 swipe 470 360 610 370 1000
microsleep 1000000
tapnswipe /dev/input/event1 swipe 610 370 480 450 1000
microsleep 1000000
tapnswipe /dev/input/event1 swipe 480 450 260 450 1000
microsleep 1000000
tapnswipe /dev/input/event1 swipe 260 450 130 350 1000
microsleep 1000000
tapnswipe /dev/input/event1 swipe 130 350 270 350 1000
microsleep 1000000
tapnswipe /dev/input/event1 swipe 270 350 320 300 1000
microsleep 1000000
tapnswipe /dev/input/event1 swipe 260 460 120 800 1000
microsleep 1000000
tapnswipe /dev/input/event1 swipe 370 460 320 900 1000
microsleep 1000000
tapnswipe /dev/input/event1 swipe 480 460 560 700 1000
microsleep 2000000

# Take screencap of image
#screencap -p /sdcard/saucer.png

# Let app idle
{{{timing}}}
microsleep 20000000

# Clear image
{{{timing}}}
tapnswipe /dev/input/event1 tap 660 100
microsleep 500000
tapnswipe /dev/input/event1 tap 500 380
microsleep 500000
tapnswipe /dev/input/event1 tap 510 670
microsleep 4000000

# Return home
{{{timing}}}
input keyevent HOME
