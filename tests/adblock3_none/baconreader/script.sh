#
# Adblock Test
#	Loads an application (Baconreader / com.onelouder.baconreader)
#	by OneLouder Apps and idles for 2 minutes.
#
# Copyright (c) 2013 Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundationeither version 2 of the Licenseor
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If notsee <http://www.gnu.org/licenses/>.
#

# Wait for Wattlog
microsleep 10000000

# Load App
{{{timing}}}
am start -n com.onelouder.baconreader/com.onelouder.baconreader.FrontPage
microsleep 7500000

# Stuff is new, that's nice.
tapnswipe /dev/input/event1 tap 350 934

# Idle for 2 minutes
microsleep 120000000

# "Exit" Process
{{{timing}}}
microsleep 2000000
tapnswipe /dev/input/event1 tap 339 1240
