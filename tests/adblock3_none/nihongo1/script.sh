#
# Adblock Test
#	Loads an application (JAPANESE 1 (JLPT N5) / net.qpen.android.nihongo1)
#	by Qpen.NET and idles for 2 minutes.
#
# Copyright (c) 2013 Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundationeither version 2 of the Licenseor
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If notsee <http://www.gnu.org/licenses/>.
#

# Wait for Wattlog
microsleep 10000000

# Load App
{{{timing}}}
am start -n net.qpen.android.nihongo1/net.qpen.android.nihongo.BrowserActivity
microsleep 7500000

# No, I don't want to install some sketchy keyboard
tapnswipe /dev/input/event1 tap 535 738

# Idle for 2 minutes
microsleep 120000000

# "Exit" Process
{{{timing}}}
microsleep 2000000
tapnswipe /dev/input/event1 tap 339 1240
