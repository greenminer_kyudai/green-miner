# WeChat instant messaging test for 15 messages
# Test will sucessfully run only on device D

# Wait for Wattlog
microsleep 10000000

# Take idle measurement
{{{timing}}}
microsleep 10000000

# Launch app 
{{{timing}}}
am start -n com.tencent.mm/.ui.LauncherUI
microsleep 15000000

# Close popup
tapnswipe /dev/input/event1 tap 225 770
microsleep 10000000

# Login
tapnswipe /dev/input/event1 tap 170 1080
microsleep 10000000

# Click Region
tapnswipe /dev/input/event1 tap 620 220
microsleep 3000000

# Search for region
tapnswipe /dev/input/event1 tap 670 100
microsleep 1000000

input text 'Canada'
microsleep 1000000

# Select country
tapnswipe /dev/input/event1 tap 140 270
microsleep 1000000

# Clear number
tapnswipe /dev/input/event1 tap 650 325
microsleep 1000000

# Only phone D will login and send messages
SERIAL={{{serial}}}
if [ "$SERIAL" = "01498B1C0100F00B" ]; then

	# Enter phone number
	input text '7802930217'
	microsleep 1000000

	# Click password
	tapnswipe /dev/input/event1 tap 330 420
	microsleep 1000000

	# Enter password
	input text 'password'
	microsleep 1000000

	# Click login
	tapnswipe /dev/input/event1 tap 360 570
	microsleep 25000000

	# Execute this code when logging in for the first time on a device
	# Confirm identity 
	#tapnswipe /dev/input/event1 tap 500 700
	#microsleep 5000000
	# Verify identity by clicking
	#tapnswipe /dev/input/event1 tap 350 540
	#microsleep 5000000

	# Close popup
	tapnswipe /dev/input/event1 tap 230 810
	microsleep 25000000

	# Move through some sort of preview/review thing
	tapnswipe /dev/input/event1 tap 405 1150
	microsleep 2000000
	tapnswipe /dev/input/event1 tap 405 1150
	microsleep 2000000
	tapnswipe /dev/input/event1 tap 405 1150
	microsleep 2000000
	tapnswipe /dev/input/event1 tap 365 945
	microsleep 5000000
	
	# View contacts
	tapnswipe /dev/input/event1 tap 265 1130
	microsleep 3000000
	
	# Click tim tam
	tapnswipe /dev/input/event1 tap 360 820 
	microsleep 2000000

	# Click "Message"
	tapnswipe /dev/input/event1 tap 360 440
	microsleep 3000000

	# Take screencap before sending messages
	screencap -p /sdcard/kik_before_sending_messages.png

	microsleep 2000000

	# Click text box
	tapnswipe /dev/input/event1 tap 200 1136
	microsleep 1000000
	
	# Send some messages to tim tam
	{{{timing}}}
	# Message 1
	input text 'IM%sis%sa%stype%sof%sonline%schat%swhich%soffers%sreal-time...'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1130
	microsleep 2000000

	# Message 2 
	input text '...text%stransmission%sover%sthe%sInternet.%sShort%smsgs%sare...'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1130
	microsleep 2000000

	# Message 3
	input text '...transmitted%sbi-directionally%sbetween%stwo%sparties.'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1130
	microsleep 2000000
	
	# Message 4
	input text 'Some%sexample%stexts:'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1130
	microsleep 2000000

	# Message 5
	input text 'Omg!!!!%sAre%syou%sserious'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1130
	microsleep 2000000
	
	# Message 6
	input text 'lol'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1130
	microsleep 2000000

	# Message 7
	input text 'Okay%shaha'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1130
	microsleep 2000000

	# Message 8
	input text 'No%sworries!!!!!'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1130
	microsleep 2000000
	
	# Message 9
	input text 'I%sjust%snoticed%sthat%syour%semail%ssaid%s2%ssorry'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1130
	microsleep 2000000

	# Message 10
	input text 'false%salarm.%sDont%stell%shillary%sanything!'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1130
	microsleep 2000000
	
	# Message 11 
	input text 'Oh%sno%ssorry%sI%sthink%sI%ssent%sa%sblank%stext'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1130
	microsleep 2000000

	# Message 12
	input text 'k'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1130
	microsleep 2000000

	# Message 13
	input text 'Argh%sI%sforgot%sto%sreply%sfor%ssure%sthis%ssounds%sgreat'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1130
	microsleep 2000000

	# Message 14
	input text 'Oh%sgeez%sthat%sis%sso%strue...%sYugh'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1130
	microsleep 2000000

	# Message 15
	input text 'bye'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1130
	microsleep 2000000

	# Take screencap of sent messages
	#screencap -p /sdcard/wechat_sent_messages.png
fi
	
# Let app idle
{{{timing}}}
microsleep 20000000

# Return to home
{{{timing}}}
input keyevent HOME
