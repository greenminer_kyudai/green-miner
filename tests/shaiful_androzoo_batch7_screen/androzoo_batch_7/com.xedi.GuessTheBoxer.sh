# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.xedi.GuessTheBoxer/com.xedi.GuessTheBoxer.activity.MainActivity
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 4000000
input keyevent ENTER
microsleep 4000000
input keyevent 61
microsleep 4000000
input keyevent ENTER
microsleep 4000000
input keyevent ENTER
microsleep 4000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 4000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 4000000
###### tap ##########
tapnswipe /dev/input/event1 tap 365 870
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1150 20 1150
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 635 121
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 442 943
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 20 20 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 404 787
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 413 208
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 670 748
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 145 621
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 615 590
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 438 402
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 609 990
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 187 270
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 375 1018
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 129 694
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1150 20 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 123 1055
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 209 362
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME