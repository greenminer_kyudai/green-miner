# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n app.cashkings/app.cashkings.Splash
microsleep 10000000
{{{timing}}}
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 57 815
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 546 985
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 692 461
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 272 927
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 528 883
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 395 730
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 20 695 20
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 20 695 1150
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 500 247
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 68 298
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 329 390
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 640 747
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 20 20 1150
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 394 854
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 220 81
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 56 363
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 598 36
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 259 1095
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 123 657
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME