# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.jbapps.contact.theme.pinkleopard/com.jbapps.gocontact.theme.mytheme.SplashScreen
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 4000000
input keyevent ENTER
microsleep 4000000
input keyevent 61
microsleep 4000000
input keyevent ENTER
microsleep 4000000
input keyevent ENTER
microsleep 4000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 4000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 4000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 525 738
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1150 20 1150
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 408 211
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 255 58
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 338 205
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 662 825
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 223 830
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME