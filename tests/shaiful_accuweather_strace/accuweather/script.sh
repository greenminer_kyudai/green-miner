# accuweather current weather test for Edmonton, Alberta, Canada

# Wait for Wattlog
microsleep 10000000

# Take idle measurement
{{{timing}}}
microsleep 10000000

# Load App
{{{timing}}}
am start -n com.accuweather.android/.LauncherActivity
microsleep 13000000

# Navigate to the current weather for Edmonton

# Agree to terms of use
tapnswipe /dev/input/event1 tap 550 1100 
microsleep 4000000

# Pass through tour
tapnswipe /dev/input/event1 tap 360 1060
microsleep 5000000
tapnswipe /dev/input/event1 tap 350 1060
microsleep 5000000
# Decline quick setup 
tapnswipe /dev/input/event1 tap 200 1070
microsleep 3000000

# Search for Edmonton
input text 'Edmonton,%sAlberta,%sCanada'
microsleep 4000000

# Click Edmonton
tapnswipe /dev/input/event1 tap 305 730
microsleep 1000000

# Click search
tapnswipe /dev/input/event1 tap 605 630
microsleep 30000000

# Checkout the current weather/let app idle 
{{{timing}}}
microsleep 10000000

# Take a screencap of current weather
#screencap -p /sdcard/accuweather.png
microsleep 10000000

# Return home
{{{timing}}}
input keyevent HOME
