import libgreenminer, subprocess

class Test(libgreenminer.AndroidTest):
    def before(self, run):
        run.phone.shell('pm uninstall org.cocos2d.test')
        run.phone.adb('logcat -c')

        # install junit tests apk
        path = "/home/pi/green-star/tests/cocos2d-junit-tests"
        newList = run.phone.install_apk(path + "/junit_test/cocos2d-tests.apk") 

        subprocess.call(["adb", "push", path + "/suites/", "/sdcard/suites/"])

    def after(self, run):
        # uninstall test apk        
        run.phone.shell('am force-stop org.cocos2d.test')
        run.phone.shell("pm uninstall org.cocos2d.test")
        
        run.phone.shell('echo Logcat: >> /sdcard/testlog')
        run.phone.adb("logcat -d >> /sdcard/testlog")
        
        out_path = run.wattlog_file + '_testlog'                    
        run.phone.adb("pull /sdcard/testlog " + out_path)        

        run.phone.shell("rm /sdcard/testlog")
        run.phone.shell("rm -rf /sdcard/suites")



