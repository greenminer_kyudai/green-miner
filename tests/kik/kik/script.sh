# Kik instant messaging test for 15 messages

# Wait for Wattlog
microsleep 10000000

# Take idle measurement
{{{timing}}}
microsleep 10000000

# Lauch app 
{{{timing}}} 
am start -n kik.android/.chat.activity.IntroActivity
microsleep 14000000

# Click login button
tapnswipe /dev/input/event1 tap 360 960
microsleep 1000000

# Each phone must be tested with its' own kik account
# (kik only allows one account to be active on one device at a time)
SERIAL={{{serial}}}
case "$SERIAL" in

	"014E0F501001101C")
	# phone A: login A:
	# Enter username
	input text 'alannabanana666'
	;;

	"0149AE240F01D012")
	# phone B: login B:
	# Enter username
	input text 'chattycat666'
	;;

	"0149B2E80501B00A")
	# phone C: login C:
        # Enter username
        input text 'dumbdumb666'
	;;

	"01498B1C0100F00B")
	# phone D: login D:
	# Enter username
        input text 'lizardperson666'
	;;
esac

microsleep 1000000

# Click password box
tapnswipe /dev/input/event1 tap 330 330
microsleep 1000000

# Enter password
input text 'password'
microsleep 1000000

# Press login button 
tapnswipe /dev/input/event1 tap 360 560
microsleep 21000000

# close popup 1
tapnswipe /dev/input/event1 tap 230 770
microsleep 2500000

# close popup 2
tapnswipe /dev/input/event1 tap 500 750
microsleep 2500000

# Click search icon
tapnswipe /dev/input/event1 tap 630 1100
microsleep 9000000

# Take screencap of contacts
screencap -p /sdcard/kik_contacts.png

microsleep 1000000

# Click to chat with Tim Tam 
tapnswipe /dev/input/event1 tap 360 650
microsleep 3000000

# Take screencap before sending messages
#screencap -p /sdcard/kik_before_sending_messages.png

microsleep 2000000

# Send some msgs to Tim Tam
{{{timing}}}
# Message 1
input text 'IM%sis%sa%stype%sof%sonline%schat%swhich%soffers%sreal-time...'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1140
microsleep 2000000

# Message 2
input text '...text%stransmission%sover%sthe%sInternet.%sShort%smsgs%sare...'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1140
microsleep 2000000

# Message 3
input text '...transmitted%sbi-directionally%sbetween%stwo%sparties.'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1140
microsleep 2000000

# Message 4
input text 'Some%sexample%stexts:'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1140
microsleep 2000000

# Message 5
input text 'Omg!!!!%sAre%syou%sserious'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1140
microsleep 2000000

# Message 6
input text 'lol'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1140
microsleep 2000000

# Message 7
input text 'Okay%shaha'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1140
microsleep 2000000

# Message 8
input text 'No%sworries!!!!!'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1140
microsleep 2000000

# Message 9
input text 'I%sjust%snoticed%sthat%syour%semail%ssaid%s2%ssorry'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1140
microsleep 2000000

# Message 10
input text 'false%salarm.%sDont%stell%shillary%sanything!'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1140
microsleep 2000000

# Message 11
input text 'Oh%sno%ssorry%sI%sthink%sI%ssent%sa%sblank%stext'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1140
microsleep 2000000

# Message 12
input text 'k'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1140
microsleep 2000000

# Message 13
input text 'Argh%sI%sforgot%sto%sreply%sfor%ssure%sthis%ssounds%sgreat'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1140
microsleep 2000000

# Message 14
input text 'Oh%sgeez%sthat%sis%sso%strue...%sYugh'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1140
microsleep 2000000

# Message 15
input text 'bye'
microsleep 2000000
# Send
tapnswipe /dev/input/event1 tap 670 1140
microsleep 2000000

# Take screencap of sent messages
#screencap -p /sdcard/kik_sent_messages.png

# Let app idle
{{{timing}}}
microsleep 20000000

# Return to home
{{{timing}}}
input keyevent HOME
