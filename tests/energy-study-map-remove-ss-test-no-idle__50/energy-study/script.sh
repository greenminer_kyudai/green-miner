# Wait for wattlog
microsleep 10000000
 
# Just setup and teardown overhead
{{{timing}}}
sh /sdcard/suites/suite_setup_and_teardown.sh >> /sdcard/testlog 2>&1 

# Remove from HashMap
{{{timing}}}
sh /sdcard/suites/suite_hm_rem.sh >> /sdcard/testlog 2>&1  

# Remove from HashedMap
{{{timing}}}
sh /sdcard/suites/suite_hashedm_rem.sh >> /sdcard/testlog 2>&1 

# Remove from LinkedHashMap
{{{timing}}}
sh /sdcard/suites/suite_lhm_rem.sh >> /sdcard/testlog 2>&1 

# Remove from LinkedMap
{{{timing}}}
sh /sdcard/suites/suite_lm_rem.sh >> /sdcard/testlog 2>&1 

# Remove from TreeMap
{{{timing}}}
sh /sdcard/suites/suite_tm_rem.sh >> /sdcard/testlog 2>&1 

# idle time 
{{{timing}}}

