# Wait for Wattlog
microsleep 20000000

# Load App
{{{timing}}}
am start -n net.fred.feedex/.activity.{{{main}}}
microsleep 20000000

# Add feed
{{{timing}}}
tapnswipe /dev/input/event1 tap 680 100
microsleep 2000000
tapnswipe /dev/input/event1 tap 500 290
microsleep 2000000
tapnswipe /dev/input/event1 tap 444 100
microsleep 2000000
tapnswipe /dev/input/event1 tap 360 730
microsleep 2000000
tapnswipe /dev/input/event1 tap 74 700
microsleep 2000000
tapnswipe /dev/input/event1 tap 530 1135
microsleep 2000000
tapnswipe /dev/input/event1 tap 100 100
microsleep 2000000
tapnswipe /dev/input/event1 tap 580 100
microsleep 8000000
tapnswipe /dev/input/event1 tap 350 200
microsleep 8000000



## Read feed 1
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 250
microsleep 4000000
tapnswipe /dev/input/event1 swipe 380 800 380 500 3000
microsleep 2000000
tapnswipe /dev/input/event1 tap 98 110
microsleep 2000000
 
            

## Read feed 2
{{{timing}}}
tapnswipe /dev/input/event1 tap 338 448
microsleep 4000000
tapnswipe /dev/input/event1 swipe 380 800 380 500 3000
microsleep 2000000
tapnswipe /dev/input/event1 tap 98 110
microsleep 2000000
 

# "Exit" Process
{{{timing}}}
microsleep 2000000
tapnswipe /dev/input/event1 tap 339 1240
