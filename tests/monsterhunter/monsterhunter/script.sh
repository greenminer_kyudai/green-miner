# Application idle state for 5 seconds.
PACKAGE="com.daviancorp.android.monsterhunter3udatabase"
ACTIVITY="com.daviancorp.android.ui.general.HomeActivity"

# Wait for Wattlog
microsleep 10000000

# Launch app ("Launch app" entry in partition_info.csv file)
{{{timing}}}
am start -n $PACKAGE/$ACTIVITY
microsleep 10000000

# Interaction ("interaction" entry in partition_info.csv file)
{{{timing}}}

# Click Region named Weapon
tapnswipe /dev/input/event1 tap 540 634
microsleep 2000000

# Click On Icon
tapnswipe /dev/input/event1 tap 170 300
microsleep 2000000

# Click On First weapon
tapnswipe /dev/input/event1 tap 242 293
microsleep 2000000

# Click On Plus button
tapnswipe /dev/input/event1 tap 561 94
microsleep 2000000

# Click To select My Wishlist
tapnswipe /dev/input/event1 tap 619 562
microsleep 2000000

# Click On OK
tapnswipe /dev/input/event1 tap 519 833
microsleep 4000000

# Stopping app and cleaning cache ("Exit" entry in partition_info.csv file)
{{{timing}}}
am force-stop $PACKAGE
pm clear $PACKAGE