#
# mvc-energy test
#	
# Copyright (c) 2015 Shaiful Chowdhury
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundationeither version 2 of the Licenseor
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If notsee <http://www.gnu.org/licenses/>.
#

# Wait for Wattlog
microsleep 10000000

# Load App, delete and write url
{{{timing}}}

am start -n com.example.android.architecture.blueprints.todo.mock/com.example.android.architecture.blueprints.todoapp.tasks.TasksActivity

microsleep 3000000

#### add todo 1#################
{{{timing}}}

tapnswipe /dev/input/event1 tap 612 1057
microsleep 1000000
input text 'grocery'
tapnswipe /dev/input/event1 tap 165 376
microsleep 1000000
input text 'ksskdffdkffafjldsfjlasdllfjsdlkj'
microsleep 1000000
tapnswipe /dev/input/event1 tap 616 1072
microsleep 5000000

#### add todo 2#################
{{{timing}}}

tapnswipe /dev/input/event1 tap 612 1057
microsleep 1000000
input text 'millwoods'
tapnswipe /dev/input/event1 tap 165 376
microsleep 1000000
input text 'ksskdffdkffafjldsfjlasdllfjsdlkj'
microsleep 1000000
tapnswipe /dev/input/event1 tap 616 1072
microsleep 5000000

#### add todo 3#################
{{{timing}}}

tapnswipe /dev/input/event1 tap 612 1057
microsleep 1000000
input text 'cricketpractice'
tapnswipe /dev/input/event1 tap 165 376
microsleep 1000000
input text 'ksskdffdkffafjldsfjlasdllfjsdlkj'
microsleep 1000000
tapnswipe /dev/input/event1 tap 616 1072
microsleep 5000000


#### add todo 4#################
{{{timing}}}

tapnswipe /dev/input/event1 tap 612 1057
microsleep 1000000
input text 'familytime'
tapnswipe /dev/input/event1 tap 165 376
microsleep 1000000
input text 'ksskdffdkffafjldsfjlasdllfjsdlkj'
microsleep 1000000
tapnswipe /dev/input/event1 tap 616 1072
microsleep 5000000

#### background1
{{{timing}}}
input keyevent HOME
microsleep 1000000
am start -n com.example.android.architecture.blueprints.todo.mock/com.example.android.architecture.blueprints.todoapp.tasks.TasksActivity
microsleep 3000000
################

#### complete---incomplete
{{{timing}}}
tapnswipe /dev/input/event1 tap 81 334
microsleep 2000000
tapnswipe /dev/input/event1 tap 81 334
microsleep 2000000
tapnswipe /dev/input/event1 tap 81 334
microsleep 2000000
tapnswipe /dev/input/event1 tap 81 334
microsleep 2000000

### delete 
{{{timing}}}
tapnswipe /dev/input/event1 tap 368 334
microsleep 1000000
tapnswipe /dev/input/event1 tap 617 114
microsleep 3000000
tapnswipe /dev/input/event1 tap 368 334
microsleep 1000000
tapnswipe /dev/input/event1 tap 617 114
microsleep 3000000

#### background 2
{{{timing}}}
input keyevent HOME
microsleep 1000000
am start -n com.example.android.architecture.blueprints.todo.mock/com.example.android.architecture.blueprints.todoapp.tasks.TasksActivity
microsleep 3000000


# "Exit" Process
{{{timing}}}
input keyevent HOME
microsleep 2000000
