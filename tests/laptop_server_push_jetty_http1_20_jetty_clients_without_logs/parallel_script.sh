#!/bin/bash

LOGFILE=/tmp/parallel_script_jetty_server.log
CLIENTLOGFILE=/home/intel/log_files/jetty_client_jetty_server_http1/jetty_client_requests_20_jetty_server.log

#echo "$(date "+%m%d%Y %T") : Script Started. Going to sleep for 60 seconds" >> $LOGFILE 2>&1
#Sleep for 40 seconds.
sleep 70

#echo "$(date "+%m%d%Y %T") : 60 Seconds Sleep over. Starting parallel command" >> $LOGFILE 2>&1

#seq 50 | parallel -j10 -n0 --joblog /home/intel/workspace/http.client/target/joblog  java -jar /home/intel/workspace/http.client/target/httpasynchronousclient.jar /home/intel/workspace/http.client/target/all_urls.txt  > /dev/null 2>&1

#seq 10 | parallel -j1 -n0 java -jar ./httpasynchronousclient.jar ./all_urls.txt  > /dev/null 2>&1

#seq 10 | parallel -j4 -n0 --joblog /tmp/joblog java -jar /home/pi/green-star/tests/server_push_h2o_http1_10_clients/httpasynchronousclient.jar /home/pi/green-star/tests/server_push_h2o_http1_10_clients/all_urls.txt >> $CLIENTLOGFILE 2>&1 & echo $! > /tmp/httpclient.pid

##h2load -H'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/48.0.2564.82 Chrome/48.0.2564.82 Safari/537.36' -H'Accept-Encoding: gzip, deflate, sdch' -H'Accept-Language: en-US,en;q=0.8' -H'Cache-Control: no_cache' -H'Connection: keep-alive' -H'Host: 10.13.11.34:8080' -n2400 -c60 --h1 -m1 -i /tmp/imp_files/40_urls.txt >> $CLIENTLOGFILE 2>&1 & echo $! > /tmp/httpclient.pid

#h2load -H'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/48.0.2564.82 Chrome/48.0.2564.82 Safari/537.36' -H'Accept-Encoding: gzip, deflate, sdch' -H'Accept-Language: en-US,en;q=0.8' -H'Cache-Control: no_cache' -H'Connection: keep-alive' -H'Host: 10.13.11.34:9090' -n4800 -c120 --h1 -m1 -i /tmp/imp_files/40_urls_jetty.txt >> $CLIENTLOGFILE 2>&1 & echo $! > /tmp/httpclient.pid

java -jar /home/intel/imp_files/multithreadedhttpclient.jar /home/intel/imp_files/40_urls_jetty_server_local.txt 20 1>>$CLIENTLOGFILE 2>/dev/null & echo $! > /tmp/httpclient.pid
