#
# SilentZone Power Test 
# Copyright (c) 2013 Kent Rasmussen, Riley Dawson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundationeither version 2 of the Licenseor
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If notsee <http://www.gnu.org/licenses/>.
#

## Wait for Wattlog
microsleep 10000000

## Load, Setup App
{{{timing}}}
am start -n ca.silentzone.silentzone/.MainActivity
microsleep 12000000

#Enable Location Services
tapnswipe /dev/input/event1 tap 521 743
microsleep 2500000

# Toggle
tapnswipe /dev/input/event1 tap 492 265
microsleep 2500000

# Go back
tapnswipe /dev/input/event1 tap 154 1231
microsleep 2500000

# Hit OK
tapnswipe /dev/input/event1 tap 203 744
microsleep 2500000

#Go through setup BS
#Tap top right corner for setting up default zone
tapnswipe /dev/input/event1 tap 660 105
microsleep 2500000

#Tap Settings
tapnswipe /dev/input/event1 tap 660 189
microsleep 2500000

#Tap Back
tapnswipe /dev/input/event1 tap 145 1216
microsleep 2500000

# Tap Plus sign
tapnswipe /dev/input/event1 tap 549 96
microsleep 2500000

# Input name
input text "Test"
microsleep 2500000

# tap pick location
tapnswipe /dev/input/event1 tap  321 310
microsleep 2500000

#Tap done (bottom of screen)
tapnswipe /dev/input/event1 tap  359 1100
microsleep 2500000
# Tap done
tapnswipe /dev/input/event1 tap  359 1100
microsleep 2500000

# Tap pause 
tapnswipe /dev/input/event1 tap  330 100
microsleep 2500000
# Tap Play
tapnswipe /dev/input/event1 tap  330 100
microsleep 2500000
# Tap refresh
tapnswipe /dev/input/event1 tap  440 100
microsleep 2500000
# Done!

# Go Home & Wait for 10 Minutes
tapnswipe /dev/input/event1 tap 339 1240
{{{timing}}}
microsleep 600000000

# Disable Location Settings
{{{timing}}}
am start -S com.android.settings/.Settings\$LocationSettingsActivity
microsleep 2500000
#Disable Location Services
tapnswipe /dev/input/event1 tap 492 265
microsleep 1000000

## Quit
{{{timing}}}
microsleep 2000000
tapnswipe /dev/input/event1 tap 339 1240
