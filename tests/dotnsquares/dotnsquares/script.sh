PACKAGE="com.endeepak.dotsnsquares"
ACTIVITY="com.endeepak.dotsnsquares.MainMenuActivity"

# Wait for Wattlog
microsleep 10000000

# Launch app ("Launch app" entry in partition_info.csv file)
{{{timing}}}
am start -n $PACKAGE/$ACTIVITY
microsleep 10000000

# Interaction ("interaction" entry in partition_info.csv file)
{{{timing}}}

# Click Region named "play"
tapnswipe /dev/input/event1 tap 372 469
microsleep 1000000

# swipe from one point to another (duration in milliseconds) : join 2 dots
tapnswipe /dev/input/event1 swipe 136 447 271 450 1000
microsleep 5000000

# begin exit.
{{{timing}}}
# Stopping app and cleaning cache
am force-stop $PACKAGE
pm clear $PACKAGE
