logcat com.log.logapplication:D \*:S > /sdcard/logcat.txt &
PID=$!
# Wait for wattlog
microsleep 10000000

# Suite nolog
{{{timing}}}
sh /sdcard/suites/suite_nolog.sh
microsleep 2000000

# idle time 
kill $PID
{{{timing}}}
