
import libgreenminer, subprocess

class Test(libgreenminer.AndroidTest):
    def before(self, run):
        # install junit tests apk
        path = "/home/pi/green-star/tests/storyhoard_73_method_suites_part1"
        run.phone.install_apk(path + "/junit_test/storyhoardTest-debug.apk")

        # push scripts onto phone
        subprocess.call(["adb", "push", path + "/suites/", "/sdcard/suites/"])

    def after(self, run):
        # uninstall test apk
        run.phone.shell("pm uninstall ca.ualberta.cmput301f13t13.storyhoard.test")

        # rm suites that were pushed onto phone
        subprocess.call("adb shell rm -rf /sdcard/suites/", shell=True)
