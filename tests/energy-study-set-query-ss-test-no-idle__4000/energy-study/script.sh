# Wait for wattlog
microsleep 10000000
 
# Just setup and teardown overhead
{{{timing}}}
sh /sdcard/suites/suite_setup_and_teardown.sh >> /sdcard/testlog 2>&1  

# Insertion in HashMap
{{{timing}}}
sh /sdcard/suites/suite_hs_query.sh >> /sdcard/testlog 2>&1    

# Insertion in TroveMap
{{{timing}}}
sh /sdcard/suites/suite_mapbackeds_query.sh >> /sdcard/testlog 2>&1  

# Insertion in TreeMap
{{{timing}}}
sh /sdcard/suites/suite_lhs_query.sh >> /sdcard/testlog 2>&1  

# Insertion in LinkedHashMap
{{{timing}}}
sh /sdcard/suites/suite_listordereds_query.sh >> /sdcard/testlog 2>&1  

# Insertion in LinkedMap
{{{timing}}}
sh /sdcard/suites/suite_ts_query.sh >> /sdcard/testlog 2>&1  


# idle time 
{{{timing}}}

