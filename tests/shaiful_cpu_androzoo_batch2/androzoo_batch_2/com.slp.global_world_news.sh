# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.slp.global_world_news/com.slp.global_world_news.activity.SplashActivity
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent 61
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 2000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 315 230
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 159 216
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 500 695
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 385 624
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 20 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 695 1000
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 695 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 644 879
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 172 604
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 20 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 188 1018
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 433 1010
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 169 418
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 675 104
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 617 742
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 31 710
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 501 565
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 129 900
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 695 1000
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME
