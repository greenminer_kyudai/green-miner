# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.klimbo.wordsearchadventure/com.klimbo.wordsearchadventure.game.IntroActivity
microsleep 10000000
{{{timing}}}
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 545 1025
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 695 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 272 640
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 308 675
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 565 1003
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 20 1000
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 385 648
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 610 538
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 663 670
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 427 244
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 81 236
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 120 978
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 209 323
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 267 893
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 150 882
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 676 983
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME
