# Wait for Wattlog
microsleep 10000000

# Load App
{{{timing}}}
am start -n org.mozilla.firefox/.App
microsleep 8000000


{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "google.com"
tapnswipe /dev/input/event1 tap 667 100
microsleep 5000000
tapnswipe /dev/input/event1 tap 400 500 50
microsleep 5000000
input text "washington%spost"
microsleep 5000000
tapnswipe /dev/input/event1 tap 650 510 
microsleep 2000000
tapnswipe /dev/input/event1 tap 400 400 50
microsleep 2500000
tapnswipe /dev/input/event1 swipe 400 800 400 200 300
microsleep 2500000
tapnswipe /dev/input/event1 swipe 400 800 400 200 300
microsleep 2500000
tapnswipe /dev/input/event1 swipe 400 200 400 800 300
microsleep 2500000
tapnswipe /dev/input/event1 swipe 400 200 400 800 300
microsleep 2500000
tapnswipe /dev/input/event1 tap 400 700 50
microsleep 30000000
tapnswipe /dev/input/event1 swipe 400 1000 400 200 50
microsleep 3000000
{{{timing}}}
microsleep 10000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "www.accuweather.com"
microsleep 4000000
tapnswipe /dev/input/event1 tap 667 100
microsleep 5000000
tapnswipe /dev/input/event1 tap 200 400 50
microsleep 5000000
tapnswipe /dev/input/event1 tap 400 400 50
microsleep 5000000
tapnswipe /dev/input/event1 tap 667 250
{{{timing}}}
microsleep 5000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "wikipedia.com"
microsleep 3000000
tapnswipe /dev/input/event1 tap 667 119
microsleep 3000000
tapnswipe /dev/input/event1 tap 530 333
microsleep 10000000

{{{timing}}}
microsleep 5000000

# "Exit" Process
{{{timing}}}
microsleep 2500000
{{{timing}}}
tapnswipe /dev/input/event1 tap 339 1240

