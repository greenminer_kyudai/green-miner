#
# badBrickBreaker test
#	
# Copyright (c) 2016 Boyan Peychoff
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundationeither version 2 of the Licenseor
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If notsee <http://www.gnu.org/licenses/>.
#

#waiting for wattlog?
microsleep 1200000

{{{timing}}}

am start -n com.bpeychof.example/.MainActivity


microsleep 500000


tapnswipe /dev/input/event1 tap 429 268

microsleep  12000000

tapnswipe /dev/input/event1 tap 100 300

microsleep 7000000

tapnswipe /dev/input/event1 tap 585 322

microsleep 12000000

tapnswipe /dev/input/event1 tap 420 288

microsleep 2000000

tapnswipe /dev/input/event1 tap 222 299

microsleep 10000000

tapnswipe /dev/input/event1 tap 640 303

microsleep 4000000

# "Exit" Process
{{{timing}}}
microsleep 2000000
input keyevent HOME

