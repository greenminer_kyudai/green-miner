# Wait for wattlog
microsleep 10000000

# Just setup and teardown overhead
{{{timing}}}
sh /sdcard/suites/suite_setup_and_teardown.sh >> /sdcard/testlog 2>&1 


# Insert String at beginning of ArrayList
{{{timing}}}
sh /sdcard/suites/suite_al_ins_beg.sh >> /sdcard/testlog 2>&1 


# Insert String at beginning of LinkedList
{{{timing}}}
sh /sdcard/suites/suite_ll_ins_beg.sh >> /sdcard/testlog 2>&1 


# Insert String at beginning of TreeList
{{{timing}}}
sh /sdcard/suites/suite_tl_ins_beg.sh >> /sdcard/testlog 2>&1


# Insert String at middle of ArrayList
{{{timing}}}
sh /sdcard/suites/suite_al_ins_mid.sh >> /sdcard/testlog 2>&1 


# Insert String at middle of LinkedList
{{{timing}}}
sh /sdcard/suites/suite_ll_ins_mid.sh >> /sdcard/testlog 2>&1 


# Insert String at middle of TreeList
{{{timing}}}
sh /sdcard/suites/suite_tl_ins_mid.sh >> /sdcard/testlog 2>&1 


# Insert String at end of ArrayList
{{{timing}}}
sh /sdcard/suites/suite_al_ins_end.sh >> /sdcard/testlog 2>&1 


# Insert String at end of LinkedList
{{{timing}}}
sh /sdcard/suites/suite_ll_ins_end.sh >> /sdcard/testlog 2>&1 


# Insert String at end of TreeList
{{{timing}}}
sh /sdcard/suites/suite_tl_ins_end.sh >> /sdcard/testlog 2>&1 
 

# idle time 
{{{timing}}}

