# Wait for wattlog
microsleep 5000000
 
# Just setup and teardown overhead
{{{timing}}}
sh /sdcard/suites/suite_setup_and_teardown.sh >> /sdcard/testlog 2>&1 

# Remove from HashSet
{{{timing}}}
sh /sdcard/suites/suite_hs_rem.sh >> /sdcard/testlog 2>&1 

# Remove from MapBackedSet
{{{timing}}}
sh /sdcard/suites/suite_mapbackeds_rem.sh >> /sdcard/testlog 2>&1 

# Remove from LinkedHashSet
{{{timing}}}
sh /sdcard/suites/suite_lhs_rem.sh >> /sdcard/testlog 2>&1 

# Remove from ListOrderedSet
{{{timing}}}
sh /sdcard/suites/suite_listordereds_rem.sh >> /sdcard/testlog 2>&1  

# Remove from TreeSet
{{{timing}}}
sh /sdcard/suites/suite_ts_rem.sh >> /sdcard/testlog 2>&1  


# idle time 
{{{timing}}}

