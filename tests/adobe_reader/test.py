import libgreenminer

class Test(libgreenminer.AndroidTest):
    
    def before(self, run):

        # Set Screen Timeout
        self.timeout = run.phone.shell('settings get system screen_off_timeout').strip()
        run.phone.shell('settings put system screen_off_timeout 1800000').strip()

	# Remove PDF from sdcard
        run.phone.shell("rm /sdcard/ahmedfeministkilljoy.pdf")

        # Put PDF on sdcard
        run.phone.adb("push tests/adobe_reader/ahmedfeministkilljoy.pdf /sdcard/ahmedfeminstkilljoy.pdf")

    def after(self, run):

        # Reset Screen Timeout
        run.phone.shell('settings put system screen_off_timeout ' + self.timeout)

        # Remove PDF from sdcard
        run.phone.shell("rm /sdcard/ahmedfeministkilljoy.pdf")
