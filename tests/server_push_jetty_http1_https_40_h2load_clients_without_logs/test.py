import libgreenminer,subprocess, logging

logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
fileHandler = logging.FileHandler('/tmp/python_jetty.log')
fileHandler.setFormatter(logFormatter)
logger.addHandler(fileHandler)


class Test(libgreenminer.RaspberryPiTest):

    def before(self,run):
#        logger.info('Before function started:')
        # Kill httpclient pid process if running. Delete the pid file.
        subprocess.call("nohup /home/pi/imp_files/kill_pid_file.sh >/dev/null 2>&1 &",shell =True)
        # Start client
#        logger.info('Call to parallel_script placed:')
        subprocess.call("nohup /home/pi/green-star/tests/server_push_jetty_http1_https_40_h2load_clients_without_logs/parallel_script.sh >/dev/null 2>&1 &",shell =True)
#        logger.info('Exiting from before method')

    def after(self,run):

#        logger.info('After function started:')
        # Kill httpclient pid process if still running. Delete the pid file.
 #       logger.info('After function, placed call to kill_pid_file')
        subprocess.call("nohup /home/pi/imp_files/kill_pid_file.sh >/dev/null 2>&1 &",shell =True)
  #      logger.info('Exiting After function')

 
