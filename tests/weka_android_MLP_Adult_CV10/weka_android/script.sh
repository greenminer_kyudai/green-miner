logcat > /sdcard/logcat.txt &
PID=$!
# Wait for wattlog (10s)
microsleep 10000000

# Load app
{{{timing}}}
am start -n weka.mlp_neuroph/.MainActivity
# Choose MLP
tapnswipe /dev/input/event1 tap 350 910
microsleep 2000000

# Read data
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 120
microsleep 38000000

# Train CV
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 360
microsleep 1000000000
microsleep 1000000000
microsleep 800000000

# Validate CV
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 600
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000

# Idle time
{{{timing}}}
kill $PID
