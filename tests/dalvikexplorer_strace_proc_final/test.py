import libgreenminer,time,subprocess
class Test(libgreenminer.AndroidTest):
	def before(self, run):
		# Set Screen Timeout
		self.timeout = run.phone.shell('settings get system screen_off_timeout').strip()
		run.phone.shell('settings put system screen_off_timeout 1250000').strip()
		run.phone.adb("push traceTools/strc_gen_sequential.sh /sdcard/strc_gen_sequential.sh")
		run.phone.adb("push traceTools/proc_logger.sh /sdcard/proc_logger.sh")
		run.phone.shell("su -c 'cp /sdcard/strc_gen_sequential.sh /data/local/' ")
		run.phone.shell("su -c 'cp /sdcard/proc_logger.sh /data/local/' ")
		run.phone.adb("push traceTools/strace /sdcard/")
		run.phone.shell("su -c 'cp /sdcard/strace /data/local/' ")
		run.phone.shell("su -c 'rm /sdcard/strace' ")
		run.phone.shell("su -c 'rm /sdcard/strc_gen_sequential.sh' ")
		run.phone.shell("su -c 'rm /sdcard/proc_logger.sh' ")
		run.phone.shell("su -c 'rm /data/local/proc.txt' ")
		run.phone.shell("su -c 'rm /data/local/trcs.txt' ")
		run.phone.shell("su -c 'chmod 0777 /data/local/strc_gen_sequential.sh'")
		run.phone.shell("su -c 'chmod 0777 /data/local/proc_logger.sh'")
		run.phone.shell("su -c 'chmod 0777 /data/local/strace'")
		subprocess.call(" adb shell busybox nohup su bash -c \"(sh /data/local/strc_gen_sequential.sh org.jessies.dalvikexplorer & sh /data/local/proc_logger.sh org.jessies.dalvikexplorer &) \" &", shell=True)
                #time.sleep(2)




	def after(self, run):
		# Reset Screen Timeout
		run.phone.shell('settings put system screen_off_timeout ' + self.timeout)
		

	def before_upload(self,run):
		#Put the files into the folder before uploading
		run.phone.adb("pull /data/local/trcs.txt "+run.wattlog_file+"_strace.txt")
		run.phone.adb("pull /data/local/proc.txt "+run.wattlog_file+"_proc.txt")
		run.phone.shell("su -c 'rm /data/local/trcs.txt' ")
		run.phone.shell("su -c 'rm /data/local/proc.txt' ")
