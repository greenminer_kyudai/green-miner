# Wait for Wattlog
{{{timing}}}
microsleep 10000000

# Load App
#am start -n com.myopicmobile.textwarrior.android/.TextWarriorApplication
tapnswipe /dev/input/event1 tap 350 1100
microsleep 2000000
tapnswipe /dev/input/event1 tap 220 245
microsleep 6000000

# Hit Text Area and write note
tapnswipe /dev/input/event1 tap 430 470
microsleep 1000000
input text "Williamsburg%siPhone%strust%sfund%scornhole%spork%sbelly%sreprehenderit%sphoto%sbooth,%sforage%sfixie%sintelligentsia%swolf.%sbanjo%sminim%skeffiyeh%sid%sfarm-to-table%svinyl.%sBeard%stote%sbag%ssriracha%slaboris,%stempor%sTerry%sRichardson%sorganic%sbespoke%sput%sa%sbird%son%sit%sBrooklyn%sassumenda.%sPour-over%sadipisicing%sVice%ssemiotics.%sChillwave%sNeutra%sBrooklyn%sethical%sart%sparty.%sBiodiesel%senim%syou%sprobably%shaven't%sheard%sof%sthem,%ssquid%saccusamus%s8-bit%swolf%sculpa%sfarm-to-table%svegan%splaceat%spop-up.%sAesthetic%slomo%shashtag,%sBanksy%sexcepteur%sasymmetrical%seu%sOdd%sFuture."
microsleep 20000000

#Idle Complete Note
microsleep 20000000

#Save Note
tapnswipe /dev/input/event1 tap 100 1230
microsleep 1000000
tapnswipe /dev/input/event1 tap 690 1230
microsleep 1000000
tapnswipe /dev/input/event1 tap 600 980
microsleep 1000000
tapnswipe /dev/input/event1 tap 300 245
microsleep 1000000
input text "wartext"
microsleep 1000000
tapnswipe /dev/input/event1 tap 670 250
microsleep 1000000

#Exit & Wait
tapnswipe /dev/input/event1 tap 350 1230
microsleep 1000000

