#
# Note taking text
# 
# Open a note-taking program, create, and save, a note.
#
# Copyright (c) 2013 Kent Rasmussen, Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

start data >>

# Wait for Wattlog
UserWait(10000)

# Load App
RunCmd(sh /mnt/sdcard/timing.sh Load App & Login)
LaunchActivity({{APP}}, {{APP}}.TextWarriorApplication)
RotateScreen(0, 1)
UserWait(10000)


RunCmd(sh /mnt/sdcard/timing.sh Compose and Save Note)

# select text field
Tap(350, 400)
UserWait(2000)

# enter title text
# enter body text
{{#input_text}}Williamsburg iPhone trust fund cornhole pork belly reprehenderit photo booth, forage fixie intelligentsia wolf. banjo minim keffiyeh id farm-to-table vinyl. Beard tote bag sriracha laboris, tempor Terry Richardson organic bespoke put a bird on it Brooklyn assumenda. Pour-over adipisicing Vice semiotics. Chillwave Neutra Brooklyn ethical art party. Biodiesel enim you probably haven't heard of them, squid accusamus 8-bit wolf culpa farm-to-table vegan placeat pop-up. Aesthetic lomo hashtag, Banksy excepteur asymmetrical eu Odd Future.{{/input_text}}


DispatchPress(KEYCODE_MENU)
UserWait(1000)

# click more.. button
Tap(600, 1100)
UserWait(1000)

# click save as.. button
Tap(350, 450)
UserWait(1000)

# select file name field
Tap(350, 260)
UserWait(1000)

# enter title text
{{#input_text}}awesome test note{{/input_text}}
UserWait(1000)

# click save button
Tap(650, 250)
UserWait(1000)


RunCmd(sh /mnt/sdcard/timing.sh Delete Note)
# This sections is for consitency with Evernote.
# We can delete 920editor notes via adb shell after the
# test is run
UserWait(1000)


# all done now

# "Exit" Process
RunCmd(sh /mnt/sdcard/timing.sh Exit & Wait)
UserWait(2000)
DispatchPress(KEYCODE_HOME)

quit
