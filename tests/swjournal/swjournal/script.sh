# Application idle state for 5 seconds.
PACKAGE="com.gk.simpleworkoutjournal"
ACTIVITY="com.gk.swjmain.MainMenu"

# Wait for Wattlog
microsleep 10000000

# Launch app ("Launch app" entry in partition_info.csv file)
{{{timing}}}
am start -n $PACKAGE/$ACTIVITY
microsleep 10000000

# Interaction ("interaction" entry in partition_info.csv file)
{{{timing}}}

# Click Region named "Start Workout"
tapnswipe /dev/input/event1 tap 326 237
microsleep 1000000

# Click On the text zone
tapnswipe /dev/input/event1 tap 337 1150
microsleep 1000000

# Add text "lagartijas"
input text 'lagartijas'
microsleep 1000000

# Click On the plus button
tapnswipe /dev/input/event1 tap 650 700
microsleep 1000000

# Click On the play button
tapnswipe /dev/input/event1 tap 77 700
microsleep 1000000

# Select 1st case
tapnswipe /dev/input/event1 tap 252 700
microsleep 1000000

# Add text "2"
input text '2'
microsleep 1000000

# Select 2nd case
tapnswipe /dev/input/event1 tap 485 700
microsleep 1000000

# Add text "3"
input text '3'
microsleep 1000000

# Click On the plus button
tapnswipe /dev/input/event1 tap 650 680
microsleep 3000000

# Stopping app and cleaning cache ("Exit" entry in partition_info.csv file)
{{{timing}}}
am force-stop $PACKAGE
pm clear $PACKAGE