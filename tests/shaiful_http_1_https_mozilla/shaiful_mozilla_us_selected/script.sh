#
# Mozilla nightly to test http 1 performance
#	
#
# Copyright (c) 2015 Shaiful Alam Chowdhury
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundationeither version 2 of the Licenseor
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If notsee <http://www.gnu.org/licenses/>.
#

# Wait for Wattlog
microsleep 10000000

# Load App
{{{timing}}}
am start -n {{APP}}/.App
microsleep 10000000


#### setting http 1 and writing web address 
{{{timing}}}

tapnswipe /dev/input/event1 tap 82 96   
microsleep 1000000
input keyevent DEL
tapnswipe /dev/input/event1 tap 82 96   
microsleep 1000000
input keyevent DEL

tapnswipe /dev/input/event1 tap 82 96   

input text "about:config"
input keyevent ENTER

microsleep 4000000
tapnswipe /dev/input/event1 tap 419 199   
microsleep 1000000
input text "spdy.enabled"
microsleep 2000000

### network.http.spdy.enabled false
tapnswipe /dev/input/event1 tap 617 389
microsleep 1000000
tapnswipe /dev/input/event1 tap 617 389
microsleep 2000000

#### network.http.spdy.enabled.http2 false


tapnswipe /dev/input/event1 tap 646 790
microsleep 1000000
tapnswipe /dev/input/event1 tap 646 790
microsleep 2000000


#### network.http.spdy.enabled.http2draft false

tapnswipe /dev/input/event1 tap 637 996
microsleep 1000000
tapnswipe /dev/input/event1 tap 637 996
microsleep 1000000


tapnswipe /dev/input/event1 tap 82 96   
input text "https://pizza.cs.ualberta.ca:1801"


#############this time is to stable the device
{{{timing}}}
microsleep 120000000

##### load the whole page #################
{{{timing}}}
input keyevent ENTER
microsleep 20000000

##### hit home button##########
{{{timing}}}
#microsleep 2000000
tapnswipe /dev/input/event1 tap 339 1240
