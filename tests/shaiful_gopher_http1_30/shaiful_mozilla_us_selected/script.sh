#
# Mozilla nightly to test http 1 performance
#	
#
# Copyright (c) 2015 Shaiful Alam Chowdhury
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundationeither version 2 of the Licenseor
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If notsee <http://www.gnu.org/licenses/>.
#

# Wait for Wattlog

microsleep 10000000

# Load App
{{{timing}}}
am start -n {{APP}}/.App
microsleep 10000000


#### writing web address 
{{{timing}}}

tapnswipe /dev/input/event1 tap 82 96   
microsleep 1000000
input keyevent DEL
tapnswipe /dev/input/event1 tap 82 96   
microsleep 1000000
input keyevent DEL


input text "http://http2.golang.org/gophertiles?latency=30"

#### device stable time ############
{{{timing}}}
microsleep 60000000

##### load tiles with http/1 for 30 ms latenc #################
{{{timing}}}
input keyevent ENTER
microsleep 10000000

###################

##### hit home button##########
{{{timing}}}
tapnswipe /dev/input/event1 tap 339 1240


