import libgreenminer, time, subprocess

class Test(libgreenminer.AndroidTest):
    def before(self, run):
        # push scripts onto phone
        path = "/home/pi/green-star/tests/weka_android_datasets"

        run.phone.shell("su -c 'rm /sdcard/logcat.txt' ")
        run.phone.adb('logcat -c')

        # push data onto the phone
        subprocess.call(["adb", "push", path + "/pgsqla_reliability_smallerdataset.arff", "/sdcard/"])

        # Measure CPU time
        run.phone.adb("push traceCPU/sysInfo_before.sh /sdcard/sysInfo_before.sh")
        run.phone.shell("su -c 'cp /sdcard/sysInfo_before.sh /data/local/' ")
        run.phone.adb("push traceCPU/sysInfo_after.sh /sdcard/sysInfo_after.sh")
        run.phone.shell("su -c 'cp /sdcard/sysInfo_after.sh /data/local/' ")

        run.phone.shell("su -c 'rm /sdcard/sysInfo_before.sh' ")
        run.phone.shell("su -c 'rm /sdcard/sysInfo_after.sh' ")

        run.phone.shell("su -c 'touch /data/local/sysInfo_before.txt' ")
        run.phone.shell("su -c 'touch /data/local/cpuLoad_before.txt' ")
        run.phone.shell("su -c 'touch /data/local/sysInfo_after.txt' ")
        run.phone.shell("su -c 'touch /data/local/cpuLoad_after.txt' ")

        run.phone.shell("su -c 'touch /data/local/processInfo.txt' ")

        subprocess.call(" adb shell sh /data/local/sysInfo_before.sh com.example.andrea.wekaandroidport", shell=True)

    def after(self, run):
        # rm data that was pushed onto phone
        run.phone.shell("su -c 'rm /sdcard/pgsqla_reliability_smallerdataset.arff' ")

        subprocess.call(" adb shell sh /data/local/sysInfo_after.sh com.example.andrea.wekaandroidport", shell=True)

    def before_upload(self,run):
        # Put the files into the folder before uploading
        run.phone.adb("pull /data/local/sysInfo_before.txt "+run.wattlog_file+"_sysinfo_before.txt")
        run.phone.adb("pull /data/local/sysInfo_after.txt "+run.wattlog_file+"_sysinfo_after.txt")
        run.phone.adb("pull /data/local/cpuLoad_before.txt "+run.wattlog_file+"_cpuLoad_before.txt")
        run.phone.adb("pull /data/local/cpuLoad_after.txt "+run.wattlog_file+"_cpuLoad_after.txt")
        run.phone.adb("pull /data/local/processInfo.txt "+run.wattlog_file+"_processinfo.txt")

        run.phone.adb("pull /sdcard/logcat.txt "+run.wattlog_file+"_logcat.txt")
        run.phone.shell("su -c 'rm /sdcard/logcat.txt' ")

        ## delete the files
        run.phone.shell("su -c 'rm /data/local/sysInfo_before.txt' ")
        run.phone.shell("su -c 'rm /data/local/cpuLoad_before.txt' ")
        run.phone.shell("su -c 'rm /data/local/sysInfo_after.txt' ")
        run.phone.shell("su -c 'rm /data/local/cpuLoad_after.txt' ")
        run.phone.shell("su -c 'rm /data/local/processInfo.txt' ")

        run.phone.shell("su -c 'rm /data/local/sysInfo_before.sh' ")
        run.phone.shell("su -c 'rm /data/local/sysInfo_after.sh' ")
