#
# Mozilla nightly to test http 1 performance
#	
#
# Copyright (c) 2015 Shaiful Alam Chowdhury
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundationeither version 2 of the Licenseor
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If notsee <http://www.gnu.org/licenses/>.
#

# Wait for Wattlog

microsleep 10000000

# Load App
{{{timing}}}
am start -n org.blockinger.game/org.blockinger.game.activities.MainActivity
microsleep 10000000


#### stable 
{{{timing}}}
microsleep 20000000

##### idle #################
{{{timing}}}
microsleep 60000000

###################

##### hit home button##########
{{{timing}}}
tapnswipe /dev/input/event1 tap 339 1240


