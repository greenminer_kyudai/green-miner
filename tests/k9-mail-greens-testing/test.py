import libgreenminer, subprocess

class Test(libgreenminer.AndroidTest):
    def before(self, run):
        run.phone.shell('pm uninstall com.fsck.k9.tests')
        run.phone.adb('logcat -c')

        # install junit tests apk
        path = "/home/pi/green-star/tests/k9-mail-greens-testing"
        newList = run.phone.install_apk(path + "/junit_test/k9mail-tests.apk") 

        subprocess.call(["adb", "push", path + "/suites/", "/sdcard/suites/"])

    def after(self, run):
        # uninstall test apk        
        run.phone.shell('am force-stop com.fsck.k9.tests')
        run.phone.shell("pm uninstall com.fsck.k9.tests")
        
        run.phone.shell('echo Logcat: >> /sdcard/testlog')
        run.phone.adb("logcat -d >> /sdcard/testlog")
        
        out_path = run.wattlog_file + '_testlog'                    
        run.phone.adb("pull /sdcard/testlog " + out_path)        

        run.phone.shell("rm /sdcard/testlog")
        run.phone.shell("rm -rf /sdcard/suites")



