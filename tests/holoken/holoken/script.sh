# Application idle state for 5 seconds.
PACKAGE="com.tortuca.holoken"
ACTIVITY="com.tortuca.holoken.MainActivity"

# Wait for Wattlog
microsleep 10000000

# Launch app ("Launch app" entry in partition_info.csv file)
{{{timing}}}
am start -n $PACKAGE/$ACTIVITY
microsleep 10000000

# Interaction ("interaction" entry in partition_info.csv file)
{{{timing}}}

# Click "OK"
tapnswipe /dev/input/event1 tap 512 1120
microsleep 2000000

# Click "+"
tapnswipe /dev/input/event1 tap 90 1163
microsleep 2000000

# Click "Easy"
tapnswipe /dev/input/event1 tap 153 437
microsleep 2000000

# Click to open applications list
tapnswipe /dev/input/event1 tap 556 1242
microsleep 2000000

# Close the app
tapnswipe /dev/input/event1 swipe 225 1030 587 1030 1000
microsleep 3000000

# begin exit.
{{{timing}}}
am force-stop $PACKAGE
pm clear $PACKAGE