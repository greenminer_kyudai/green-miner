# FB Reader e-reader test 

# Wait for Wattlog
microsleep 10000000

# Take idle measurement
{{{timing}}}
microsleep 10000000

# Load App
{{{timing}}}
am start -n org.geometerplus.zlibrary.ui.android/org.geometerplus.android.fbreader.FBReader
microsleep 10000000

# Close message
tapnswipe /dev/input/event1 tap 360 660
microsleep 2000000

# Find epub
# Enter library
tapnswipe /dev/input/event1 tap 450 105
microsleep 1500000
# Click file tree
tapnswipe /dev/input/event1 tap 330 990
microsleep 1500000
# Click /storage/sdcard/Books
tapnswipe /dev/input/event1 tap 370 220
microsleep 1500000
# Click the epub
tapnswipe /dev/input/event1 tap 350 220
microsleep 3000000
# Click to read 
tapnswipe /dev/input/event1 tap 450 100
microsleep 6000000

# Change background color to white
tapnswipe /dev/input/event1 tap 360 660
microsleep 2000000
# Click menu
tapnswipe /dev/input/event1 tap 670 100
microsleep 3000000
# Click settings
tapnswipe /dev/input/event1 tap 500 690
microsleep 5000000
# Click colors and wallpapers
tapnswipe /dev/input/event1 tap 360 850
microsleep 2000000
# Click background
tapnswipe /dev/input/event1 tap 360 220
microsleep 2000000
# Click solid color
tapnswipe /dev/input/event1 tap 360 560
microsleep 1000000
# Default is white so click okay
tapnswipe /dev/input/event1 tap 530 920
microsleep 1000000
# Go back to ebook
tapnswipe /dev/input/event1 tap 160 1240
microsleep 1000000
tapnswipe /dev/input/event1 tap 160 1240
microsleep 6000000

### Let app idle
{{{timing}}}
microsleep 20000000

# Search for word
{{{timing}}}
# Exit full screen
tapnswipe /dev/input/event1 tap 360 660
microsleep 1000000
# Click Menu
tapnswipe /dev/input/event1 tap 660 100
microsleep 1000000
# Clear search
tapnswipe /dev/input/event1 tap 500 490
microsleep 1000000
input text 'attired'
microsleep 1000000
# Displays search result
input keyevent ENTER
microsleep 4000000
# Display result in context
tapnswipe /dev/input/event1 tap 50 650
microsleep 4000000

# Prepare to read
{{{timing}}}
# Exit fullscreen mode
tapnswipe /dev/input/event1 tap 360 660
microsleep 2000000
# Click to next page
tapnswipe /dev/input/event1 tap 650 650
microsleep 2000000
# Enter fullscreen mode
tapnswipe /dev/input/event1 tap 360 660
microsleep 4000000

# Read for a while
{{{timing}}}
# The Stag at the Pool
microsleep 30000000
# The Stag at the Pool cont.
tapnswipe /dev/input/event1 tap 650 650
microsleep 10000000
# The Fox and the Mask
tapnswipe /dev/input/event1 tap 650 650
microsleep 20000000
# The Bear and the Fox
tapnswipe /dev/input/event1 tap 650 650
microsleep 23000000

# Return home
{{{timing}}}
input keyevent HOME
