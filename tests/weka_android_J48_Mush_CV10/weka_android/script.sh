logcat > /sdcard/logcat.txt &
PID=$!
# Wait for wattlog (10s)
microsleep 10000000

# Load app
{{{timing}}}
am start -n com.example.andrea.wekaandroidport/.MainActivity
# Choose NB
tapnswipe /dev/input/event1 tap 250 210
microsleep 2000000

# Read data
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 120
microsleep 3000000

# Train CV10
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 360
microsleep 1000000

# Validate CV10
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 600
microsleep 10000000

# Idle time
kill $PID
{{{timing}}}
