# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.notriddle.budget/com.notriddle.budget.EnvelopesActivity
microsleep 8000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent 61
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
tapnswipe /dev/input/event1 tap 200 400 300
microsleep 2000000
tapnswipe /dev/input/event1 tap 300 500 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 146 732 300
microsleep 2000000
###### tap menu ##########
tapnswipe /dev/input/event1 tap 682 1236 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 158 443 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 256 745 300
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 535 701 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 576 267 1000
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 532 306 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 178 595 300
microsleep 2000000
###### tap menu ##########
tapnswipe /dev/input/event1 tap 682 1236 300
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME