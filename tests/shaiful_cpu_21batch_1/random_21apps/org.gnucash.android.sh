# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n org.gnucash.android/org.gnucash.android.ui.account.AccountsActivity
microsleep 8000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent 61
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
tapnswipe /dev/input/event1 tap 200 400 300
microsleep 2000000
tapnswipe /dev/input/event1 tap 300 500 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 548 811 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 545 658 300
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 615 1100 100 100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 537 304 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 100 100 615 100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 600 242 300
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME