logcat > /sdcard/logcat.txt &
PID=$!
# Wait for wattlog (10s)
microsleep 10000000

# Load app
{{{timing}}}
am start -n weka.mlp_neuroph/.MainActivity
# Choose MLP
tapnswipe /dev/input/event1 tap 450 910
microsleep 2000000

# Read data
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 120
microsleep 5000000

# Train CV
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 360
microsleep 160000000

# Validate CV
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 600
microsleep 1300000000

# Idle time
{{{timing}}}
kill $PID
