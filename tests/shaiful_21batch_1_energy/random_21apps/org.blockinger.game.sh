# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n org.blockinger.game/org.blockinger.game.activities.MainActivity
microsleep 8000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent 61
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
tapnswipe /dev/input/event1 tap 200 400 300
microsleep 2000000
tapnswipe /dev/input/event1 tap 300 500 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 364 793 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 559 499 1000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 615 100 100 1100 2000
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 530 152 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 428 457 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 442 593 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 615 1100 100 100 2000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 502 750 1000
microsleep 2000000
###### tap menu ##########
tapnswipe /dev/input/event1 tap 682 1236 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 271 378 1000
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME