# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n net.progval.android.andquote/net.progval.android.andquote.MainActivity
microsleep 8000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent 61
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
tapnswipe /dev/input/event1 tap 200 400 300
microsleep 2000000
tapnswipe /dev/input/event1 tap 300 500 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 522 300 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 593 655 1000
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 100 100 100 1100 2000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 291 171 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 409 929 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 579 141 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 100 100 615 1100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 266 742 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 100 100 100 1100 2000
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 483 609 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 615 100 615 1100 2000
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 542 206 1000
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME