# Black on White image viewer test
# Copyright (c) 2013 Kent Rasmussen, Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Wait for Wattlog
microsleep 10000000

# Load App
{{{timing}}}

am start -t image/png -n com.google.android.gallery3d/com.android.gallery3d.app.Gallery -d file:///sdcard/ff.png -a android.intent.action.VIEW
microsleep 19000000

# tap once to go full screen
tapnswipe /dev/input/event1 tap 500 500
microsleep 19000000

{{{timing}}}
microsleep 120000000

# "Exit" Process
{{{timing}}}
microsleep 2000000
input keyevent BACK 
microsleep 2000000
input keyevent HOME
