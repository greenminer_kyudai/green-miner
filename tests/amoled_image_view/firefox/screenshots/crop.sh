#!/bin/sh

for img in `ls *.png`; do
	rev=`echo "$img" | sed -e 's/\.png$//'`
	convert -crop 720x1130+0+50 $img  "cropped$img"
	rm $img
	mv "cropped$img" $img
done
