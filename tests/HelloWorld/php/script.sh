# Wait for Wattlog
~/bin/microsleep 20000000

{{{ timing }}}

# Run app
php5-cli hello.php > /tmp/hello_php

{{{ timing }}}

# Cleanup
rm -rf /tmp/hello_php