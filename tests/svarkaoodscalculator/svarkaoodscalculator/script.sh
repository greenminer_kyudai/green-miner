# Application idle state for 5 seconds.
PACKAGE="eu.veldsoft.svarka.odds.calculator"
ACTIVITY="eu.veldsoft.svarka.odds.calculator.SplashActivity"

# Wait for Wattlog
microsleep 10000000

# Launch app ("Launch app" entry in partition_info.csv file)
{{{timing}}}
am start -n $PACKAGE/$ACTIVITY
microsleep 10000000

# Interaction ("interaction" entry in partition_info.csv file)
{{{timing}}}
microsleep 5000000

# begin exit.
{{{timing}}}
am force-stop $PACKAGE
pm clear $PACKAGE