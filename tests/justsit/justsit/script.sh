# Application idle state for 5 seconds.
PACKAGE="com.brocktice.JustSit"
ACTIVITY="com.brocktice.JustSit.JustSit"

# Wait for Wattlog
microsleep 10000000

# Launch app ("Launch app" entry in partition_info.csv file)
{{{timing}}}
am start -n $PACKAGE/$ACTIVITY
microsleep 10000000

# Interaction ("interaction" entry in partition_info.csv file)
{{{timing}}}

# Press button
tapnswipe /dev/input/event1 tap 272 1159
microsleep 2000000

# Click Region to show opened Applications
tapnswipe /dev/input/event1 tap 553 1250
microsleep 2000000

# swipe from one point to another (duration in milliseconds) : shut down application
tapnswipe /dev/input/event1 swipe 225 1030 587 1030 1000
microsleep 3000000

# begin exit.
{{{timing}}}
am force-stop $PACKAGE
pm clear $PACKAGE