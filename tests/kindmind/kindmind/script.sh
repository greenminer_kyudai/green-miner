# Application idle state for 5 seconds.
PACKAGE="com.sunyata.kindmind"
ACTIVITY="Main.MainActivityC"

# Wait for Wattlog
microsleep 10000000

# Launch app ("Launch app" entry in partition_info.csv file)
{{{timing}}}
am start -n $PACKAGE/.$ACTIVITY
microsleep 10000000

# Interaction ("interaction" entry in partition_info.csv file)
{{{timing}}}

# Click Region named Anger
tapnswipe /dev/input/event1 tap 202 335
microsleep 5000000

# Stopping app and cleaning cache ("Exit" entry in partition_info.csv file)
{{{timing}}}
am force-stop $PACKAGE
pm clear $PACKAGE