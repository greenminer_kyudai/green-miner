# Wait for wattlog (10s)
microsleep 10000000

# Load app
{{{timing}}}
am start -n com.example.andrea.wekaandroidport/.MainActivity
# Choose IBk
tapnswipe /dev/input/event1 tap 50 770
microsleep 2000000

# Read data
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 120
microsleep 18000000

# Train 50/50
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 840
microsleep 1000000

# Validate 50/50
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 1080
microsleep 73000000

# Idle time
{{{timing}}}
