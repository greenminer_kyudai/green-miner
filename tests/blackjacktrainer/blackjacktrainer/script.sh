# Application idle state for 5 seconds.
PACKAGE="eu.domob.bjtrainer"
ACTIVITY="eu.domob.bjtrainer.StrategyTrainer"

# Wait for Wattlog
microsleep 10000000

# Launch app ("Launch app" entry in partition_info.csv file)
{{{timing}}}
am start -n $PACKAGE/$ACTIVITY
microsleep 10000000

# Interaction ("interaction" entry in partition_info.csv file)
{{{timing}}}

# Click "..."
tapnswipe /dev/input/event1 tap 642 1252
microsleep 2000000

# Click "Preferences"
tapnswipe /dev/input/event1 tap 535 995
microsleep 2000000

# Click to open applications list
tapnswipe /dev/input/event1 tap 556 1242
microsleep 2000000

# Close the app
tapnswipe /dev/input/event1 swipe 225 1030 587 1030 1000
microsleep 3000000

# begin exit.
{{{timing}}}
am force-stop $PACKAGE
pm clear $PACKAGE