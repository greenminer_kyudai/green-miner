import libgreenminer,time,subprocess

OLD_VERSIONS = set(["FeedEx-1.0.8","FeedEx-1.0.9","FeedEx-1.1.0","FeedEx-1.1.1","FeedEx-1.1.2","FeedEx-1.1.3","FeedEx-1.1.4","FeedEx-1.2.0","FeedEx-1.2.1","FeedEx-1.2.2","FeedEx-1.2.3","FeedEx-1.2.4","FeedEx-1.2.5","FeedEx-1.2.6","FeedEx-1.2.7","FeedEx-1.2.8","FeedEx-1.3.0","FeedEx-1.3.2","FeedEx-1.3.3","FeedEx-1.3.4"])


class Test(libgreenminer.AndroidTest):
	def before(self, run):
		# Set Screen Timeout
		self.timeout = run.phone.shell('settings get system screen_off_timeout').strip()
		run.phone.shell('settings put system screen_off_timeout 1800000').strip()
		run.phone.adb("push traceTools/strc_gen.sh /sdcard/strc_gen.sh")
		run.phone.shell("su -c 'cp /sdcard/strc_gen.sh /data/local/' ")
		run.phone.adb("push traceTools/strace /sdcard/")
		run.phone.shell("su -c 'cp /sdcard/strace /data/local/' ")
		run.phone.shell("su -c 'rm /sdcard/strace' ")
		run.phone.shell("su -c 'rm /sdcard/strc_gen.sh' ")
		run.phone.shell("su -c 'chmod 0777 /data/local/strc_gen.sh'")
		run.phone.shell("su -c 'chmod 0777 /data/local/strace'")
		subprocess.call(" adb shell busybox nohup su bash -c \"(sh /data/local/strc_gen.sh net.fred.feedex &) \" &", shell=True)
		#time.sleep(2)

	def after(self, run):
		# Reset Screen Timeout
		#run.phone.adb("pull /data/local/trc.txt /home/pi/green-star/" + run.version + ".txt")
		#run.phone.shell("rm /data/local/trc.txt")
		run.phone.shell('settings put system screen_off_timeout ' + self.timeout)

	def before_upload(self,run):
		run.phone.adb("pull /data/local/trc.txt "+run.wattlog_file+"_strace.txt")
		run.phone.shell("su -c 'rm /data/local/trc.txt' ")

	def with_run(self, run):
		self.version = run.version

	def get_template_data(self, phone, packages):
		activity = "HomeActivity"
		if self.version in OLD_VERSIONS:
			activity = "MainActivity"
		return {"main": activity}
