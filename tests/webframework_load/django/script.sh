# Exit and leave if there is an error
set -e


# Wait for Wattlog
/home/pi/bin/microsleep 10000000

{{{timing}}}

# Change to the Django directory
cd CMPUT410-Project/Project-Django/

# Get the env
source venv/bin/activate

# Start serving django
python manage.py runserver 127.0.0.1:8000 &

# Wait for it to be ready to serve
/home/pi/bin/microsleep 10000000

{{{timing}}}

# Define the urls to test
URLS=(
    "http://localhost:8000/"
    "http://localhost:8000/animals/"
    "http://localhost:8000/animals/bird/"
    "http://localhost:8000/animals/snake/"
    "http://localhost:8000/activities/surrenders/"
    "http://localhost:8000/applications/adopt/"
)

# Go over each url
for url in ${URLS[@]}
do
    # And curl it 5 times
    for i in 1 2 3 4 5
    do
        # curl it
        curl -H "Cache-control: no-cache" -s -o /dev/null $url
    done
done

{{{timing}}}

# Kill django
kill -TERM -$$
