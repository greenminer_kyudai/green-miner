# Wait for Wattlog
/home/pi/bin/microsleep 10000000

{{{timing}}}

# Change to the Django directory
cd CMPUT410-Project/Project-Rails/

# Kill it if it is already running
if [ -f "tmp/pids/server.pid" ]; then
  kill -9 `cat tmp/pids/server.pid`

  if [ -f "tmp/pids/server.pid" ]; then
    rm tmp/pids/server.pid
  fi
fi

# Start serving django
rails server -p 8000 &

# Wait for it to be ready to serve
/home/pi/bin/microsleep 60000000

{{{timing}}}

# Define the urls to test
URLS=(
    "http://localhost:8000/"
    "http://localhost:8000/animals/"
    "http://localhost:8000/animals/bird/"
    "http://localhost:8000/animals/snake/"
    "http://localhost:8000/activities/surrenders/"
    "http://localhost:8000/applications/adopt/"
)

# Go over each url
for url in ${URLS[@]}
do
    # And curl it 5 times
    for i in 1 2 3 4 5
    do
        # curl it
        curl -s -H "Cache-control: no-cache" -o /dev/null $url
    done
done

{{{timing}}}

# Kill rails
if [ -f "tmp/pids/server.pid" ]; then
  kill -9 `cat tmp/pids/server.pid`

  if [ -f "tmp/pids/server.pid" ]; then
    rm tmp/pids/server.pid
  fi
fi
