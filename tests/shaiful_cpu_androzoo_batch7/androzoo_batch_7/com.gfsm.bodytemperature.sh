# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.gfsm.bodytemperature/com.gfsm.bodytemperature.MainActivity
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 4000000
input keyevent ENTER
microsleep 4000000
input keyevent 61
microsleep 4000000
input keyevent ENTER
microsleep 4000000
input keyevent ENTER
microsleep 4000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 4000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 4000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 322 593
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 569 811
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 20 695 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 278 960
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 159 1026
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 285 711
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 270 272
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 276 683
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 20 695 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 74 90
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 439 956
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 182 339
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 338 607
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1150 695 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 669 63
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 551 661
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 377 731
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME