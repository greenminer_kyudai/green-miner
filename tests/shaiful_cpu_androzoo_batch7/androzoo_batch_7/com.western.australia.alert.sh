# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.western.australia.alert/com.appyet.activity.SplashActivity
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 4000000
input keyevent ENTER
microsleep 4000000
input keyevent 61
microsleep 4000000
input keyevent ENTER
microsleep 4000000
input keyevent ENTER
microsleep 4000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 4000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 4000000
###### tap ##########
tapnswipe /dev/input/event1 tap 326 514
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 66 921
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 529 1098
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 51 232
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 686 668
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 508 612
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 341 930
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 623 471
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 207 786
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 257 698
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 381 439
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 20 695 1150
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 20 695 1150
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1150 695 1150
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 43 662
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 585 883
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 20 695 1150
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 256 904
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 256 111
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 483 419
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 20 695 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 74 466
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME