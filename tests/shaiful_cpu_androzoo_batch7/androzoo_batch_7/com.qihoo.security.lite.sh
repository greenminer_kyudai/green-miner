# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.qihoo.security.lite/com.qihoo.security.lite.AppEnterActivity
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 4000000
input keyevent ENTER
microsleep 4000000
input keyevent 61
microsleep 4000000
input keyevent ENTER
microsleep 4000000
input keyevent ENTER
microsleep 4000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 4000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 4000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 20 695 1150
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 72 1083
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 647 534
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 599 85
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 535 549
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 614 902
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 544 471
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 142 642
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 467 690
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 52 879
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 274 509
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 359 968
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 404 586
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 679 229
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 565 151
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 525 502
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1150 695 1150
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 571 372
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 631 425
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 20 695 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 451 706
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 172 409
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 556 345
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME