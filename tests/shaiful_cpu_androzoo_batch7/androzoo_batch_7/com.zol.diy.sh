# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.zol.diy/com.zol.diy.ui.FirstActivity
microsleep 10000000
{{{timing}}}
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1150 20 1150
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 509 376
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 287 369
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 400 589
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 687 1025
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 447 534
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 689 808
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 210 969
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 630 818
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 608 477
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 323 311
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 252 416
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 398 920
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1150 695 1150
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 683 974
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 241 1123
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 206 394
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 70 819
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 570 192
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 328 660
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 444 739
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 20 20 20
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 20 695 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 645 665
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME