# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n jp.atlas.procguide.bmb2015/jp.atlas.procguide.bmb2015.StartingActivity
microsleep 10000000
{{{timing}}}
###### tap ##########
tapnswipe /dev/input/event1 tap 119 730
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 527 766
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 555 366
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 257 972
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 393 861
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 81 562
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 326 306
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 20 695 1150
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 133 945
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1150 695 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 668 795
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1150 695 1150
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 606 503
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 177 196
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 71 643
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1150 695 1150
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 268 39
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 39 1067
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME