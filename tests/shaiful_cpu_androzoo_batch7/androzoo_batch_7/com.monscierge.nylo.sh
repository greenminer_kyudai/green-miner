# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.monscierge.nylo/com.monscierge.connect.ActivitySDK
microsleep 10000000
{{{timing}}}
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 327 283
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 644 981
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 76 767
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 208 141
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 20 20 1150
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1150 20 1150
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 427 909
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 197 430
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 242 632
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 24 1141
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME