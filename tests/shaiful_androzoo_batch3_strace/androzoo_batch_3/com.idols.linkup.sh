# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.idols.linkup/com.idols.linkup.Main
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent 61
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 2000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 229 857
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 624 182
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 192 969
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 675 882
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 393 380
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 296 666
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 169 1017
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 337 878
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 108 184
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 579 938
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 579 194
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 20 1000
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME