# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.matixsoft.velopal/com.matixsoft.sportspal.activities.StartScreen
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent 61
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 2000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 47 961
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 633 318
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 695 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 353 354
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 629 798
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 343 253
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 616 989
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME