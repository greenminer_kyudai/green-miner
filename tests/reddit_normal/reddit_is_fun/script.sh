#
# Reddit Test (Reddit Is Fun Free)
#	Loads our greenminer subreddit on redditisfun and simulates some activities
#
# Copyright (c) 2013 Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundationeither version 2 of the Licenseor
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If notsee <http://www.gnu.org/licenses/>.
#

## Wait for Wattlog
microsleep 10000000

## Load, Setup App & Login
{{{timing}}}
am start -n com.andrewshu.android.reddit/.MainActivity
microsleep 12000000
# I've read what's new
tapnswipe /dev/input/event1 tap 489 1127
microsleep 1000000
tapnswipe /dev/input/event1 tap 659 102
microsleep 1000000
tapnswipe /dev/input/event1 tap 452 300
microsleep 2500000
# Login
{{#AUTH}}
{{#reddit}}
# Username (Already highlighted)
input text {{username}}
microsleep 1000000
# Password
tapnswipe /dev/input/event1 tap 134 721
microsleep 2500000
input text {{password}}
microsleep 1000000
# Save
tapnswipe /dev/input/event1 tap 66 809
microsleep 500000
# Login
tapnswipe /dev/input/event1 tap 550 798
microsleep 10000000
{{/reddit}}
{{/AUTH}}
# Open Settings
tapnswipe /dev/input/event1 tap 659 102
microsleep 1000000
tapnswipe /dev/input/event1 tap 454 695
microsleep 1000000
# Don't Enable Backup
tapnswipe /dev/input/event1 tap 104 742
microsleep 2000000
tapnswipe /dev/input/event1 tap 324 873
microsleep 1000000
# Enable Ads
tapnswipe /dev/input/event1 tap 619 1063
microsleep 1000000

# Go home
tapnswipe /dev/input/event1 tap 339 1240
microsleep 2500000
# Kill app
su -c am kill com.andrewshu.android.reddit
microsleep 5000000

## Load & Idle
{{{timing}}}
am start -a android.intent.action.VIEW -c android.intent.category.BROWSABLE -d http://www.reddit.com/r/greenmining/ -n com.andrewshu.android.reddit/.MainActivity
microsleep 60000000

## Open the top three posts
{{{timing}}}
# Link #1
tapnswipe /dev/input/event1 tap 639 321
microsleep 20000000
tapnswipe /dev/input/event1 tap 141 1240
microsleep 2500000
# Link #2
tapnswipe /dev/input/event1 tap 639 510
microsleep 20000000
tapnswipe /dev/input/event1 tap 141 1240
microsleep 2500000
# Link #3
tapnswipe /dev/input/event1 tap 639 619
microsleep 20000000
tapnswipe /dev/input/event1 tap 141 1240
microsleep 2500000

## Swipe up and down (Peruse the subreddit)
{{{timing}}}
# Swipe down twice
tapnswipe /dev/input/event1 swipe 380 900 380 250 250
microsleep 2000000
tapnswipe /dev/input/event1 swipe 380 900 380 250 250
microsleep 7000000
# Swipe up three times
tapnswipe /dev/input/event1 swipe 380 250 380 900 250
microsleep 2000000
tapnswipe /dev/input/event1 swipe 380 250 380 900 250
microsleep 2000000
tapnswipe /dev/input/event1 swipe 380 250 380 900 250
microsleep 7000000

## Add a comment
{{{timing}}}
# Open top link
tapnswipe /dev/input/event1 tap 225 306
microsleep 500000
tapnswipe /dev/input/event1 tap 602 471
microsleep 10000000
# Make comment
tapnswipe /dev/input/event1 tap 448 110
microsleep 5000000
input text "We're%stesting%san%sapplication%sright%snow!"
microsleep 2500000
# Submit
tapnswipe /dev/input/event1 tap 639 827
microsleep 10000000

## Quit
{{{timing}}}
microsleep 2000000
tapnswipe /dev/input/event1 tap 339 1240
