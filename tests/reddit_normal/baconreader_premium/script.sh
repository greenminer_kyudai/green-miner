#
# Reddit Test (Baconreader Free)
#	Loads our greenminer subreddit on baconreader and simulates some activities
#
# Copyright (c) 2013 Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundationeither version 2 of the Licenseor
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If notsee <http://www.gnu.org/licenses/>.
#

## Wait for Wattlog
microsleep 10000000

## Load, Setup App & Login
{{{timing}}}
am start -n com.onelouder.baconreader.premium/com.onelouder.baconreader.FrontPagePhone
microsleep 12000000
# Exit first run screen
tapnswipe /dev/input/event1 tap 370 924
microsleep 1000000
# Login
{{#AUTH}}
{{#reddit}}
tapnswipe /dev/input/event1 tap 147 1143
microsleep 2500000
# Username (Already highlighted)
input text {{username}}
microsleep 1000000
# Password
tapnswipe /dev/input/event1 tap 196 652
microsleep 2500000
input text {{password}}
microsleep 1000000
# Submit
tapnswipe /dev/input/event1 tap 402 777
microsleep 10000000
{{/reddit}}
{{/AUTH}}
# Go home
tapnswipe /dev/input/event1 tap 339 1240
microsleep 2500000
# Kill app
su -c am kill com.onelouder.baconreader.premium
microsleep 5000000

## Load & Idle
{{{timing}}}
am start -a android.intent.action.VIEW -c android.intent.category.BROWSABLE -d http://www.reddit.com/r/greenmining/ -n com.onelouder.baconreader.premium/com.onelouder.baconreader.LinkDispatcher
microsleep 60000000

## Open the top three posts
{{{timing}}}
# Link #1
tapnswipe /dev/input/event1 tap 672 241
microsleep 20000000
tapnswipe /dev/input/event1 tap 141 1240
microsleep 2500000
# Link #2
tapnswipe /dev/input/event1 tap 672 447
microsleep 20000000
tapnswipe /dev/input/event1 tap 141 1240
microsleep 2500000
# Link #3
tapnswipe /dev/input/event1 tap 672 638
microsleep 20000000
tapnswipe /dev/input/event1 tap 141 1240
microsleep 2500000

## Swipe up and down (Peruse the subreddit)
{{{timing}}}
# Swipe down twice
tapnswipe /dev/input/event1 swipe 380 900 380 250 250
microsleep 2000000
tapnswipe /dev/input/event1 swipe 380 900 380 250 250
microsleep 7000000
# Swipe up three times
tapnswipe /dev/input/event1 swipe 380 250 380 900 250
microsleep 2000000
tapnswipe /dev/input/event1 swipe 380 250 380 900 250
microsleep 2000000
tapnswipe /dev/input/event1 swipe 380 250 380 900 250
microsleep 7000000

## Add a comment
{{{timing}}}
# Open top link
tapnswipe /dev/input/event1 tap 263 253
microsleep 10000000
# Make comment
tapnswipe /dev/input/event1 tap 374 1139
microsleep 5000000
input text "We're%stesting%san%sapplication%sright%snow!"
microsleep 2500000
# Submit
tapnswipe /dev/input/event1 tap 670 1126
microsleep 10000000

## Quit
{{{timing}}}
microsleep 2000000
tapnswipe /dev/input/event1 tap 339 1240
