#
# Reddit Test (Baconreader Free)
#	Loads our greenminer subreddit on baconreader and simulates some activities
#
# Copyright (c) 2013 Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundationeither version 2 of the Licenseor
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If notsee <http://www.gnu.org/licenses/>.
#

## Wait for Wattlog
microsleep 10000000

## Load, Setup App & Login
{{{timing}}}
am start -n com.android.chrome/com.google.android.apps.chrome.Main
microsleep 6000000
# Accept terms
tapnswipe /dev/input/event1 tap 355 1080
microsleep 4000000
# Go to Reddit
tapnswipe /dev/input/event1 tap 230 100
microsleep 1000000
input text "m.reddit.com"
input keyevent KEYCODE_ENTER
microsleep 10000000
# Enable New Layout
tapnswipe /dev/input/event1 tap 392 681
microsleep 6000000
# Login
{{#AUTH}}
{{#reddit}}
tapnswipe /dev/input/event1 tap 675 196
microsleep 1000000
tapnswipe /dev/input/event1 tap 625 265
microsleep 8000000
# Username (Already highlighted)
input text {{username}}
microsleep 1000000
# Password
tapnswipe /dev/input/event1 tap 143 468
input text {{password}}
microsleep 1000000
# Remember Me!
tapnswipe /dev/input/event1 tap 51 529
microsleep 1000000
# Submit
tapnswipe /dev/input/event1 tap 79 645
microsleep 20000000
{{/reddit}}
{{/AUTH}}
# Go home
tapnswipe /dev/input/event1 tap 339 1240
microsleep 2500000
# Kill app
su -c am kill com.android.chrome
microsleep 5000000

## Load & Idle
{{{timing}}}
am start -a android.intent.action.VIEW -d http://www.m.reddit.com/r/greenmining/ -n com.android.chrome/com.google.android.apps.chrome.Main
microsleep 60000000

## Open the top three posts
{{{timing}}}
# Link #1
tapnswipe /dev/input/event1 tap 230 353
microsleep 20000000
tapnswipe /dev/input/event1 tap 141 1240
microsleep 2500000
# Link #2
tapnswipe /dev/input/event1 tap 196 548
microsleep 20000000
tapnswipe /dev/input/event1 tap 141 1240
microsleep 2500000
# Link #3
tapnswipe /dev/input/event1 tap 115 714
microsleep 20000000
tapnswipe /dev/input/event1 tap 141 1240
microsleep 2500000

## Swipe up and down (Peruse the subreddit)
{{{timing}}}
# Swipe down twice
tapnswipe /dev/input/event1 swipe 380 900 380 250 250
microsleep 2000000
tapnswipe /dev/input/event1 swipe 380 900 380 250 250
microsleep 7000000
# Swipe up three times
tapnswipe /dev/input/event1 swipe 380 250 380 900 250
microsleep 2000000
tapnswipe /dev/input/event1 swipe 380 250 380 900 250
microsleep 2000000
tapnswipe /dev/input/event1 swipe 380 250 380 900 250
microsleep 7000000

## Add a comment
{{{timing}}}
# Open top link
tapnswipe /dev/input/event1 tap 674 330
microsleep 10000000
# Make comment
tapnswipe /dev/input/event1 tap 316 655
microsleep 5000000
input text "We're%stesting%san%sapplication%sright%snow!"
microsleep 2500000
# Submit
tapnswipe /dev/input/event1 tap 628 788
microsleep 10000000

## Quit
{{{timing}}}
microsleep 2000000
tapnswipe /dev/input/event1 tap 339 1240
