# Acrylic Paint drawing test

# Wait for Wattlog
microsleep 10000000

# Launch App
{{{timing}}}
am start -n org.nick.wwwjdic/.Wwwjdic
microsleep 10000000

# emualte escape to continue
tapnswipe /dev/input/event1 tap 101 906 1000
microsleep 2000000

tapnswipe /dev/input/event1 tap 61 1134 100 
microsleep 2000000

tapnswipe /dev/input/event1 tap 61 1134 1000 
microsleep 2000000


# Draw Image
{{{timing}}}

tapnswipe /dev/input/event1 swipe 184 499 416 499 1000

microsleep 1000000

tapnswipe /dev/input/event1 swipe 400 450 400 799 1000
microsleep 1000000

tapnswipe /dev/input/event1 swipe 430 760 184 799 1000
microsleep 1000000

tapnswipe /dev/input/event1 swipe 200 820 200 450 1000
microsleep 1000000


tapnswipe /dev/input/event1 tap 217 205 100
microsleep 7000000

input keyevent BACK
microsleep 2000000


tapnswipe /dev/input/event1 tap 515 200 100
microsleep 2000000


tapnswipe /dev/input/event1 swipe 184 499 416 499 1000
microsleep 1000

tapnswipe /dev/input/event1 swipe 341 300 341 799 1000
microsleep 1000


tapnswipe /dev/input/event1 tap 217 205 100
microsleep 7000000

input keyevent BACK
microsleep 2000000


# Return home
{{{timing}}}
microsleep 6000000
input keyevent HOME
