
import libgreenminer, subprocess

class Test(libgreenminer.AndroidTest):
    def before(self, run):
        # install junit tests apk
        path = "/home/pi/green-star/tests/andstatus_13_method_suites"
        run.phone.install_apk(path + "/junit_test/AndStatus-debug.apk")

        # push scripts onto phone
        subprocess.call(["adb", "push", path + "/suites/", "/sdcard/suites/"])

    def after(self, run):
        # uninstall test apk
        run.phone.shell("pm uninstall org.andstatus.app.tests")

        # rm suites that were pushed onto phone
        subprocess.call("adb shell rm -rf /sdcard/suites/", shell=True)
