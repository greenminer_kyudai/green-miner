import libgreenminer

class Test(libgreenminer.AndroidTest):
	def before(self, run):
		# Set Screen Timeout
		run.phone.shell('"settings get system screen_off_timeout > /sdcard/screen_off_timeout"').strip()
		run.phone.shell('settings put system screen_off_timeout 1800000').strip()

	def after(self, run):
		### Get Screenshots
		# Google
		run.phone.adb("pull /sdcard/screen_google.png " + run.wattlog_file + "_screen_google.png")
		run.phone.shell("rm /sdcard/screen_google.png")
		# Wikipedia
		run.phone.adb("pull /sdcard/screen_million.png " + run.wattlog_file + "_screen_million.png")
		run.phone.shell("rm /sdcard/screen_million.png")
		# Filestube
		run.phone.adb("pull /sdcard/screen_filestube.png " + run.wattlog_file + "_screen_filestube.png")
		run.phone.shell("rm /sdcard/screen_filestube.png")

		# Reset Screen Timeout
		run.phone.shell('settings put system screen_off_timeout `cat /sdcard/screen_off_timeout`')
		run.phone.shell('rm /sdcard/screen_off_timeout')
