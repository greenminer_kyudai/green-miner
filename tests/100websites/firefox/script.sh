#
# 100 Website Test
#	Loads the top 100 webpages (that aren't hidden) from https://www.quantcast.com/top-sites
#	One after another, idling for a minute on each page
#
# Copyright (c) 2013 Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundationeither version 2 of the Licenseor
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If notsee <http://www.gnu.org/licenses/>.
#

# Wait for Wattlog
microsleep 10000000

# Load App
{{{timing}}}
am start -n org.mozilla.firefox/.App
microsleep 8000000

# Don't worry, I generated this with a shell script...
# I tried doing a loop with a file, but I think it messed up with nohup.
# TODO: If someone wants to look in to this, it was something like:
#
# while read website
# do
# 	{enter url, idle, other stuff}
# done < website_list
#
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "google.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "youtube.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "facebook.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "msn.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "twitter.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "amazon.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "ebay.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "yahoo.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "microsoft.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "pinterest.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "huffingtonpost.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "live.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "wikipedia.org"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "wordpress.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "bing.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "blogger.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "linkedin.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "ask.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "blogspot.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "about.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "answers.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "adobe.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "craigslist.org"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "tumblr.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "aol.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "comcast.net"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "ehow.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "paypal.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "weather.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "godaddy.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "reddit.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "walmart.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "buzzfeed.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "whitepages.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "wikia.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "cnn.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "go.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "vimeo.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "yellowpages.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "inbox.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "imgur.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "wellsfargo.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "imdb.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "nytimes.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "yelp.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "att.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "apple.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "bleacherreport.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "webmd.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "legacy.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "chase.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "mapquest.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "nydailynews.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "ancestry.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "target.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "reference.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "tmz.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "photobucket.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "usps.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "examiner.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "wow.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "msnbc.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "chacha.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "urbandictionary.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "cnet.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "city-data.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "drudgereport.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "bizrate.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "people.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "windows.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "bankofamerica.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "mlb.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "flickr.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "tripadvisor.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "comcast.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "topix.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "booking.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "merriam-webster.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "ups.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "today.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "homedepot.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "goodreads.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "hp.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "zillow.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "sears.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "time.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "drugs.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "bestbuy.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "grindtv.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "usmagazine.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "deviantart.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "mayoclinic.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "mozilla.org"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "safeshare.tv"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "skype.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "lowes.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "babycenter.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "fixya.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "break.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "fedex.com"
microsleep 1000000
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000

# "Exit" Process
{{{timing}}}
microsleep 2000000
tapnswipe /dev/input/event1 tap 339 1240
