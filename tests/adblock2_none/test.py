import libgreenminer

class Test(libgreenminer.AndroidTest):
	def before(self, run):
		# Set Screen Timeout
		self.timeout = run.phone.shell('settings get system screen_off_timeout').strip()
		run.phone.shell('settings put system screen_off_timeout 1800000').strip()

		### Setup Hosts File
		run.phone.shell("su -c mount -o remount,rw /system")
		# Backup host file on android device
		run.phone.shell("cp /etc/hosts /sdcard/hosts_backup")
		# Push new host file
		run.phone.adb("push tests/" + self.test.name + "/hosts /sdcard/hosts")
		run.phone.shell("su -c cp /sdcard/hosts /etc/hosts")
		run.phone.shell("rm /sdcard/hosts")
		run.phone.shell("su -c mount -o remount,ro /system")

	def after(self, run):
		# Reset Screen Timeout
		run.phone.shell('settings put system screen_off_timeout ' + self.timeout)

		### Takedown Hosts File
		run.phone.shell("su -c mount -o remount,rw /system")
		# Push backed up host file
		run.phone.shell("su -c cp /sdcard/hosts_backup /etc/hosts")
		run.phone.shell("rm /sdcard/hosts_backup")
		run.phone.shell("su -c mount -o remount,ro /system")
