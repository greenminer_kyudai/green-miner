#
# Adblock Test
#	Loads the top 100 webpages (that aren't hidden) from https://www.quantcast.com/top-sites
#	One after another, idling for a minute on each page
#
# Copyright (c) 2013 Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundationeither version 2 of the Licenseor
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If notsee <http://www.gnu.org/licenses/>.
#

# Wait for Wattlog
microsleep 10000000

# Load App
{{{timing}}}
am start -n org.mozilla.firefox/.App
microsleep 12000000
# Don't send data
tapnswipe /dev/input/event1 tap 553 376
# No setup for adblock

# Don't worry, I generated this with a shell script...
# I tried doing a loop with a file, but I think it messed up with nohup.
# TODO: If someone wants to look in to this, it was something like:
#
# while read website
# do
# 	{enter url, idle, other stuff}
# done < website_list
#
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "google.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "youtube.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "facebook.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "msn.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "twitter.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "amazon.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "ebay.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "yahoo.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "microsoft.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "pinterest.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "huffingtonpost.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "live.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "wikipedia.org"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "wordpress.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "bing.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "blogger.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "linkedin.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "ask.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "blogspot.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "about.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "answers.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "adobe.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "craigslist.org"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "tumblr.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "aol.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "comcast.net"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "ehow.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "paypal.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "weather.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "godaddy.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "reddit.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "walmart.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "buzzfeed.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "whitepages.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "wikia.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "cnn.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "go.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "vimeo.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "yellowpages.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "inbox.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "imgur.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "wellsfargo.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "imdb.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "nytimes.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "yelp.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "att.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "apple.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "bleacherreport.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "webmd.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "legacy.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "chase.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "mapquest.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "nydailynews.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "ancestry.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "target.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "reference.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "tmz.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "photobucket.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "usps.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "examiner.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "wow.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "msnbc.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "chacha.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "urbandictionary.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "cnet.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "city-data.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "drudgereport.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "bizrate.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "people.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "windows.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "bankofamerica.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "mlb.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "flickr.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "tripadvisor.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "comcast.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "topix.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "booking.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "merriam-webster.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "ups.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "today.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "homedepot.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "goodreads.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "hp.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "zillow.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "sears.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "time.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "drugs.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "bestbuy.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "grindtv.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "usmagazine.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "deviantart.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "mayoclinic.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "mozilla.org"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "safeshare.tv"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "skype.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "lowes.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "babycenter.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "fixya.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "break.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "fedex.com"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000

# "Exit" Process
{{{timing}}}
microsleep 2000000
tapnswipe /dev/input/event1 tap 339 1240
