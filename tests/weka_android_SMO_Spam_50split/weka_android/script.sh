logcat TestLog:I \*:S > /sdcard/logcat.txt &
PID=$!
# Wait for wattlog (10s)
microsleep 10000000

# Load app
{{{timing}}}
am start -n com.example.andrea.wekaandroidport/.MainActivity
# Choose J48
tapnswipe /dev/input/event1 tap 550 350
microsleep 2000000

# Read data
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 120
microsleep 10000000

# Train 50/50
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 840
microsleep 6000000

# Validate 50/50
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 1080
microsleep 600000

# Idle time
kill $PID
{{{timing}}}
