#!/usr/bin/env python3
#
# Copyright (c) 2013 Alex Wilson, Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import argparse, collections, imp, json, os, subprocess
from libgreenminer import DeviceBuilder, StatusUpdater, TestBuilder, TestFile, TestRunBuilder

config = json.loads(open('config.json').read())
APK_FOLDER = os.path.expanduser(config['images_folder'])

RunTestArgs = collections.namedtuple('RunTestArgs',
                    ['test', 'app', 'version'])

def get_test(path, app, device, version):
    testFile = TestFile(path, app)
    py_file = testFile.get_py_file()
    if not py_file:
        return TestBuilder(device).build_test(testFile)
    with open(py_file) as f:
        suffix_info = ('.py', 'U', imp.PY_SOURCE)
        module = imp.load_module('test', f, py_file, suffix_info)
        return module.Test(testFile, version)

def run_test(device, arduino, test, app, version, batch=None, upload=True):
    # we use this because we used to pass the ArgParse namespace directly
    args = RunTestArgs(test, app, version)

    #device.connect(arduino)
    print("[Test]       Installing " + app)
    with device.install_context(args) as installed:
        # run test here
        test_obj = get_test(os.path.join('tests', test), app, device, version)

        testRunBuilder = TestRunBuilder(
            device=device,
            arduino=arduino,
            app=app,
            version=version,
            batch=batch,
            test=test_obj,
            name=test,
            packages=installed.packages
        )

        testRun = testRunBuilder.build_testrun()

        print("[Testing]    Starting Test " + test + " on " + app + " " + version)

        StatusUpdater(arduino.deviceid).running_test(test_obj.duration, testRun)
        test_obj.run(testRun)

    print("[Test]       Uninstalling " + app)
    print("[Test]       Test Finished")
    
    # Upload hook
    test_obj.before_upload(testRun)

    if upload:
        cmd = './upload_test_data.py --path {} --list {}'.format(testRun.wattlog_file, "uploads.txt").split()
        print("[Uploading]  Uploading Results to Server")
        try:
            subprocess.check_call(cmd)
        except Exception as e:
            print("Error uploading :( :: ", str(e))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run a test')
    parser.add_argument('device', help="device id")
    parser.add_argument('test', help="test name")
    parser.add_argument('app', help="app name")
    parser.add_argument('version', help="app version")
    parser.add_argument('--batch', help="optional batch name")
    parser.add_argument('--noupload', help="suppress data upload", action='store_true')

    args = parser.parse_args()
    builder = DeviceBuilder(deviceid=args.device)
    device, arduino = builder.build_device(), builder.build_arduino()

    run_test(device, arduino, args.test, args.app, args.version, args.batch, not args.noupload)
