#!/usr/bin/env python3
#
# Copyright (c) 2013 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import json, requests, sys
from itertools import *


from libgreenminer import Queue
queue = Queue('android')
queue.pull()

app_versions = list(queue.get_app_versions())
print("apks: {}".format(len(app_versions)))
print("tests: {}".format(queue.get_total_runs()))

for batch, tests in queue.get_runs_per_batch().items():
    print("\t{}: {} tests".format(batch, tests))

devices = int(sys.argv[1]) if len(sys.argv) > 1 else 3
print("days remaining (5 minutes per test, {} devices): {}".format(devices,
	queue.get_total_runs() * 5 / 60 / 24 / devices ))
