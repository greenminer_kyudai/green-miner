#
# Copyright (c) 2013 Alex Wilson, Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import os, re, serial, subprocess
import libgreenminer

class Arduino(object):
    """Represents an Arduino connected to the host computer.
        Provides methods for initializing, enabling/disabling USB,
        and reading measurements."""

    SERIAL_LOCATION = "/dev/"
    #TTY_PATTERN = re.compile(r"^ttyA[MC][MA]\d+$")
    TTY_PATTERN = re.compile(r"^cu.usbmodem\d+$")


    COMMAND_TURN_ON = b'0'
    STATUS_ON = '+'
    COMMAND_TURN_OFF = b'1'
    STATUS_OFF = '#'

    def __init__(self, deviceid=None, tuning=None, port=None, regex=None, header=None):
        """Build an Arduino instance for the given device/tuning/etc...
            If tuning is not specified, both header and regex must be.
            If tuning is specified, header and regex will be configured
            automatically.

            DeviceBuilder should generally be used instead of instantiating this
            class directly.
            """
        self.deviceid = deviceid
        self.port = port
        self.tuning = tuning

        self._packet_length = 0
        self._packet_index = 0

        if(not tuning):
            if(not header or not regex):
                raise Exception("Custom tuning information requires a CSV header and a compiled regex expression")
            self.header = header
            self.regex = regex
        elif(tuning == "hall"):
            self._setup_hall()
        elif(tuning == "ina219"):
            self._setup_ina219()
        elif(tuning == "ina169"):
            self._setup_ina169()
        elif(tuning == "legacy"):
            self._setup_legacy()
        else:
            raise Exception("Unknown tuning type")

        self.serial = serial.Serial(self.get_port(), 115200)

    def get_port(self):
        """
        If the Arduino's port is set, return it, otherwise attempt to look for it.
        """
        if(self.port):
            return self.port
        if(not self.deviceid):
            raise Exception("Device ID Not Specified")

        try:
            devices = os.listdir(Arduino.SERIAL_LOCATION)
        except:
            raise Exception("No Serial Devices Detected")

        for device in devices:
            if not Arduino.TTY_PATTERN.match(device):
                continue

            device = os.path.join(Arduino.SERIAL_LOCATION, device)
            if(os.path.exists(device)):
                if(subprocess.call(['fuser', device]) == 0):
                    print('skipping ', device, ', which is in use')
                    # Something's using this device
                    continue
                else:
                    # Make the serial port sane again!
                    #if(subprocess.call(['stty', '-F', device, 'sane']) != 0):
                    #   raise Exception("Could not make serial port sane!")
                    # Connect and see if it's the device we want
                    temp_serial = serial.Serial(device, 19200, timeout=0.25)
                    # Clear Buffer
                    temp_serial.read(temp_serial.inWaiting())
                    for attempt_read in range(0,10):
                        try:
                            if(temp_serial.readline().strip().decode('UTF-8')[0] == self.deviceid):
                                # We've found it!
                                self.port = device
                                temp_serial.close()
                                print("found arduino at", device)
                                return device
                        except:
                            # Blank or malformed line
                            continue
        raise Exception("No port was found -OR- port is already in use")

    def set_usb_on(self):
        """
        Turns on USB switch on the Arduino
        """
        for attempt_on in range(0, 5):
            self.flush_buffer()

            for attempt_read in range(0,10):
                self.serial.write(Arduino.COMMAND_TURN_ON)
                try:
                    if(self.get_reading()[2] == Arduino.STATUS_ON):
                        return True
                except:
                    continue
        raise Exception("Arduino could not set USB on")

    def set_usb_off(self):
        """
        Turns off USB switch on the Arduino
        """
        for attempt_off in range(0, 5):
            self.flush_buffer()

            for attempt_read in range(0,10):
                self.serial.write(Arduino.COMMAND_TURN_OFF)
                try:
                    if(self.get_reading()[2] == Arduino.STATUS_OFF):
                        return True
                except:
                    continue
        raise Exception("Arduino could not set USB off")

    def flush_buffer(self):
        """
        pyserial's flushOutput is broken.
        """
        #print(self.serial.in_waiting)
        #self.serial.reset_input_buffer()

        # read input buffer untill buffer become empty
        while self.serial.in_waiting > 1:
            self.serial.read(1)


    def _get_reading_ina219(self):
        return self.serial.readline().strip().decode('UTF-8')

    def _get_reading_ina169(self):
        if self._packet_index == 0:
            while True:
                if self.serial.read(1).hex() == "ff":
                    if self.serial.read(1).hex() == "ff":
                        if self.serial.read(1).hex() == "ff":
                            packet_length = int.from_bytes(self.serial.read(1), byteorder='big')
                            self._packet_length = packet_length
                            break

        rawValue_A0 = self.serial.read(2)
        rawValue_A1 = self.serial.read(2)
        voltage = self._transform_v_ina169(rawValue_A1)
        m_A = self._transform_ma_ina169(rawValue_A0)
        arduino_data = "X	#	" + str(m_A)[:7] + "	" + str(voltage)[:5]

        self._packet_index += 1
        if self._packet_index == self._packet_length:
            self._packet_index = 0

        return arduino_data

    #def _get_packet_length_ina169(self):



    def _setup_hall(self):
        self.header = "id,status,adc1,adc2,arduino_v"
        self.regex = re.compile("^[A-Z]\t[\+#]\t([0-9\.]{4,6})\t([0-9\.]{4,6})\t([0-9]{4})")

    def _setup_ina219(self):
        self.header = "id,status,device_ma,device_v"
        self.regex = re.compile("^[A-Z]\t[\+#]\t(-?[0-9\.]{4,7})\t([0-9\.]{4})")
        self.get_reading = self._get_reading_ina219

    def _setup_ina169(self):
        self.header = "id,status,device_ma,device_v"
        self.regex = re.compile("^[A-Z]\t[\+#]\t(-?[0-9\.]{3,7})\t([0-9\.]{5})")
        self.get_reading = self._get_reading_ina169

    def _setup_legacy(self):
        self.header = "id,status,adc1,adc2,arduino_v"
        self.regex = re.compile("^[A-Z]\t[\+#]\t([0-9\.]{4,6})\t([0-9\.]{4,6})\t([0-9]{4})")

    def _transform_ma_ina169(self, value_A0):
        RS = 0.1
        REF_VOLTAGE = 5.0
        SLOPE = 1.0083
        OFFSET = 0.0718

        value_A0 = int.from_bytes(value_A0, byteorder='big')
        sensorValue = (value_A0 * REF_VOLTAGE) / 1023.0
        measured_current = sensorValue / (10 * RS)
        measured_current_ma = measured_current * 1000
        current_ma = measured_current_ma * SLOPE + OFFSET
        return current_ma

    def _transform_v_ina169(self, value_A1):
        REF_VOLTAGE = 5.0
        SLOPE = 0.9996
        OFFSET = 0.0178

        value_A1 = int.from_bytes(value_A1, byteorder='big')
        # load_voltage is voltage between Vin+ ~ GND
        measured_load_voltage = (value_A1 * REF_VOLTAGE) / 1023.0
        load_voltage = measured_load_voltage * SLOPE + OFFSET
        return load_voltage
