#
# Copyright (c) 2013 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import json, logging, sys

import requests

def get_report_url():
    # fallback web root in case config.json is broken!
    web_root = 'https://pizza.cs.ualberta.ca/gm/'

    try:
        json.load(open('config.json'))['web_root']
    except:
        pass
    return web_root + '/status.py'


class StatusUpdater(object):
    def __init__(self, device):
        self.device = device

    def running_test(self, duration, testRun):
        return self.update_status('running', {
            'duration': duration,
            'test': testRun.name,
            'app': testRun.app,
            'version': testRun.version,
            'batch': testRun.batch or 'no_batch'
        })

    def processing(self, testRun):
        return self.update_status('processing', {
            'test': testRun.name,
            'app': testRun.app,
            'version': testRun.version,
            'batch': testRun.batch or 'no_batch'
        })

    def polling(self):
        return self.update_status('polling')

    def updating(self):
        return self.update_status('updating')

    def crashed(self):
        return self.update_status('crashed')

    def update_status(self, status, data={}):
        try:
            response = requests.post(get_report_url(), 
                    json.dumps({'status': status, 'data': data}),
                    params={'device': self.device})
        except requests.exceptions.RequestException as e:
            logging.info('failed to update status ' + str(e))
            return
        if not response.ok:
            logging.info('failed to update status ' + response.text)
        return response
