#
# Copyright (c) 2013 Alex Wilson, Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import calendar, datetime, re, shlex, subprocess, time, json, os

from .device import Device
from .test import AndroidTest


class Phone(Device):
    """Represents a phone connected to an Arduino, and a host machine.
        This class includes facilities for connecting through ADB,
        running commands, reading and changing settings and MORE!
    """
    MAX_ADB_ATTEMPTS = 2    # Number of times to try an ADB command
    CONNECT_TIME = 3        # Time to wait for USB connect in seconds

    def __init__(self, serial, battery=False, sleep=time.sleep):
        """
            Instantiate a phone object for the phone represented by
            serial.

            DeviceBuilder should generally be used instead of instantiating
            this class directly.

            sleep is expected to be a callable similar to time.sleep.
        """
        self.serial = serial
        self.battery = Battery(self, battery)
        self.sleep = sleep

    def build_test(self, test_file):
        """Builds a test from the test_file"""
        return AndroidTest(test_file)

    def adb(self, command, async=False):
        """Run an adb command for this phone.

            If the command fails, it will be run again after 5 seconds.
            The output of the command is stripped and returned as a string.
        """
        arguments = shlex.split(command)
        attempts_left = Phone.MAX_ADB_ATTEMPTS
        command_list = ['adb', '-s', self.serial] + arguments
        while(attempts_left):
            try:
                # If async, fire off call
                if async:
                    subprocess.Popen(command_list)
                    return

                return subprocess.check_output(command_list).decode('UTF-8')
            except subprocess.CalledProcessError:
                # Command failed, try again in 5 seconds
                attempts_left -= 1
                self.sleep(Phone.CONNECT_TIME)
        raise Exception("Failed to execute: " + str(command))

    def prepare_for_test(self, arduino):
        """Implement to add actions to be executed before a test"""
        self.battery.charge(arduino)

    def get_setting(self, name, namespace='system'):
        """Get the setting from the given namespace.
            Functionally equivalent to
                phone.shell('settings get {namespace} {name}')
            but faster.
        """
        return self.get_settings(namespace)[name]

    def get_settings(self, namespace='system'):
        """Gets a dictionary of all settings from the given namespace."""

        if(namespace not in ['system', 'global', 'secure']):
            raise Exception("Unknown settings namespace " + namespace)
        settings_database = "/data/data/com.android.providers.settings/databases/settings.db"
        output = self.shell("su -c \"sqlite3 {0} 'SELECT name,value FROM {1}'\"".format(settings_database, namespace)).strip()
        settings = {}
        for pair in output.split('\r\n'):
            # here we are, what if a spam app puts in multiline settings?
            # we now choose to ignore them
            if '|' in pair:
                [name, value] = pair.split('|')
                settings[name] = value
        return settings

    def get_wifi(self):
        """Get wifi info"""
        output = self.shell('dumpsys wifi')
        match = re.search(r"mWifiInfo SSID: (.+)", output)
        return match.group(1).strip()

    def get_enabled_packages(self):
        """Get a list of all enabled packages on the phone"""
        regexp = re.compile(r'package:\s*')

        def remove_prefix(line):
            return regexp.sub("", line.strip())

        return map(remove_prefix, self.shell('pm list packages').split())

    def get_installed_packages(self):
        """Get a list of 3rd party installed packages"""
        regexp = re.compile(r'package:\s*')

        def remove_prefix(line):
            return regexp.sub("", line.strip())

        return map(remove_prefix, self.shell('pm list packages -3').split())

    def install_apk(self, apk_path):
        """Installs an apk and returns a list of packages added
            as a result"""
        installed = set(self.get_installed_packages())
        self.adb('install {}'.format(apk_path))
        self.sleep(1)
        new_installed = set(self.get_installed_packages())
        return list(new_installed - installed)

    # abstracted
    def shell(self, command, async=False):
        """Convenience method: equivalent to adb("shell " + command)"""
        command = 'shell ' + command
        return self.adb(command, async)

    # abstracted method
    def push(self, src, target):
        """Push a file (src) to a location (target) on the phone."""
        return self.adb('push "{}" "{}"'.format(src, target))

    def is_connected(self):
        """Check whether the phone is connected to this computer and adb."""
        print( self.adb('devices'))
        print(self.serial)
        return self.serial in self.adb('devices')

    def connect(self, arduino):
        """Connect this phone via the given arduino object."""
        for attempt_on in range(0, 5):
            if(self.is_connected()):
                return True
            arduino.set_usb_on()
            self.sleep(Phone.CONNECT_TIME)
        raise Exception("Could not connect phone")

    def dump_info(self):
        """Returns a dict with lots of info about the phone and
            the current environment."""
        system_settings = self.get_settings()
        global_settings = self.get_settings("global")

        # Return Specific Settings
        return {
            'time': int(calendar.timegm(time.gmtime())),
            'os_version': self.shell('getprop ro.build.version.release').strip(),
            'charge_level': self.battery.get_charge_level(),
            'battery_health': self.battery.get_health(),
            'battery_temperature': self.battery.get_temperature(),
            'screen_auto_brightness': system_settings.get('screen_brightness_mode',None),
            #'screen_auto_brightness': system_settings['screen_brightness_mode'],
            'screen_brightness': system_settings.get('screen_brightness',None),
            #'screen_brightness': system_settings['screen_brightness'],
            'screen_off_timeout': system_settings.get('screen_off_timeout',None),
            #'screen_off_timeout': system_settings['screen_off_timeout'],
            'haptic_feedback_enabled': system_settings.get('haptic_feedback_enabled',None),
            #'haptic_feedback_enabled': system_settings['haptic_feedback_enabled'],
            'airplane_mode_on': global_settings.get('airplane_mode_on',None),
            #'airplane_mode_on': global_settings['airplane_mode_on'],
            'wifi': self.get_wifi(),
            'bluetooth_on': global_settings.get('bluetooth_on',None),
            #'bluetooth_on': global_settings['bluetooth_on']
        }

    def find_apk(self, app, commit):
        config = json.loads(open('config.json').read())
        APK_FOLDER = os.path.expanduser(config['images_folder'])
        path = os.path.join(APK_FOLDER, app)
        for d in [path, APK_FOLDER]:
            try:
                for f in os.listdir(d):
                    if(f == commit + '.apk' or f == commit):
                        return os.path.join(d, f)
            except FileNotFoundError:
                pass
        raise Exception("Did not find anything %s %s %s %s" % (path,app,commit,APK_FOLDER))

    def install_context(self, args):
        return Phone.APK(self, self.find_apk(args.app, args.version))

    class APK(object):
        def __init__(self, phone, apk):
            self.phone = phone
            self.apk = apk

        def __enter__(self):
            # Remove all old applications on the phone
            print('apk remove') # for debug
            old_packages = self.phone.get_installed_packages()
            for package in old_packages:
                print('apk remove; %s' % package)
                self.phone.adb('uninstall {}'.format(package))
            # Install our desired application
            print('apk install:%s' % self.apk)
            self.packages = self.phone.install_apk(self.apk)
            return self

        def __exit__(self, exc_type, exc_value, traceback):
            for package in self.packages:
                self.phone.adb('uninstall {}'.format(package))


class Battery(object):
    """Represents the battery in a phone. Useful for parsing battery info and
        charging the battery."""
    MAX_READING_AGE = 60    # Maximum Seconds since last reading
    SLEEP_TIME = 120        # Time between getting charge readings
    MIN_CHARGE = 95         # Minimum charge for the battery to be considered 'full'

    # Values from http://developer.android.com/reference/android/os/BatteryManager.html
    # Charging Codes
    BATTERY_UNKNOWN = 1
    BATTERY_CHARGING = 2
    BATTERY_DISCHARGING = 3
    BATTERY_NOT_CHARGING = 4
    BATTERY_FULL = 5

    # Health Codes
    BATTERY_HEALTH_UNKNOWN = 1
    BATTERY_HEALTH_GOOD = 2
    BATTERY_HEALTH_OVERHEAT = 3
    BATTERY_HEALTH_DEAD = 4
    BATTERY_HEALTH_OVER_VOLTAGE = 5
    BATTERY_HEALTH_UNSPECIFIED_FAILURE = 6
    BATTERY_HEALTH_COLD = 7

    def __init__(self, phone, battery=False, sleep=time.sleep):
        """Create a battery for the given phone.
            If battery is false, then this object will act as
            a dummy battery.
        """
        self.phone = phone
        self.battery = battery
        self.sleep = sleep

        self.last_reading_time = -1

    def charge(self, arduino=None, minimum_charge=MIN_CHARGE, verbose=True):
        """Charge this battery until it is full.
            The charge is checked every 2 minutes. If the charge reaches
            maximum, or the phone says it is done charging 5 times in a row,
            it will exit. If the charge starts to go down, it will exit as
            a failure. If the charge is above the minimum threshold after 30
            minutes, or for the last 10 minutes, it will exit as a success.
        """

        if(not self.battery):
            # No battery connected, nothing to charge
            if(verbose):
                print("[Charging Phone] COMPLETE: Phone is on a power supply!")
            return True
        self.phone.connect(arduino)

        charge_history = list()
        charge_history.append(self.get_charge_level())
        while(True):
            if(verbose):
                print("[Charging Phone] {0} Status: {1}, Levels: {2}".format(datetime.datetime.now(), self.get_status(), charge_history[-5:]))

            # If it's been ~30 minutes and obtained minimum charge
            if(len(charge_history) > 15 and self.get_charge_level() >= Battery.MIN_CHARGE):
                if(verbose):
                    print("[Charging Phone] COMPLETE: Reached Minimum Charge!")
                return True

            # If battery reports full
            if(self.get_status() == Battery.BATTERY_FULL or self.get_status() == Battery.BATTERY_NOT_CHARGING):
                if(verbose):
                    print("[Charging Phone] COMPLETE: Phone Reports Full!")
                return True
            # Battery reports discharging
            if(self.get_status() == Battery.BATTERY_DISCHARGING):
                if(verbose):
                    print("[Charging Phone] ERROR: Phone Reports Discharging!")
                return False

            # If the last five charges are the same
            changed = False
            for charge in charge_history[-5:]:
                if(charge != self.get_charge_level()):
                    changed = True
            if(changed == False and self.get_charge_level() > Battery.MIN_CHARGE):
                if(verbose):
                    print("[Charging Phone] COMPLETE: Phone Achieved Stable Charge!")
                return True

            self.sleep(120)
            charge_history.append(self.get_charge_level())

    def read_battery_info(self):
        """Throttled battery reading. Battery info is cached for
            Battery.MAX_READING_AGE seconds."""
        # If enough time has passed, get a new reading
        if((time.time() - self.last_reading_time) > Battery.MAX_READING_AGE):
            self.info = self.phone.adb('shell dumpsys battery')
        return self.info

    def get_charge_level(self):
        """Gets the charge level out of 100"""
        try:
            return int(re.search(r"\s*level:\s*(\d+)", self.read_battery_info()).group(1))
        except Exception as e:
            raise e from Exception("Could not obtain battery charge level")

    def get_health(self):
        """Gets the battery health"""
        try:
            return int(re.search(r"\s*health:\s*(\d)", self.read_battery_info()).group(1))
        except Exception as e:
            raise e from Exception("Could not obtain battery health")

    def get_status(self):
        """Gets the battery status"""
        try:
            return int(re.search(r"\s*status:\s*(\d)", self.read_battery_info()).group(1))
        except Exception as e:
            raise e from Exception("Could not obtain battery status")

    def get_temperature(self):
        """"Get the battery temperature"""
        try:
            return int(re.search(r"\s*temperature:\s*(\d+)", self.read_battery_info()).group(1))
        except Exception as e:
            raise e from Exception("Could not obtain battery temperature")
