#
# Copyright (c) 2013 Alex Wilson, Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import os, re, serial, subprocess


class Arduino(object):
    """Represents an Arduino connected to the host computer.
        Provides methods for initializing, enabling/disabling USB,
        and reading measurements."""

    SERIAL_LOCATION = "/dev/"
    #TTY_PATTERN = re.compile(r"^ttyA[MC][MA]\d+$")
    TTY_PATTERN = re.compile(r"^cu.usbmodem\d+$")


    COMMAND_TURN_ON = b'0'
    STATUS_ON = '+'
    COMMAND_TURN_OFF = b'1'
    STATUS_OFF = '#'

    def __init__(self, deviceid=None, tuning=None, port=None, regex=None, header=None):
        """Build an Arduino instance for the given device/tuning/etc...
            If tuning is not specified, both header and regex must be.
            If tuning is specified, header and regex will be configured
            automatically.

            DeviceBuilder should generally be used instead of instantiating this
            class directly.
            """
        self.deviceid = deviceid
        self.port = port
        self.tuning = tuning

        if(not tuning):
            if(not header or not regex):
                raise Exception("Custom tuning information requires a CSV header and a compiled regex expression")
            self.header = header
            self.regex = regex
        elif(tuning == "hall"):
            self._setup_hall()
        elif(tuning == "ina219"):
            self._setup_ina219()
        elif(tuning == "legacy"):
            self._setup_legacy()
        else:
            raise Exception("Unknown tuning type")

        self.serial = serial.Serial(self.get_port(), 19200)

    def get_port(self):
        """
        If the Arduino's port is set, return it, otherwise attempt to look for it.
        """
        if(self.port):
            return self.port
        if(not self.deviceid):
            raise Exception("Device ID Not Specified")

        try:
            devices = os.listdir(Arduino.SERIAL_LOCATION)
        except:
            raise Exception("No Serial Devices Detected")

        for device in devices:
            if not Arduino.TTY_PATTERN.match(device):
                continue

            device = os.path.join(Arduino.SERIAL_LOCATION, device)
            if(os.path.exists(device)):
                if(subprocess.call(['fuser', device]) == 0):
                    print('skipping ', device, ', which is in use')
                    # Something's using this device
                    continue
                else:
                    # Make the serial port sane again!
                    #if(subprocess.call(['stty', '-F', device, 'sane']) != 0):
                    #   raise Exception("Could not make serial port sane!")
                    # Connect and see if it's the device we want
                    temp_serial = serial.Serial(device, 19200, timeout=0.25)
                    # Clear Buffer
                    temp_serial.read(temp_serial.inWaiting())
                    for attempt_read in range(0,10):
                        try:
                            if(temp_serial.readline().strip().decode('UTF-8')[0] == self.deviceid):
                                # We've found it!
                                self.port = device
                                temp_serial.close()
                                print("found arduino at", device)
                                return device
                        except:
                            # Blank or malformed line
                            continue
        raise Exception("No port was found -OR- port is already in use")

    def set_usb_on(self):
        """
        Turns on USB switch on the Arduino
        """
        for attempt_on in range(0, 5):
            self.flush_buffer()

            for attempt_read in range(0,10):
                self.serial.write(Arduino.COMMAND_TURN_ON)
                try:
                    if(self.get_reading()[2] == Arduino.STATUS_ON):
                        return True
                except:
                    continue
        raise Exception("Arduino could not set USB on")

    def set_usb_off(self):
        """
        Turns off USB switch on the Arduino
        """
        for attempt_off in range(0, 5):
            self.flush_buffer()

            for attempt_read in range(0,10):
                self.serial.write(Arduino.COMMAND_TURN_OFF)
                try:
                    if(self.get_reading()[2] == Arduino.STATUS_OFF):
                        return True
                except:
                    continue
        raise Exception("Arduino could not set USB off")

    def flush_buffer(self):
        """
        pyserial's flushOutput is broken.
        """
        #self.serial.reset_output_buffer()
        #self.serial.read(self.serial.inWaiting())
        self.serial.read(self.serial.in_waiting)
    def get_reading(self):
        return self.serial.readline().strip().decode('UTF-8')

    def _setup_hall(self):
        self.header = "id,status,adc1,adc2,arduino_v"
        self.regex = re.compile("^[A-Z]\t[\+#]\t([0-9\.]{4,6})\t([0-9\.]{4,6})\t([0-9]{4})")

    def _setup_ina219(self):
        self.header = "id,status,device_ma,device_v"
        self.regex = re.compile("^[A-Z]\t[\+#]\t(-?[0-9\.]{4,7})\t([0-9\.]{4})")

    def _setup_legacy(self):
        self.header = "id,status,adc1,adc2,arduino_v"
        self.regex = re.compile("^[A-Z]\t[\+#]\t([0-9\.]{4,6})\t([0-9\.]{4,6})\t([0-9]{4})")
