#
# Copyright (c) 2013 Alex Wilson, Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import re

class Wattlog(object):
    def __init__(self, arduino=None, header=None, regex=None, returnline=None):
        """
        Initializes a wattlog object
        """
        self.arduino = arduino

        if(not self.arduino.tuning):
            if(header == None or regex == None):
                # You need to specify header and regex.
                raise Exception("No tuning information specified")
            self.header = header
            self.regex_device = regex
            self.return_line = returnline
        elif(arduino.tuning == "hall"):
            self._setup_hall()
            self.return_line = self._return_hall
        elif(arduino.tuning == "ina219"):
            self._setup_ina219()
            self.return_line = self._return_ina219
        elif(arduino.tuning == "ina169"):
            self._setup_ina169()
            self.return_line = self._return_ina169
        elif(arduino.tuning == "legacy"):
            self._setup_legacy()
            self.return_line = self._return_legacy
        else:
            raise Exception("Tuning type unknown")

    def return_header(self):
        return(self.header + "\n")

    def _compare_line(self, line):
        """
        Returns a regex object if it matches the specified formatting.
        """
        reading = self.arduino.regex.match(line)
        if(reading == None):
            raise Exception("Invalid line")
        else:
            return reading

    def _setup_hall(self):
        """
        Sets up the header and regex for the hall effect wattlogger.
        """
        self.header = "time,id,adc_batt_V,adc_A,V,mA,W"
        self.regex = re.compile("^([0-9\.]+),[A-Z],([\-0-9\.]+),([\-0-9\.]+),([\-0-9\.]+),([\-0-9\.]+),([\-0-9\.]+)")
    def _return_hall(self, line, time):
        """
        Prints a CSV line with the current data, formatted to the hall effect wattlogger.
        """
        if(line == None or time == None):
            raise Exception("No input")
        else:
            reading = self._compare_line(line)

            adc1 = float(reading.group(1))
            adc2 = float(reading.group(2))
            arduino_mV = float(reading.group(3))

            voltage_in = adc1 * (arduino_mV / 1023.0) / 1000.0
            amperage = (((adc2 - phone.tuning['zero_offset']) * (arduino_mV / 1023.0)) / 66.0) * 1000.0
            wattage = (voltage_in * amperage) / 1000.0

            return("%.3f,%s,%.3f,%.3f,%.0f,%.3f,%.3f\n" %(line_time, self.arduino.deviceid, adc1, adc2, mV_in, amperage, wattage))

    def _setup_ina219(self):
        """
        Sets up the header and regex for the ina219 (Adafruit) wattlogger.
        """
        self.header = "time,id,mA,V,W"
        self.regex = re.compile("^([0-9\.]+),[A-Z],([\-0-9\.]+),([\-0-9\.]+),([\-0-9\.]+)")
    def _return_ina219(self, line, line_time):
        """ Prints a CSV line with the current data, formatted to the ina219 (Adafruit) wattlogger.
        """
        if(line == None or line_time == None):
            raise Exception("No input")
        else:
            reading = self._compare_line(line)

            mA = float(reading.group(1))
            V = float(reading.group(2))

            wattage = (V * mA) / 1000.0

            return("%.3f,%s,%.3f,%.3f,%.3f\n" %(line_time, self.arduino.deviceid, mA, V, wattage))

    def _setup_ina169(self):
        """
        Sets up the header and regex for the ina169 (sparkfun) wattlogger.
        """
        self.header = "time,id,mA,V,W"
        self.regex = re.compile("^([0-9\.]+),[A-Z],([\-0-9\.]+),([\-0-9\.]+),([\-0-9\.]+)")

    def _return_ina169(self, line, line_time):
        """
        Prints a CSV line with the current data, formatted to the ina169 (sparkfun) wattlogger.
        """
        if(line == None or line_time == None):
            raise Exception("No input")
        else:
            reading = self._compare_line(line)

            mA = float(reading.group(1))
            V = float(reading.group(2))

            wattage = (V * mA) / 1000.0

            return("%.5f,%s,%.3f,%.3f,%.3f\n" %(line_time, self.arduino.deviceid, mA, V, wattage))

    def _setup_legacy(self):
        """
        Sets up the header and regex for the legacy (Jed's) wattlogger.
        """
        self.header = "time,id,adc_batt_V,adc_A,V_in,V_out,V_drop,mA,W"
        self.regex = re.compile("^([0-9\.]+),[A-Z],([\-0-9\.]+),([\-0-9\.]+),([\-0-9\.]+),([\-0-9\.]+),([\-0-9\.]+),([\-0-9\.]+),([\-0-9\.]+)")
    def _return_legacy(self, line, line_time):
        """
        Prints a CSV line with the current data, formatted to the legacy (Jed's) wattlogger.
        """
        if(line == None or line_time == None):
            raise Exception("No input")
        else:
            reading = self._compare_line(line)

            adc1 = float(reading.group(1))
            adc2 = float(reading.group(2))
            arduino_mV = float(reading.group(3))

            scale = (arduino_mV / 1023000.0)
            voltage_in = adc1 * scale
            voltage_out = (adc2 - phone.tuning['zero_offset']) * scale
            voltage_drop = voltage_out * phone.tuning['gain_ratio']
            amperage = (voltage_drop / phone.tuning['shunt']) * 1000.0
            wattage = amperage * voltage_in / 1000.0

            return("%.3f,%s,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f\n" %(line_time, self.arduino.deviceid, adc1, adc2, voltage_in, voltage_out, voltage_drop, amperage, wattage))
