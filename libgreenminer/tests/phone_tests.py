#
# Copyright (c) 2013 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import textwrap, unittest
from unittest.mock import Mock
from ..arduino import Arduino
from ..phone import Phone

class FakeInfo(object):
	def __init__(self):
		self.serial = "DUMMY_SERIAL"
		self.deviceid = "T"

	def adb_show_device_connected(self):
		return "List of devices attached\n{0}	device\n".format(self.serial)

	def adb_show_no_devices(self):
		return "List of devices attached\n"

	def package_list(self):
		return textwrap.dedent( \
		"""package:com.google.android.syncadapters.bookmarks
		package:com.google.android.syncadapters.contacts
		package:com.google.android.tag
		package:com.google.android.talk
		package:com.google.android.tts
		package:com.google.android.videoeditor
		package:com.google.android.videos
		package:com.google.android.voicesearch
		package:com.google.android.youtube
		package:com.google.earth
		package:com.tf.thinkdroid.sg
		package:org.mozilla.fennec_ejbarlow""")

class PhoneTests(unittest.TestCase):
	def setUp(self):
		self.fake_info = FakeInfo()
		self.time_slept = 0
		def sleep(x):
			self.time_slept += x
		self.phone = Phone(self.fake_info.serial, sleep=sleep)

	def test_assert_connected(self):
		"""Assert Connected (ADB Shows Device)"""
		# Make our "adb" show our device is connected
		self.phone.adb = Mock(return_value=self.fake_info.adb_show_device_connected())
		self.assertTrue(self.phone.is_connected())

		# Our phone should not try to use the Arduino (None) if it is already connected
		self.assertTrue(self.phone.connect(None))

	def test_assert_not_connected(self):
		"""Assert Disconnected (ADB Shows No Devices & Different Device)"""
		# Make our "adb" show no devices connected
		self.phone.adb = Mock(return_value=self.fake_info.adb_show_no_devices())
		self.assertFalse(self.phone.is_connected())

		# Make our "adb" show a different device connected
		self.fake_info.serial = "NOT_THE_SAME_SERIAL"
		self.phone.adb = Mock(return_value=self.fake_info.adb_show_device_connected())
		self.assertFalse(self.phone.is_connected())

	def test_package_listing(self):
		"""Test parsing of package listing"""
		self.phone.adb = Mock(return_value=self.fake_info.package_list())
		result = list(self.phone.get_installed_packages())
		self.assertIn("org.mozilla.fennec_ejbarlow", result)
		self.assertIn("com.google.android.tts", result)
		self.assertIn("com.google.android.youtube", result)

	def test_package_install_diff(self):
		"""Test returning of installed packages"""
		packages = ['com.google.android.videos',
			'com.google.android.voicesearch', 'com.google.android.youtube',
			'com.google.earth', 'com.tf.thinkdroid.sg',
			'org.mozilla.fennec_ejbarlow']
		def get_installed():
			self.phone.get_installed_packages.side_effect = None
			self.phone.get_installed_packages.return_value = packages
			return packages[:-3]
		self.phone.get_installed_packages = Mock(side_effect=get_installed)
		self.phone.adb = Mock()

		apk = 'some/fake/thing.apk'
		installed = self.phone.install_apk(apk)

		self.phone.adb.assert_any_call('install {}'.format(apk))
		# sort to avoid ordering issues
		self.assertEqual(sorted(installed), sorted(packages[-3:]))


if __name__ == '__main__':
	unittest.main()
