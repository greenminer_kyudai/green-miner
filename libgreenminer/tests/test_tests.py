#
# Copyright (c) 2013 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import io, itertools, json, os, pystache, random, string, time, unittest

from unittest.mock import Mock, ANY

from libgreenminer import Phone, RaspberryPi, RaspberryPiTestRun, AndroidTestRun
from libgreenminer import AndroidTest, RaspberryPiTest


class MockTestFile(object):
    def __init__(self, name, app, duration=None):
        self.name = name
        self.app = app
        self.duration = duration or 10

    def openDurationFile(self):
        return io.StringIO(\
u"""
{}
""".format(self.duration))

    def openBashInFile(self):
        return io.StringIO(u"{{" + self.bash_key + "}}")

    def openBashOutFile(self):
        self.bash_out = io.StringIO()
        self.bash_out.close = Mock(return_value=None)
        self.bash_out.name = ''.join(random.sample(string.ascii_letters, 20))
        return self.bash_out

class AndroidTestTests(unittest.TestCase):
    def setUp(self):
        self.app = 'custom_app'
        self.mockFile = MockTestFile('mock_test', self.app)
        self.mockFile.bash_key = 'APP'
        self.test = AndroidTest(self.mockFile)
        self.commit ='abc1234def56'
        self.packages = ['org.mozilla.fennec']
        self.phone = Mock()
        self.phone.serial = 'dummy_serial'
        self.phone.push = Mock(spec=Phone.push)
        self.phone.dump_info = Mock(return_value={
            'time': str(time.time()),
            'wifi': 'wifi stuff!',
        })

        self.builder = Mock()
        self.builder.build_phone.return_value = self.phone
        self.builder.build_arduino.return_value = Mock()
        self.builder.build_arduino.return_value.deviceid = 'Z'

        phone = self.builder.build_phone()
        arduino = self.builder.build_arduino()

        self.testRun = AndroidTestRun(phone=phone, arduino=arduino, test=self.test,
                version=self.commit, packages=self.packages)

    def test_duration(self):
        """Test that duration is read properly"""
        for i in [0,1,275,98]:
                self.mockFile.duration = i
                self.assertEqual(AndroidTest(self.mockFile).duration, i)

    def test_templating(self):
        """test that templating works at least a bit correctly"""
        self.test.push_files(self.testRun)
        self.mockFile.bash_key = 'APP'
        self.assertEqual(self.mockFile.bash_out.getvalue().strip(),
                self.packages[0].strip())

    def test_templating_hook(self):
        """test that the templating hook is properly used"""
        def dumper(**kwargs):
            # remove lambdas
            if 'input_text' in kwargs:
                del kwargs['input_text']
            return json.dumps(kwargs)
        self.test.template_bash = Mock(side_effect=dumper)
        self.test.get_template_data = Mock(return_value={
            'custom_stuff': 'custom',
            'APP': 'overwritten',
        })
        self.test.push_files(self.testRun)

        result = json.loads(self.mockFile.bash_out.getvalue())
        self.assertEqual(result['custom_stuff'], 'custom')
        self.assertEqual(result['APP'], 'overwritten')

    def test_pushing(self):
        """check that the correct files are pushed"""
        self.test.push_files(self.testRun)
        assert(self.testRun.phone.push.call_args_list[0] == ((self.mockFile.bash_out.name, '/sdcard/script'),))

    def test_get_next_run(self):
        """test getting the next run number in a folder"""
        files = ['.stuff', 'other thing']
        folder = 'test_folder/name'
        listdir = Mock(return_value=files, spec=os.listdir)

        self.assertEqual(self.test.get_next_run(folder, listdir), 0)
        listdir.assert_called_with(folder)

        for i in range(0, 20):
            listdir.return_value = files + list(range(0, i))
            self.assertEqual(self.test.get_next_run(folder, listdir), i)
            listdir.assert_called_with(folder)

    def test_dump_device_data_calls_hook(self):
        """ assert dump_device_data calls  dump_data() hook"""
        output = io.StringIO()
        output.close = Mock()

        self.test.dump_data = Mock(return_value={'custom_stuff': 'custom'})
        self.test.dump_device_data(self.phone, output)

        self.test.dump_data.assert_called_with(self.phone)
        self.assertTrue(self.phone.dump_info.called)
        self.assertTrue(output.close.called)

    def test_dump_device_data_hook_can_overwrite(self):
        """assert dump_data() result is used"""
        output = io.StringIO()
        output.close = Mock()

        self.test.dump_data = Mock(return_value={
            'custom_stuff': 'custom',
            'time': 0,
        })
        self.test.dump_device_data(self.phone, output)
        result = json.loads(output.getvalue())
        self.assertEqual(result['time'], 0)
        self.assertEqual(result['custom_stuff'], 'custom')

    def test_before_after_hooks(self):
        """test that before() and after() are called"""
        self.test.after = Mock(spec=self.test.after)
        self.test.before = Mock(spec=self.test.before)
        self.test.run_wattlog_and_bash = Mock(return_value=3.4,
                spec=self.test.run_wattlog_and_bash)
        self.test.get_next_run = Mock(return_value=4,
                spec=self.test.get_next_run)
        self.test.get_timing_info = Mock(spec=self.test.get_timing_info)
        self.test.clean_files = Mock(spec=self.test.clean_files)
        self.test.dump_device_data = Mock()
        self.test.compute_statistics = Mock()

        self.test.run(self.testRun)

        self.test.after.assert_any_call(self.testRun)
        self.test.before.assert_any_call(self.testRun)

class RaspberryPiTestTests(unittest.TestCase):
    def setUp(self):
        self.app = 'custom_app'
        self.mockFile = MockTestFile('mock_test', self.app)
        self.mockFile.bash_key = 'APP'
        self.test = RaspberryPiTest(self.mockFile)
        self.commit ='abc1234def56'
        self.packages = ['org.mozilla.fennec']
        self.raspberrypi = Mock()
        self.raspberrypi.serial = 'dummy_serial'
        self.raspberrypi.push = Mock(spec=RaspberryPi.push)
        self.raspberrypi.dump_info = Mock(return_value={
            'time': str(time.time()),
            'wifi': 'wifi stuff!',
        })

        self.builder = Mock()
        self.builder.build_raspberrypi.return_value = self.raspberrypi
        self.builder.build_arduino.return_value = Mock()
        self.builder.build_arduino.return_value.deviceid = 'Z'

        raspberrypi = self.builder.build_raspberrypi()
        arduino = self.builder.build_arduino()

        self.testRun = RaspberryPiTestRun(raspberrypi=raspberrypi, arduino=arduino, test=self.test,
                version=self.commit, packages=self.packages)

    def test_duration(self):
        """Test that duration is read properly"""
        for i in [0,1,275,98]:
                self.mockFile.duration = i
                self.assertEqual(RaspberryPiTest(self.mockFile).duration, i)

    def test_templating(self):
        """test that templating works at least a bit correctly"""
        self.test.push_files(self.testRun)
        self.mockFile.bash_key = 'APP'
        self.assertEqual(self.mockFile.bash_out.getvalue().strip(),
                self.packages[0].strip())

    def test_templating_hook(self):
        """test that the templating hook is properly used"""
        def dumper(**kwargs):
            # remove lambdas
            if 'input_text' in kwargs:
                del kwargs['input_text']
            return json.dumps(kwargs)
        self.test.template_bash = Mock(side_effect=dumper)
        self.test.get_template_data = Mock(return_value={
            'custom_stuff': 'custom',
            'APP': 'overwritten',
        })
        self.test.push_files(self.testRun)

        result = json.loads(self.mockFile.bash_out.getvalue())
        self.assertEqual(result['custom_stuff'], 'custom')
        self.assertEqual(result['APP'], 'overwritten')

    def test_pushing(self):
        """check that the correct files are pushed"""
        self.test.push_files(self.testRun)
        assert(self.testRun.raspberrypi.push.call_args_list[0] == ((self.mockFile.bash_out.name, '/tmp/script'),))

    def test_get_next_run(self):
        """test getting the next run number in a folder"""
        files = ['.stuff', 'other thing']
        folder = 'test_folder/name'
        listdir = Mock(return_value=files, spec=os.listdir)

        self.assertEqual(self.test.get_next_run(folder, listdir), 0)
        listdir.assert_called_with(folder)

        for i in range(0, 20):
            listdir.return_value = files + list(range(0, i))
            self.assertEqual(self.test.get_next_run(folder, listdir), i)
            listdir.assert_called_with(folder)

    def test_dump_device_data_calls_hook(self):
        """ assert dump_device_data calls  dump_data() hook"""
        output = io.StringIO()
        output.close = Mock()

        self.test.dump_data = Mock(return_value={'custom_stuff': 'custom'})
        self.test.dump_device_data(self.raspberrypi, output)

        self.test.dump_data.assert_called_with(self.raspberrypi)
        self.assertTrue(self.raspberrypi.dump_info.called)
        self.assertTrue(output.close.called)

    def test_dump_device_data_hook_can_overwrite(self):
        """assert dump_data() result is used"""
        output = io.StringIO()
        output.close = Mock()

        self.test.dump_data = Mock(return_value={
            'custom_stuff': 'custom',
            'time': 0,
        })
        self.test.dump_device_data(self.raspberrypi, output)

        print(self.raspberrypi)

        result = json.loads(output.getvalue())
        self.assertEqual(result['time'], 0)
        self.assertEqual(result['custom_stuff'], 'custom')

    def test_before_after_hooks(self):
        """test that before() and after() are called"""
        self.test.after = Mock(spec=self.test.after)
        self.test.before = Mock(spec=self.test.before)
        self.test.run_wattlog_and_bash = Mock(return_value=3.4,
                spec=self.test.run_wattlog_and_bash)
        self.test.get_next_run = Mock(return_value=4,
                spec=self.test.get_next_run)
        self.test.get_timing_info = Mock(spec=self.test.get_timing_info)
        self.test.clean_files = Mock(spec=self.test.clean_files)
        self.test.dump_device_data = Mock()
        self.test.compute_statistics = Mock()

        self.test.run(self.testRun)

        self.test.after.assert_any_call(self.testRun)
        self.test.before.assert_any_call(self.testRun)

class AndroidTestRunTests(unittest.TestCase):
    def setUp(self):
        self.commit ='abc1234def56'
        self.packages = ['org.mozilla.fennec']
        builder = Mock()
        builder.build_phone.return_value = Mock()
        builder.build_arduino.return_value = Mock()
        builder.build_arduino.return_value.deviceid = 'Z'
        phone = builder.build_phone()
        arduino = builder.build_arduino()
        self.test = Mock()
        self.test.test = MockTestFile('fake_test', 'firefox')

        self.testRun = AndroidTestRun(phone=phone, arduino=arduino, test=self.test,
            version=self.commit, packages=self.packages)

    def test_folder_name(self):
        """Assert that the folder name contains the correct parts"""
        folder = self.testRun.make_folder(makedirs=Mock())
        parts = []
        while True:
            folder, part = os.path.split(folder)
            if not part:
                break
            parts.append(part)

        self.assertIn(self.commit, parts)
        self.assertIn(self.testRun.arduino.deviceid, parts)
        self.assertIn(self.test.test.name, parts)
        self.assertIn(self.testRun.batch, parts)

    def test_make_folder(self):
        """Assert that makedirs is called correctly"""
        makedirs = Mock(spec=os.makedirs)

        folder = self.testRun.make_folder(makedirs)
        makedirs.assert_any_call(folder, exist_ok=True)

if __name__ == '__main__':
	unittest.main()

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
