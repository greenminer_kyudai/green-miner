#
# Copyright (c) 2013 Alex Wilson, Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import json
import libgreenminer


class DeviceBuilder(object):
    """Factory for Arduino and Phone objects.
        This class will read the devices.json file for configuration
        information, which it will use to produce appropriately
        configured objects.
    """
    DEFAULT_CONFIG_FILE = "devices.json"

    def __init__(self, deviceid=None, devicefile=None, serial=None):
        """Create a devicebuilder for the specified device.

            At least a deviceid or serial must be specified."""
        if(not deviceid and not serial):
            raise Exception("No Device ID Specified")
        if(not devicefile):
            devicefile = DeviceBuilder.DEFAULT_CONFIG_FILE

        # Opening config file
        try:
            if(type(devicefile) == str):
                config = json.load(open(devicefile, 'r'))
            else:
                config = json.load(devicefile)
        except:
            raise Exception("Could not open " + devicefile)

        # Get device ID from serial if not speicified
        if(serial):
            for device in config:
                if(config[device]['serial'] == serial):
                    deviceid = device
                    break
            if(not deviceid):
                raise Exception("Could not find device id for serial " + serial)

        # Getting information out of config file
        try:
            self.config = config[deviceid]
        except:
            raise Exception("No device info found for " + deviceid + " in " + devicefile)

        # Device
        self.device = self.config.get('device')

        # Arduino Parameters
        self.arduino_id = deviceid
        self.arduino_port = self.config.get('port')
        print(self.arduino_port)
        self.arduino_tuning = self.config.get('tuning')

    def build_device(self):
        if self.device == 'raspberrypi':
            return self.build_raspberrypi()
        elif self.device == 'android':
            return self.build_phone()
        else:
            raise Exception("Device \"" + self.device + "\" cannot be built")

    def build_phone(self):
        """Instantiate a Phone object for the device."""
        serial = self.config.get('serial')
        battery = self.config.get('battery')
        return libgreenminer.Phone(serial, battery)

    def build_raspberrypi(self):
        """Instantiate an Raspberry Pi object for the device."""
        username = self.config.get('username')
        ip_address = self.config.get('ip_address')
        return libgreenminer.RaspberryPi(username, ip_address)

    def build_arduino(self):
        """Instantiate an Arduino object for the device."""
        deviceid = self.arduino_id
        tuning_type = self.arduino_tuning.get('type')
        port = self.arduino_port
        print(port)
        return libgreenminer.Arduino(deviceid, tuning_type, port)
