#
# Copyright (c) 2013 Alex Wilson, Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import abc, json, os, pystache, re, subprocess, sys, tempfile, time

from multiprocessing import Process

import libgreenminer

AUTH_FILE = os.path.expanduser('~/tests_auth.json')


class TestRun(object):
    """Represents a single run of a test on a given phone, arduino,
    application and version."""
    __metaclass__ = abc.ABCMeta

    RESULT_ROOT = os.path.expanduser('~/dat/')

    @abc.abstractmethod
    def get_device(self):
        """Returns the device for the test"""
        pass

    def make_folder(self, makedirs=os.makedirs):
        """Makes a folder for this test run's results."""
        self.run_folder = os.path.join(self.RESULT_ROOT, self.arduino.deviceid, self.batch, self.test.test.name, self.test.test.app, self.version)
        makedirs(self.run_folder, exist_ok=True)

        return self.run_folder


class AndroidTestRun(TestRun):
    """Represents a single run of a test on a given phone, arduino,
        application and version."""

    def __init__(self, phone, arduino, test, name, app, version, packages, batch='no_batch'):
        self.phone = phone
        self.arduino = arduino
        self.test = test
        self.name = name
        self.app = app
        self.version = version
        self.batch = batch or 'no_batch'
        self.packages = packages

    # abstracted
    def get_device(self):
        """Returns the device for the test"""
        return self.phone


class RaspberryPiTestRun(TestRun):
    """Represents a single run of a test on a given raspberry pi, arduino,
        application and version."""

    def __init__(self, raspberrypi, arduino, test, name, app, version, packages, batch='no_batch'):
        self.raspberrypi = raspberrypi
        self.arduino = arduino
        self.test = test
        self.name = name
        self.app = app
        self.version = version
        self.batch = batch or 'no_batch'
        self.packages = packages

    # abstracted
    def get_device(self):
        """Returns the device for the test"""
        return self.raspberrypi


class TestFile(object):
    """
    Represents a test on the file system
    """

    def __init__(self, path, app):
        """ Instantiates an object for the given path and app."""
        self.path = path
        self.app = app
        self.name = os.path.split(path)[1]

        self.py_file = os.path.join(self.path, app, 'test.py')

    def get_py_file(self):
        """Returns the path for the appropriate test.py file, if there is one.
        First the path/app folder is checked, then path/."""
        app_file = os.path.join(self.path, self.app, 'test.py')

        if os.path.exists(app_file):
            return app_file

        test_file = os.path.join(self.path, 'test.py')
        return test_file if os.path.exists(test_file) else None

    def get_partition_file(self):
        """Gets the path to the partition_info.csv file, if it exists."""
        partition_file = os.path.join(self.path, '..', 'partition_info.csv')
        return partition_file if os.path.exists(partition_file) else None

    def openDurationFile(self):
        """Opens and returns the duration file for the test."""
        return open(os.path.join(self.path, self.app, 'duration'), 'r')

    def openBashInFile(self):
        """Opens and returns the bash input file for the test."""
        return open(os.path.join(self.path, self.app, 'script.sh'), 'r')

    def openBashOutFile(self):
        """Opens and returns the bash output file for the test."""
        return tempfile.NamedTemporaryFile(mode='w')


class MultiTestFile(TestFile):
    """
    Represents a test on the file system
    """

    def __init__(self, testFile, package_name):
        """ Instantiates an object for the given path and app."""
        self.path = testFile.path
        self.app = testFile.app
        self.name = os.path.split(testFile.path)[1]
        self.package_name = package_name
        #self.package_name = self.version  # todo lookup packagename in dictionary
        
        self.py_file = os.path.join(self.path, self.app, 'test.py')


    def get_partition_file(self):
        """Gets the path to the partition_info.csv file, if it exists."""
        partition_file = os.path.join(self.path, 'partition', self.package_name + '_partition.csv')
        return partition_file if os.path.exists(partition_file) else None

    def openDurationFile(self):
        """Opens and returns the duration file for the test."""
        return open(os.path.join(self.path, 'duration', self.package_name + '_duration'), 'r')

    def openBashInFile(self):
        """Opens and returns the bash input file for the test."""
        return open(os.path.join(self.path, self.app, self.package_name + '.sh'), 'r')
    # note: '__' used to join the version with the suffix is also in the libgreenminer/result.py file. It is controller by the multiTest and version parameter in the call to TestResult()


class Test(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, test, version = None):
        self.test = test
        self.duration = self.get_duration()

    def get_duration(self):
        with self.test.openDurationFile() as dfile:
            return int(dfile.read().strip())

    @staticmethod
    def bash_input_text(text):
        """Used in script templates to input large swathes of text."""
        return "".join("input text {}\ninput keycode SPACE\n".format(x) for x in text.split(" "))

    def get_auth_info(self):
        """Read the AUTH_FILE for auth information to include in the template."""
        try:
            with open(AUTH_FILE, 'r') as auth:
                data = json.load(auth)
                return data
        except IOError:
            print("Failed to read auth file")
            return None

    def template_bash(self, **kwargs):
        """Template the script file."""
        output = []
        comment_regexp = re.compile(r'^\s*#')
        with self.test.openBashInFile() as bash:
            renderer = pystache.Renderer(escape=lambda u: u)
                # disable html escaping
            rendered = renderer.render(bash.read(), kwargs)
            for line in rendered.split('\n'):
                if comment_regexp.match(line):
                    continue
                line = line.strip()
                if line:
                    output.append(line)
            return '\n'.join(output)

    def compute_statistics(self, testRun):
        """Compute the post-run statistics."""
        result = libgreenminer.TestResult(testRun.wattlog_file)
        out_file = testRun.wattlog_file + '_totals.csv'
        with open(out_file, 'w') as f:
            f.write(result.getTotalInfo())

        out_file = testRun.wattlog_file + '_partitions.csv'
        with open(out_file, 'w') as f:
            f.write(result.getPartitionInfo())

    def get_next_run(self, folder, listdir=os.listdir):
        """Find the appropriate run number for this run in <folder>."""
        run = -1
        for f in listdir(folder):
            try:
                if int(f) > run:
                    run = max(run, int(f))
            except ValueError:
                pass
        return run + 1

    @abc.abstractmethod
    def push_files(self, testRun):
        """Push all the files needed on the device for the test to run."""
        raise Exception("Abstract Method not implemented")

    @abc.abstractmethod
    def clean_files(self, device):
        """Clean up any files that might have been made by the test run."""
        raise Exception("Abstract Method not implemented")

    @abc.abstractmethod
    def run(self, run):
        """Run this test on the phone. Does all the things."""
        raise Exception("Abstract Method not implemented")

    @abc.abstractmethod
    def get_microtime(self, testRun):
        """Returns the time from the device"""
        raise Exception("Abstract Method not implemented")

    def run_wattlog_and_bash(self, testRun):
        print("wat1")
        """Start wattlog and the test script."""
        wattlog = libgreenminer.Wattlog(testRun.arduino)

        with open(testRun.wattlog_file, 'w') as output:
            print("wat2")
            # Clear Arduino's old readings
            #testRun.arduino.flush_buffer()

            # Sync time with launching script
            android_sync = self.get_microtime(testRun)
            sync_time = time.time()

            print("wat3")
            # Run the test command on the device
            self.run_test_command(testRun.get_device())

            print("wat4")
            time.sleep(1)
            #testRun.arduino.set_usb_off()

            # Print header (CSV)
            output.write(wattlog.return_header())

            print("wat5")
            # Run wattlog for test duration
            start_time = time.time()
            while(start_time >= time.time() - self.duration):
                try:
                    output.write(wattlog.return_line(testRun.arduino.get_reading(), time.time() - start_time))
                except KeyboardInterrupt:
                    break
                except:
                    # Empty or malformed reading
                    continue

        return android_sync + (start_time - sync_time)

    @abc.abstractmethod
    def run_test_command(self, device):
        """Makes a command to run the script on the phone."""
        raise Exception("Abstract Method not implemented")

    @abc.abstractmethod
    def dump_device_data(self, phone, output):
        """Dump the device info to a file for this run."""
        raise Exception("Abstract Method not implemented")

    @abc.abstractmethod
    def get_timing_info(self, testRun, sync):
        """Get the timing info, as reported by the device during the test run."""
        raise Exception("Abstract Method not implemented")

    # hook functions
    def dump_data(self, phone):
        """Hook method to add any extra data to the before/after json file"""
        return dict()

    def get_template_data(self, phone, packages):
        """Hook method to add any extra data to the script template."""
        return dict()

    def with_run(self, run):
        """Hook method called when we first create the run object"""
        pass

    def before(self, run):
        """Hook method called before starting the test."""
        pass

    def after(self, run):
        """Hook method called after running the test.
           BUT before killing the app!!!
        """
        pass

    def before_upload(self, run):
        pass


class AndroidTest(Test):
    """Represents a green-star test.
        It encapsulates the many steps of running a test:

        connecting the phone
        installing an app
        templating the script
        installing the script
        logging power usage
        running the script
        cleaning up
        processing results a bit
    """

    # abstracted
    def push_files(self, testRun):
        """Push all the files needed on the device for the test to run."""
        template_data = dict(
            input_text=Test.bash_input_text,
            AUTH=self.get_auth_info(),
            APP=testRun.packages[0],
            timing="echo `microtime` >> /sdcard/timing"
        )

        template_data.update(self.get_template_data(testRun.phone, testRun.packages))
        with self.test.openBashOutFile() as bash:
            bash.write(self.template_bash(**template_data))
            bash.flush()
            testRun.phone.push(bash.name, '/sdcard/script')

    # abstracted
    def clean_files(self, device):
        """Clean up any files that might have been made by the test run."""
        device.shell('rm /sdcard/scriptlog.txt')
        device.shell('rm /sdcard/script')
        device.shell('rm /sdcard/timing')

    # abstracted
    def get_microtime(self, testRun):
        """Returns the time from the device"""
        time = testRun.get_device().shell("microtime").strip()
        return float(testRun.get_device().shell("microtime").strip())

    # abstracted
    def run(self, run):
        """Run this test on the phone. Does all the things."""
        print("RUN")
        run.phone.connect(run.arduino)

        run_folder = run.make_folder()

        next_run = self.get_next_run(run_folder)
        run.wattlog_file = os.path.join(run_folder, str(next_run))
        self.with_run(run)
        self.dump_device_data(run.phone, run.wattlog_file + "_before.json")

        print("RUN2")
        try:
            self.clean_files(run.phone)
            self.push_files(run)
            print("RUN2.5")
            self.before(run)
            print("RUN3")
            android_sync = self.run_wattlog_and_bash(run)
            print("RUN4")
            #run.phone.connect(run.arduino)
            self.get_timing_info(run, android_sync)
            print("RUN5")
        finally:
            self.after(run)
            self.clean_files(run.phone)

        self.dump_device_data(run.phone, run.wattlog_file + "_after.json")
        print(run.wattlog_file)
        self.compute_statistics(run)

        return run

    # abstracted
    def run_test_command(self, device):
        """Makes a command to run the script on the phone."""
        device.shell('busybox nohup sh /sdcard/script > /dev/null 2>&1', True)

    # abstracted
    def dump_device_data(self, phone, output):
        """Dump the device info to a file for this run."""
        if type(output) is str:
            output = open(output, 'w')

        with output as out:
            data = phone.dump_info()
            data.update(self.dump_data(phone))
            out.write(json.dumps(data))

    # abstracted
    def get_timing_info(self, testRun, sync):
        """Get the timing info, as reported by the device during the test run."""
        out_file = testRun.wattlog_file + '_timing'
        with open(out_file, 'w') as f:
            # Put start time as first line
            f.write(str(sync) + '\n')
            # Add rest of timing information
            f.write(testRun.phone.shell('cat /sdcard/timing'))

class AndroidMultiTest(AndroidTest):
    def __init__(self, test, version):  # input to our script
        self.test = test
        # TODO replace version in MultiTestFile with package_name
        self.package_name = self.to_package_name(version, test.path)
        self.test = MultiTestFile(test, self.package_name)
        self.duration = self.get_duration()
        

    def compute_statistics(self, testRun):
        """Compute the post-run statistics."""
        result = libgreenminer.TestResult(testRun.wattlog_file, multiTest=True, package_name=self.package_name)
        out_file = testRun.wattlog_file + '_totals.csv'
        with open(out_file, 'w') as f:
            f.write(result.getTotalInfo())

        out_file = testRun.wattlog_file + '_partitions.csv'
        with open(out_file, 'w') as f:
            f.write(result.getPartitionInfo())

                
    def to_package_name(self, version, test_path):
        """
        Translates apk names into package names for code-reflecting in
        MultiTest test.py files.
        """
        # file format: app_version_name\tpackage_name\tactivity_name\n
        plain_text = open(os.path.join(test_path, 'apk_to_package.txt')).readlines()
        split_text = [x.rstrip().split('\t') for x in plain_text]
        ver_to_package_dict = dict()
        for line in split_text:
            ver_to_package_dict[line[0]] = line[1]
        return ver_to_package_dict[version]

            
class RaspberryPiTest(Test):

    # abstracted
    def dump_device_data(self, device, output):
        """Dump the device info to a file for this run."""
        if type(output) is str:
            output = open(output, 'w')

        with output as out:
            data = device.dump_info()
            data.update(self.dump_data(device))
            out.write(json.dumps(data))

    def clean_files(self, device):
        """Removes data folder if used"""
        device.rm('/tmp/script')
        device.rm('/tmp/timing')

    def hub_action(self):
        """Implement to add functionality when test runs on client"""
        pass

    # abstracted
    def run_test_command(self, device):
        """Makes a command to run the script on the pi."""

        # Run the script on the pi
        with device.cd('/tmp/rpi'):
          device.shell('bash /tmp/script 2>&1', True)

        # Start the hub action
        Process(target=self.hub_action).start()

    # abstracted
    def get_microtime(self, testRun):
        """Returns the time from the device"""
        return float(testRun.get_device().shell("/home/pi/bin/microtime").strip())

    # abstracted
    def run(self, run):
        """Run this test on the phone. Does all the things."""
        # run.raspberrypi.connect(run.arduino)

        run_folder = run.make_folder()

        next_run = self.get_next_run(run_folder)
        run.wattlog_file = os.path.join(run_folder, str(next_run))

        self.dump_device_data(run.raspberrypi, run.wattlog_file + "_before.json")

        try:
            self.clean_files(run.raspberrypi)
            self.push_files(run)

            self.before(run)

            android_sync = self.run_wattlog_and_bash(run)
            self.get_timing_info(run, android_sync)
            self.after(run)
        finally:
            self.clean_files(run.raspberrypi)

        self.dump_device_data(run.raspberrypi, run.wattlog_file + "_after.json")
        self.compute_statistics(run)

        return run

    # abstracted
    def push_files(self, testRun):
        """Push all the files needed on the device for the test to run."""
        template_data = dict(
            input_text=Test.bash_input_text,
            AUTH=self.get_auth_info(),
            APP=testRun.packages[0],
            timing="echo `/home/pi/bin/microtime` >> /tmp/timing"
        )

        template_data.update(self.get_template_data(testRun.raspberrypi, testRun.packages))
        with self.test.openBashOutFile() as bash:
            bash.write(self.template_bash(**template_data))
            bash.flush()
            testRun.raspberrypi.push(bash.name, '/tmp/script')

    # abstracted
    def get_timing_info(self, testRun, sync):
        """Get the timing info, as reported by the device during the test run."""
        out_file = testRun.wattlog_file + '_timing'

        with open(out_file, 'w') as f:
            # Put start time as first line
            f.write(str(sync) + '\n')
            print("SYNC ===>" + str(sync) + "<===")
            # Add rest of timing information
            f.write(testRun.raspberrypi.ssh('cat /tmp/timing', False))
