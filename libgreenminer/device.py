#
# Copyright (c) 2014 Wyatt Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import abc


class Device(object):
    __metaclass__ = abc.ABCMeta

    def prepare_for_test(self, arduino):
        """Implement to add actions to be executed before a test with the arduino"""
        pass

    @abc.abstractmethod
    def push(self, src, target):
        """Push a file (src) to a location (target) on the phone."""
        raise Exception("Abstract Method not implemented")

    @abc.abstractmethod
    def dump_info(self):
        """Returns a dict with lots of info about the phone and
            the current environment."""
        raise Exception("Abstract Method not implemented")

    @abc.abstractmethod
    def build_test(self, test_file):
        """Builds a test from the test_file"""
        raise Exception("Abstract Method not implemented")

    @abc.abstractmethod
    def shell(self, command, async=False):
        """Runs a shell command"""
        raise Exception("Abstract Method not implemented")
