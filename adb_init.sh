#!bin/bash
echo 192.168.35.{1..254} | xargs -P256 -n1 ping -s1 -c1 -W1 | grep ttl
arp -a > arp.txt
while read line
do
  set ${line}
  ques=${1}
  ip=${2}
  at=${3}
  mac=${4}
  #echo "[ip]${ip} [mac]${mac}"
  if [ $mac = "70:4d:7b:3d:26:43" ]; then
    break
  fi
done < ./arp.txt
#echo ${ip:1:10}
length=${#ip}-2
adb tcpip 5555
adb connect ${ip:1:$length}:5555
sed  -i  "s/serial\": \".*\"/serial\": \"${ip:1:$length}:5555\"/" ~/green-miner/devices.json
echo "power off port 4"
sudo hub-ctrl -h 0 -P 2 -p 0
echo 'chmod /dev/ttyACM0'
sudo chmod 777 /dev/ttyACM0
