import sys

EVENT_PATH = "/dev/input/event1"

def tapnswipe_to_emu(line):
    """
    Changes tapnswipe taps to taps that will work on emulator.
    Eg. A tap... 
    tapnswipe version: tapnswipe /dev/input/event1 tap 298 437  
    emulator version: input tap 298 437
    Eg. A swipe...
    tapnswipe version: tapnswipe /dev/input/event1 swipe 505 1135 441 299 
    emulator version: input swipe 505 1135 441 299

    If the event is a long press, then it will be converted into a swipe
    since input tap doesn't take a duration option. The swipe will not 
    change coordinates and will have a duration of 2 seconds.
    Eg.
    tapnswipe /dev/input/event1 tap 298 437 1000000
    turns into...
    input swipe 298 437 298 437 2000
    """
    bits = line.split()
    new_line = "input"
    for bit in bits[2:]:
        new_line += " " + bit 

    # check if it's a long press
    if len(bits) == 6 and "tap" in new_line:
        new_line = "input swipe " + bits[-3] + " " + bits[-2] + " " +\
                    bits[-3] + " " + bits[-2] + " 2000"

    return new_line + "\n"

def emu_to_tapnswipe(line):
    """
    Changes tapnswipe taps to taps that will work on emulator.
    Eg. A tap... 
    emulator version: input tap 298 437
    tapnswipe version: tapnswipe /dev/input/event1 tap 298 437  
    Eg. A swipe...
    emulator version: input swipe 505 1135 441 299
    tapnswipe version: tapnswipe /dev/input/event1 swipe 505 1135 441 299 
    
    If the event is a long press, i.e. a swipe with a duration, then it will 
    be converted into a tapnswipe tap. 
    Eg.
    input swipe 298 437 298 437 2000
    turns into...
    tapnswipe /dev/input/event1 tap 298 437 2000000
    """
    bits = line.split()
    new_line = "tapnswipe " + EVENT_PATH

    # check if it's a long press
    if len(bits) == 7 and bits[2] == bits[4] and bits[3] == bits[5]:
        new_line += " tap " + bits[2] + " " + bits[3] + " 2000000"
    else:
        for bit in bits[1:]:
            new_line += " " + bit 

    return new_line + "\n"

def handle_template(line, reverse=False):
    """
    Handles the {{{timing}}} template lines. If reverse is set to True,
    converts from emulator to tapnswipe. Else, converts from tapnswipe to
    emulator.

    Eg. Tapnswipe to emulator:
        {{{timing}} ===> #{{{timing}}}

    Eg. emulator to tapnswipe:
        #{{{timing}}} ===> {{{timing}}}
    """
    if reverse:
        return(line[1:])
    else:
        return("#"+line)

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('''Missing arguments: \n 
            $ python change_script.py <input_file> <output_file> [-r]
              if -r is NOT set, will convert from tapnswipe to emulator style
              if -r is set, will convert from emulator style to tapnswipe
              <input_file> and <output_file> should be bash scripts''')
        sys.exit()
    f_out = open(sys.argv[2], 'w')

    try:
        with open(sys.argv[1]) as f_in:
            for line in f_in:
                if len(line.strip()) > 0 and line.strip()[0] == "#":
                    f_out.write(line)
                elif "tap" in line or "swipe" in line:
                    if len(sys.argv) == 4 and "-r" in sys.argv:
                        f_out.write(emu_to_tapnswipe(line))
                    else:
                        f_out.write(tapnswipe_to_emu(line))
                elif "{{{" in line:
                    f_out.write(handle_template(line, "-r" in sys.argv))
                else:
                    f_out.write(line)
            f_in.close()

        f_out.close() 
    except IOError as e:
        print(str(e))
        sys.exit() 
