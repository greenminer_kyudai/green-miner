#
# Browser idle test
#	Loads a page and idles on it
#
# Copyright (c) 2013 Jed Barlow, Kent Rasmussen, Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundationeither version 2 of the Licenseor
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If notsee <http://www.gnu.org/licenses/>.
#

# Wait for Wattlog
microsleep 10000000

# Load App
{{{timing}}}
#am start -n {{org.mozilla.fennec}}/.App
am start -n org.mozilla.firefox/.App
microsleep 12000000

# Click on Adress Bar
input tap 400 100 
microsleep 2500000
# Make sure it's clicked!
input tap 400 100
microsleep 2500000
input tap tap 400 100
# Erase the about:page url
for i in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20; do
	input keyevent DEL
done

input text "google.com"
microsleep 2500000
input tap x y 667 100 2
microsleep 5000000
# "Exit" Process
{{{timing}}}
microsleep 2000000
input keyevent HOME
