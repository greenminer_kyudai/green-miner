import unittest
from convert_script import *

class TestConvertScript(unittest.TestCase):

    def setUp(self):
        pass
    
    def test_emu_to_tapnswipe(self):
        """
        Tests converting lines with taps and swipes from emulator friendly 
        style to tapnswipe
        """
        lines = ['input tap 366 794',
                 'input swipe 504 292 526 710 # scroll up']
        expected = ['tapnswipe /dev/input/event0 tap 366 794\n',
                    'tapnswipe /dev/input/event0 swipe 504 292 526 710 # scroll up\n']
        for line, exp_line in zip(lines, expected):  
            self.assertEqual(emu_to_tapnswipe(line), exp_line)

    def test_tapnswipe_to_emu(self):
        """
        Tests converting lines with taps and swipes from tapnswipe style to 
        emulator friendly style
        """
        lines = ['tapnswipe /dev/input/event0 tap 366 794',
                 'tapnswipe /dev/input/event0 swipe 504 292 526 710 # scroll up']
        expected = ['input tap 366 794\n',
                    'input swipe 504 292 526 710 # scroll up\n']
        
        for line, exp_line in zip(lines, expected):  
            self.assertEqual(tapnswipe_to_emu(line), exp_line)

    def test_emu_to_tapnswipe_longpress(self):
        """
        Tests converting a longpress event from emulator friendly 
        style to tapnswipe
        """
        line = "input swipe 504 292 504 292 2000"
        expected = "tapnswipe /dev/input/event0 tap 504 292 2000000\n"
        self.assertEqual(emu_to_tapnswipe(line), expected)

    def test_tapnswipe_to_emu_longpress(self):
        """
        Tests converting a longpress event from tapnswipe style to 
        emulator friendly style
        """
        line = "tapnswipe /dev/input/event0 tap 504 292 2000000"
        expected = "input swipe 504 292 504 292 2000\n"
        self.assertEqual(tapnswipe_to_emu(line), expected)

    def test_handle_template(self):
        """
        Tests converting lines with {{{timing}}}
        """
        line = "{{{timing}}}"
        expected = "#{{{timing}}}\n"

        # From tapnswipe to emu
        self.assertEqual(handle_template(line), expected)

        line = "##{{{timing}}}"
        expected = "{{{timing}}}\n"

        # From emu to tapnswipe 
        self.assertEqual(handle_template(line, True), expected)


if __name__ == '__main__':
    unittest.main()
