#!/usr/bin/env perl


use strict;
use warnings;

die("Requires one arguement, a test directory for an android app <test>/{script.sh,duration}") if (@ARGV != 1);
# ex usage: tests % perl ../test_info.pl shaiful_pinball_energy/shaiful_pinball
# ex output: Difference in duration versus microsleeps was 28 seconds

my $script_name = $ARGV[0] . "/script.sh";
my $duration_name = $ARGV[0] . "/duration"; 

open(my $sfh, '<:encoding(UTF-8)', $script_name) or
    die("Failed to open file: $script_name due to error $!");

open(my $dfh, '<:encoding(UTF-8)', $duration_name) or
    die("Failed to open file: $duration_name due to error $!");

my @duration_content = <$dfh>;
close($dfh) or die("Failed to close: $duration_name");
print "Are you sure this is the duration file? It has multiple lines.\n" 
    if @duration_content > 1;

my $duration_time = $duration_content[0];

my $microsleeps = 0;
while (<$sfh>) {
    chomp($_);
    # go through $script_name and sum the number of microsleep durations
    next if $_ !~ /microsleep/gi;
    my @line = split(' ', $_);
    $microsleeps += $line[1];
}
close($sfh) or die("Failed to close $script_name");

$ microsleeps /= 1e6;
print "Difference in duration versus microsleeps was " . ($duration_time - $microsleeps) . " seconds\n";

