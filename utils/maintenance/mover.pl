#!/usr/bin/perl
use strict;
use DateTime::Duration;
use DateTime;

# Purpose: make commands to move files in the data directory
#          into the appropriate sub directories (per day)
#          Move stuff older than 90 days
#
#          Note: does not execute the commands, just produces a shell script
#                you have to execute the output
#
# usage: perl mover.pl | sh -x

my $basedir = "/var/uploads/data";
my $oldest = 90;
sub mv_older_files {
	my $pattern = "$basedir/*Z.tar.gz";
	my @files = glob $pattern;
	return map { cmd_mv_file_to_day($_) } grep { is_days_old( $oldest, $_ )  } @files;
}

sub cmd_mv_file_to_day {
	my ($filename) = @_;
	if ($filename =~ /\.(\d{8})T\d{4}Z\.tar\.gz$/) {
		my $day = $1;
		my $dir = $basedir . "/" . $day;
		my $cmd = "mkdir $dir; mv \'$filename\' $dir/";
		return $cmd;
	}
	die "Not an appropriate file $filename!";
}

sub is_days_old {
	my ($days, $filename, $now) = @_;
	if ($filename =~ /\.(\d{8})T\d{4}Z\.tar\.gz$/) {
		my $day = $1;
		my ($y,$m,$d) = ($day =~ /^(\d\d\d\d)(\d\d)(\d\d)$/);
		$now = ($now)?DateTime->from_epoch( epoch => $now ):DateTime->now();
		my $then = DateTime->new( year => $y, month => $m, day => $d);
		my $oldest = $now->subtract_duration( DateTime::Duration->new( days => $days ) );
		return (DateTime->compare($then, $oldest) <= 0);
	}
	die "Not an appropriate file $filename!";
}

sub test() {
	my $oldexample = "/var/uploads/data/firefox.2012-08-13_f89feda9d997.reading.A.OrangeBasketOne.20140129T2031Z.tar.gz";
	my $newexample = "/var/uploads/data/firefox.2011-09-05_2bb8c0b664cf.idle.A.firefoxIdlePowerRun.20140424T1409Z.tar.gz";
	my $now = 1400884841;
	my $commands = cmd_mv_file_to_day($oldexample);
	die "Bad Commands $commands" unless $commands eq "mkdir /var/uploads/data/20140129; mv \'$oldexample\' /var/uploads/data/20140129/";	
	
	die "old $oldexample is not old?" unless is_days_old(90, $oldexample);
	die "old $newexample is old?" if is_days_old(90, $newexample);
}

test();
print $_.$/ for mv_older_files();
