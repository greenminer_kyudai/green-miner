\documentclass[10pt,times]{article}
%\usepackage{graphicx,fullpage,hyperref}
\usepackage{graphicx,hyperref,comment}


\topmargin 0pt
\advance \topmargin by -\headheight
\advance \topmargin by -\headsep
     
\textheight 8.9in
     
\oddsidemargin 0pt
\evensidemargin \oddsidemargin
\marginparwidth 0.5in
     
\textwidth 6.5in
     

\hypersetup{colorlinks=false}
%\newcommand{\url}[1]{#1}
\usepackage{fancyhdr,xspace,comment}
\pagestyle{fancy}
\newcommand{\thetitle}{Green Mining and Mobile App Energy Ratings}
%Green Mining on Mobile
%  Platforms: The Power Consumption of Software Change}
\title{\thetitle}
\author{Abram Hindle}
\rfoot{Page \thepage~of~5}
\cfoot{}
\lhead{\thetitle}
\lfoot{}
\rhead{\textbf{Abram Hindle}}

\newcommand{\Section}[1]{
\vspace{0.5em}
\noindent{\large\textbf{#1}}
\vspace{0.5em}}


% if desperate
%\usepackage[top=tlength, bottom=blength, left=llength, right=rlength]{geometry}



\begin{document}

\vspace*{0em}

\begin{abstract}
  \input{abstract.txt}
\end{abstract}
    % Problem Statement: What is the problem addressed by the proposal, and why is it important? What is the potential contribution to the field of the project if successful? Cite relevant work in the field as appropriate.
    % Expected Outcomes: What tangible assets will be created or produced as a result of the proposed project? How will the results of this project be disseminated to others?
    % Related Research: Briefly summarize the current state of the art in this field, including references where appropriate.
    % Benefits to the community: Include a statement of the profile of the community served by the proposal and how the project addresses issues and challenges in that community.
    % Schedule: What milestones will be used to measure progress of the project during the year and when will they be completed? If the project described is part of a larger ongoing research program, estimate the time for completion of this project only.
    % Qualifications of principal investigator: For the PI, include a brief description of any relevant prior research, publications, or other professional experience. A detailed CV or list of publications is not required; you may include a personal webpage address instead.
    % Evaluation: How will the results of this project be evaluated?
    % Use of Microsoft technologies: Describe the Microsoft tools and technologies to be used in this project. If software is to be developed, give details of the tools to be used, the number of software developers, and the proposed timescale. Does the software to be developed require the incorporation of code from commercial or public-domain libraries? If so, please provide details. (Note that successful proposals are not required to use Microsoft tools or technologies.)
    % Use of funds: Provide a budget (in U.S. dollars) describing how the award will be used. The budget does not have to be detailed, and should be presented as a table with the total budget request clearly indicated.
    % Other support: Include other contributions to this project (cash, goods, and services), if any, but do not include the use of university facilities that are otherwise provided on an ongoing basis. Note: Authors of winning proposals will be required to submit an original letter on department letterhead certifying the commitment of any additional or matching support described in the proposal.

%\begin{abstract}
%\end{abstract}

\Section{Problem Statement}


Limited battery-life is a fact of life for smart-phone and mobile
device users. Battery-life limits the availability and reliability of
the consumer's device; if a smart-phone has no battery left it cannot
even be used to make calls.

What can consumers do? In most cases they have already purchased a
mobile device and they are stuck with that hardware. They can update
the OS but they do not have a lot of control on the system side. But
what they can do is alter their behaviour or their choice in
applications. For instance watching a video or reading a PDF in a
feature complete client can use more power than a minimalist application
or an application focused on power consumption performance, thus the user
could opt for the application that uses power when they are in a power
limited context (such as during transit or at a cafe).

Unfortunately if the user is going to optimize their own power
consumption of their mobile device they are going to have to either
measure their own applications or find measurements of their own
applications. Already in both cases, this is beyond the abilities of
the majority of smart-phone users. Thus we suspect we will have to
provide this information to the user so they can make a decision
themselves.

Thus we want to be able to rate apps based on their power
performance. This can be done by bench-marking tasks and then comparing
the power consumption of the same task run across multiple against
each other. We can implement a naive implementation of task-fulfilling
app and use that as a base-line for the app-tests.

In order to execute this kind of testing we will leverage our work on
the the effect of software change on software power consumption, as
demonstrated by in Figure \ref{fig:ffwatt} and our prior
work~\cite{greennier,greenmsr}.

Thus we propose to apply \emph{Green Mining}~\cite{greennier}, the
power consumption analysis of software repositories (version control)
and versioning via dynamic analysis (use-case tests), to software on
mobile platforms, such as Windows Phone and apps associated with that
platform.  Mobile platforms have different characteristics compared to
desktop computers and laptops, thus they need special
attention. Battery life-time is also a serious concern for mobile
platforms as it limits availability, a quality very important in a
phone.

In this proposal we plan to get samples of multiple kinds of mobile
applications, produce test cases to exercise these mobile applications
and then run and measure these test cases, thus allowing apps to be ranked by
efficiency. By doing so we can create an Energy Star style rating for
apps, allowing consumers to make decision in times of dire battery
levels, or during their app purchase process.

This work has a potential environmental impact: deployed software will
consume less resources and thus have a direct reduction on the $CO_2$
emissions~\cite{Easterbrook}. % produced by electrical power generation.
This work is industrially relevant because vendors, such as Microsoft,
Intel, and IBM, have already begun investing in
power-management educational
documentation~\cite{MicrosoftPerformance} and
tools~\cite{MSRPowerPaper,LessWatts,ibm}.

We expect that this kind of user-facing task-oriented power
consumption bench-marking will benefit consumers and pressure
3rd party vendors to produce applications that are
power-consumption aware. If consumers can choose, they can make smart
decisions in terms of the power consumption of their applications.


\Section{Expected Outcomes}

    % - Expected Outcomes - tangible assets will be created or
    %   produced as a result of the proposed project? How will the
    %   results of this project be disseminated to others?
    %  - data arising from this work will be released

The outcomes of this work include :
      \begin{itemize}
      \item A design, and implementation of a test-bed, both hardware
        and software, for measuring power consumption of software
        undergoing run-time tests on a mobile platform.
      \item A corpus of naive task implementations, tests for these
        implementations and tests for various apps available to us.
      \item A corpus of test results running task oriented user-tests
        on apps.
      \item A method of rating many apps by their power consumption
        efficiency in fulfilling a task.
      \end{itemize}


\Section{Related Research}

\newcommand{\apara}[1]{\textbf{#1}:\xspace}

\apara{Measurement} Power measurement and modelling is industrially
relevant and a concern of multiple
vendors\cite{MicrosoftPerformance,MSRPowerPaper,LessWatts}.
Consumer interest in power management is demonstrated by the
Linux-oriented Phoronix site that has bench-marked power usage of Linux
distributions~\cite{phoronix}.  Measuring the power usage of software
changes relies on dynamic analysis to measure running systems.

In terms of benchmarking and monitoring that is relevant, Gurumurthi
et al.~\cite{machinesim} and Amsel et al.~\cite{greentracker} have
produced tools that simulate a real systems power usage, and benchmark
individual application power usage.  Gupta et
al.~\cite{MSRPowerPaper}, from Microsoft Research, describe a method
of measuring and correlating the power consumption of applications on
Windows Phone 7.  %In terms of fine grained modelling,
Tiwari et al.~\cite{instructionpoweranalysis} modelled the power
consumption of individual CPU instructions.

Power usage advice will be related to the power consumption of
peripherals.  Lattanzi et al.~\cite{wifi} have modeled the power usage
of WiFi adaptors, while Greenwalt~\cite{powerHD} measured and modelled
the power consumption of hard-drives.

\apara{Optimization} Measuring power consumption is not enough, acting
on it is necessary. Relevant to power consumption patterns,
Fei et al.~\cite{sourcecodetransformation} have
tuned source code transformations and compiler optimizations to reduce
power-use via context awareness and avoiding the memory bus.

\apara{Mining Software Repositories (MSR)} This research proposal is about
combining mining software repositories research~\cite{MSR} and power
consumption research.  MSR provides us with the software changes we
need to fulfill our objectives.  MSR has been used to mine links
between development artifacts, predict defects, and model developer
behaviour, but it has not been used to reason about power consumption
or performance.  Researchers such as Poshyvanyk, Marcus, Linstead and
Thomas have leveraged information retrieval techniques in order to
query, relate and summarize activities from these large
datasets~\cite{Poshyvanyk2007,Marcus2005,Linstead2007,ThomasICSE}.
Hindle et al.~\cite{greennier,greenmsr} discuss \emph{Green Mining},
a mixture of MSR combined with power consumption benchmarking.
The measurement of software changes impact on power usage has not been
covered in MSR literature.

These articles demonstrate the interest in power consumption
caused by software in industry and academia.  \emph{Green mining}
demonstrates novelty by combining MSR research and power
consumption.




\Section{Benefits to the Community}

    % - Benefits to the community     %   – Include a statement of the profile
    %   of the community served by the proposal and how the project
    %   addresses issues and challenges in that community.

This work has a chance to impact our environment by enabling an
optimization of the use of electrical energy. Reduction in electrical
consumption through greater efficiency or careful conservation would
result in less emissions of $CO_2$ due to energy
consumption~\cite{costs}.  \textbf{We can do more with less.}  
This work would be valuable to end users as it puts pressure on
developers to be aware of power consumption related issues.
 \emph{Thus this work should
  be novel yet broad enough to have an impact within the venues it is
  published in and beyond.}  The students trained during this research
will industrially relevant to mobile and server-based companies like
Microsoft.


\Section{Schedule}

    % - Schedule 
      % – What milestones will be used to measure progress of the project
      % during the year and when will they be completed? If the project
      % described is part of a larger ongoing research program, estimate the
      % time for completion of this project only.

Over the period of a year there will be 3 main milestones, each taking
1 term (4 months): \emph{mobile device test-bed}, \emph{candidate software apps
and tests}, and \emph{test analysis}. Each milestone will be worked on by
myself and 1 or 2 undergraduate or graduate research assistants.

The mobile device test-bed will instrument the recharging cable of the
phone power adapter (as to avoid nasty battery fires) of a phone and
provide a method of measuring, recording and storing test power usage
data. The mobile device test-bed will also enable the instantiation of
tests.

The candidate software apps and tests will be a collection of
 software apps, combined with tests which
simulate real-world usage of the software. These tests will be run, 
repeated, and measured so we can compile stable statistics about the
test-runs in terms of power consumption. We will also develop some
test software allowing us to artificially exercise and measure some
hardware/software power consumption performance. Furthermore for each
task we identify we have to make a naive implementation and benchmark that.

The test analysis milestone will aggregate and analysis this test data
in order to investigate reasons for power usage (software or
hardware). Further more test analysis will result in a compiled
data-set of tests that will be publicly released.




      % - instrumented phone
      %   2 months in
      % - replicate instrumented phone
      %   - 3 months in
      % - produce tests
      %   - 4 months in
      % - produce data sets
      %   - 5 months in
      %     ...

\Section{Qualifications}

Although \emph{green mining}~\cite{greennier,greenmsr} is a new area, my prior work provides a solid
foundation.  I worked on my PhD with Prof. M. W. Godfrey and Prof. R.
C. Holt at the University of Waterloo from 2005 to graduation in 2010.
I then did post-doctoral work with Prof. P. Devanbu and Prof. Z. Su at
the University of California, Davis. In July to September 2011, I was
a visiting researcher to Microsoft Research in Redmond, USA where I
worked with the former Empirical Software Engineering group: N. Nagappan,
T. Zimmerman and C. Bird. Now I am an assistant professor at the
Department of Computing Science within the University of Alberta.

Over the past 8 years I have published 2 journal papers, with 2 in
submission, and 17 refereed conference papers at conference such as
FSE, ICSE, MSR, ICSM, WCRE, ICPC, SCAM, METRICS.  This past year, in terms
of top tier and top tier sub-area conferences, I published papers at
FSE2011, ICSE: NIER track 2011, ICSM 2010, and 3 papers at MSR2011.

\emph{Mining Software Repositories} (MSR) is a field of research~\cite{MSR} that
focuses on extracting, recovering, and reasoning about information
mined from software repositories such as version control systems, issue
tracker systems, and mailing-list archives.
My MSR work is relevant to this research agenda
as I analyze revisions to software in many of my papers
\cite{hindle09sciprog,hindle09icpc,Hindle:2011:ATN:1985441.1985466}.
% [J3,C4,C8,C10].

\emph{Empirical Software Engineering} is research that uses evidence
and existing software systems to test theories.  I have published
evidence-based work
about the nature of defects in software repositories~\cite{Posnett:2011:GIN:2086197.2086252,Rahman:2011:BIH:2025113.2025157}. This
agenda relies on modelling techniques from this field.


\emph{Green Mining and Software Power Consumption} Figure
\ref{fig:ffwatt} is taken from our  
ICSE NIER paper 2012 called ``Green Mining: Investigating Power Consumption
Across Versions''~\cite{greennier} and our Green Mining MSR 2012 paper~\cite{greenmsr}. We measured over 509 versions of Firefox executing
the same browsing test and we were able to see a progression across
time of power consumption. In these tests we showed the Mozilla's
Firefox was becoming more power efficient as it matured.

\Section{Evaluation}

In general, our work will be evaluated via peer review in terms of
conference and journal publications describing this research. In terms
empirically measurable of goals, we wish to produce a set of benchmark
tasks with naive implementations that allow us to design and benchmark
apps that do similar tasks (video playing, image browsing, text
composition, etc.). This benchmark will allow one to rate apps by
their power efficiency fulfilling a task, for tasks we have implemented.


\Section{Use of Microsoft Technologies}
    % - Use of Microsoft technologies --- Describe the Microsoft tools and
    %   technologies to be used in this project. If software is to be
    %   developed, give details of the tools to be used, the number of
    %   software developers, and the proposed timescale. Does the
    %   software to be developed require the incorporation of code from
    %   commercial or public-domain libraries? If so, please provide
    %   details. (Note that successful proposals are not required to use
    %   Microsoft tools or technologies.)

We plan to test Windows Phone devices. For development we expect to
use .NET and Visual Studio for some development.
We also have hardware for AC and DC power measurement.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{firefox-tests-embed}
  \caption{This is a graph of the distributions of mean wattage (power use) of
    different versions of Firefox~\cite{greennier} running the same
    browsing web-pages use-case test. The green-blue area is the range between
    the minimum and maximum mean wattage for that version. The red
    line is the mean wattage and the box-plots depict the distribution
    of mean wattage per test per Firefox version. Note this plot depicts over 509
    builds of Firefox 3.6 from alpha to stable versions. The dotted
    line with a negative slope is a line of best fit on the means; its
    slope indicates a decrease in power use across versions.
    The ranges for the earlier versions are smaller because we tested
    less instances of each of the earlier versions.
  }
  \label{fig:ffwatt}
\end{figure}


        
\Section{Use of Funds}
    % - Use of funds --- Provide a budget (in U.S. dollars) describing how
    %   the award will be used. The budget does not have to be detailed,
    %   and should be presented as a table with the total budget request
    %   clearly indicated.
      
%      2 under grad software developers

The funds will be used to hire and train undergraduates and graduate students who will
implement some of the hardware and software that is not made. These
students will also design use-case tests for the mobile
applications that allow them to test the power consumption of software
under real-world usage conditions. The students will also work
on the data-mining technology that parses inchoate source code
snippets from differences and patches producing features describing
the change. These features are then related to changes in power
consumption.


Graduate and undergraduate Research Assistants cost about $\$8000$ CAD/USD for a 4
month term.  In terms of equipment for power measurement, we can already
measure both AC and DC power
consumption of a device.  In terms of equipment for testing, we
will need either Windows Phones or Qualcomm dragonboard development
kits (currently $\$498.00$ at
Bsquare~\footnote{\url{http://store.bsquare.com/catalog/index.cfm?fuseaction=product\&theParentId=160\&id=2786}}).
Thus 3 undergraduate research assistants ($\$24000$) and 2 devices
(Windows Phone and a Qualcomm Dragonboard, $\$996$) add up to
\textbf{\$24996} in requested support.

\Section{Other Support}

I have a start-up grant, granted 2 years ago, used for equipment, travel and
graduate student funding, which can be used to cover any excess wages
for the undergraduates that are not covered by this award.

\vspace{-1.0em}

\bibliographystyle{abbrv}
%\bibliography{partII}
%\bibliographystyle{IEEEtran}
\bibliography{seif.bib}




\end{document}

