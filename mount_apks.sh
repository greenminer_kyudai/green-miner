#!/bin/bash
#
# Mounts folders from remote server to local device
#
# usage: ./mount_apks.sh
#
# Copyright (c) 2013 Alex Wilson, Kent Rasmussen, Wyatt Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

function configValue {
	CONFIG_STRING=""
	until [ -z "$1" ]; do
		CONFIG_STRING=$CONFIG_STRING"['$1']"
		shift
	done
	python3 -c "import json, os; print(os.path.expanduser(json.loads(open('config.json').read())"$CONFIG_STRING"))"
}

# Pull config from json
IMAGE_FOLDER=`configValue images_folder`
TARBALLS_FOLDER=`configValue tarballs_folder`
SERVER=`configValue server`
USERNAME=`configValue scp username`
APKS_FOLDER=`configValue scp apks_folder`
REMOTE_TARBALLS_FOLDER=`configValue scp tarballs_folder`

# Defines the mount_folder function used to mount a folder
# 
# mount_folder MOUNT_LOCAL MOUNT_REMOTE
# 
# @param MOUNT_LOCAL the local mount point (must already exist)
# @param MOUNT_REMOTE the remote mount point (must already exist)
function mount_folder {
	MOUNT_LOCAL=$1
	MOUNT_REMOTE=$2

	# Create folders if not there
	if [[ ! -d $MOUNT_LOCAL ]] ; then
		mkdir -p $MOUNT_LOCAL
	fi

	# Mount using sshfs
  # you have to ssh-add before calling this 
	#sshfs -o idmap=user -o IdentityFile=/home/greenminer/.ssh/id_rsa ro "$USERNAME"@"$SERVER":"$MOUNT_REMOTE" "$MOUNT_LOCAL" > /dev/null
	sshfs -o idmap=user "$USERNAME"@"$SERVER":"$MOUNT_REMOTE" "$MOUNT_LOCAL" > /dev/null
	# Get return
	mount_succesfull=$?

	# Check succesfull mount
	if [[ $mount_succesfull == 0 ]] ; then
		# Mount was succesfull
		echo "Mounted "$USERNAME"@"$SERVER":"$MOUNT_REMOTE" "$MOUNT_LOCAL
	else
		# Mount failed
		echo "Failed Mounting "$USERNAME"@"$SERVER":"$MOUNT_REMOTE" "$MOUNT_LOCAL"!"

		# Fail miserably on failed mount
		exit 2
	fi
}

# Check if already mounted
mount -v | grep "$USERNAME"@"$SERVER" > /dev/null
# Get return
is_mounted=$?
echo $is_mounted

# Check if mounted
if [[ $is_mounted != 0 ]] ; then
	# Is not mounted, try and mount

	# Mount APK folder
	mount_folder $IMAGE_FOLDER $APKS_FOLDER

	# Mount Tarball folder
	mount_folder $TARBALLS_FOLDER $REMOTE_TARBALLS_FOLDER

	echo "All mounts succesfull!"
else
	echo "Already mounted!"
fi
