# Arduino Watt Logger (INA169)
The INA169 can maeasure the voltage drop across shunt resistor and report it over serial.
We used an [INA169(sparkfun)](https://www.sparkfun.com/products/12040) to measure the power usage of our devices.

## Schematic
![ina169_circuit](./Graphics/ina169_circuit.png)
- **GND** should be connected to ground of the circuit you are trying to maeasure
- **VIN+** needs to be connected to the positive side of the source (e.g battery)
- **VIN-** needs to be connected to the positive side of the load (e.g. positive side of an LED)
- **VOUT** is the measured output and should be connected to something that measures voltage levels, such as a multimeter or Arduino ADC pin.
- **VCC** is the supply power to the INA 169, which needs to be connected 3.3V, 5V, etc. Note that the VOUT range depends on the voltage supplied by VCC.

The INA169 connects analog pins of arduino, and has its own internal registers. The original RS resister has 10 ohm, and current sense range is between 3.5 mA and 35 mA. So we need to modify RS resister(10 ohm to 0.1 ohm) and the current sense range is between 350 mA and 3.5 A.

## INA169 Tuning

### LED Test circuit(ina169)
![ina169_LED_test_circuit](./Graphics/ina169_LED_Testcircuit.png)

As shown in the diagram, connect the Arduino 5V to the INA 169 VCC and the Arduino GMD to the INA 169 GND. To read the output voltage level, we connect the Arfuino A0 to the INA 169 VOUT pin. Also, We used the Arduino A1 for measuring load voltage.

Because INA 169 can measure analog voltage and Arduino reads analog value, we need to return to real voltage.

```C
sensorValue = analogRead(SENSOR_PIN);              // SEENSOR_PIN = A0
sensorValue = (sensorValue * VOLTAGE_REF) / 1023;
current = sensorValue / (10 * RS);                 // RS = 10ohm
```

`analogRead()` reads analog value from Arduino analog pin. This analog value is 0 ~ 1023, so we remap the value into a voltage number (5V reference).

A bit of math is needed to convert to the source current (Is).
$$Is = \frac{VOUT * 1kΩ}{RS * RL}$$
- **Is** is the current we want to measure.
- **VOUT** is the voltage we measured at the output of the INA169.
- **1kΩ** is a constant resistance value we need to include due to the internals of the INA169.
- **RS** is the value of the shunt resistor. if you do not modify the board, then this is set at 10Ω. LED test circuit needs 10Ω resistor.
- **RL** is the value of the output resistor. if you do not modify the board, then this is set at 10kΩ.

We try to have Arduino read more than 2000 readings per second, so we have Arduino sent data by packet. Each Data size is 2 byte and total packet length is 24 byte, which contains header, data (10) and tail.

```
ff | DATA_SIZE | ## | ## | ## | ## | ## | ## | ## | ## | ## | ## | ff | ff |
```

Arduino can send packets to Raspberry pi with the below code.
```C
void sendPacket(){
  const int DATA_SIZE = 10;
  const int BUFFER_SIZE = DATA_SIZE * 4 + 4;
  byte buffer[BUFFER_SIZE];

  buffer[0] = 0xff;
  buffer[1] = DATA_SIZE;
  for(int i = 0; i < DATA_SIZE; i++){
    int val_A0 = getRawSensorValue(A0);
    int val_A1 = getRawSensorValue(A1);
    buffer[2 + i * 4]     = val_A0 >> 8;
    buffer[2 + i * 4 + 1] = val_A0;
    buffer[2 + i * 4 + 2] = val_A1 >> 8;
    buffer[2 + i * 4 + 3] = val_A1;
  }
  buffer[2 + DATA_SIZE * 4] = 0xff;
  buffer[2 + DATA_SIZE * 4 + 1] = 0xff;

  Serial.write(buffer, BUFFER_SIZE);
}
```

Raspberry pi can accept packets and convert to voltage. This code is used for test and calibration of ina169_circuit.
```python
import serial
import time
import numpy as np

def transform_amperage(value_A0):
    RS = 0.1
    REF_VOLTAGE = 5.0
    value_A0 = int.from_bytes(value_A0, byteorder='big')
    sensorValue = (value_A0 * REF_VOLTAGE) / 1023.0
    current = sensorValue / (10 * RS)
    return current * 1000

def transform_voltage(value_A1):
    REF_VOLTAGE = 5.0
    value_A1 = int.from_bytes(value_A1, byteorder='big')
    # load_voltage is voltage between Vin+ ~ GND
    load_voltage = (value_A1 * REF_VOLTAGE) / 1023.0
    return load_voltage

def wattlog(amperage, voltage, time):
    wattage = voltage * amperage
    return ("%.3f,%.3f,%.3f,%.3f" %(time, amperage, voltage, wattage))

def main():
    m_A_array = np.array([])
    V_array = np.array([])
    with serial.Serial('/dev/cu.usbmodem1411', 115200, timeout=None) as ser:

        # flush buffer
        while ser.in_waiting > 1:
            ser.read(1)

        start_time = time.time()
        while True:
            elapsed_time = time.time() - start_time
            # read packet
            if ser.read(1).hex() == "ff":
                if ser.read(1).hex() == "ff":
                    if ser.read(1).hex() == "ff":
                        packet_length = int.from_bytes(ser.read(1), byteorder='big')
                        for i in range(packet_length):
                            rawValue_A0 = ser.read(2)
                            rawValue_A1 = ser.read(2)
                            amperage = transform_amperage(rawValue_A0)
                            voltage = transform_voltage(rawValue_A1)
                            m_A_array = np.append(m_A_array, amperage)
                            V_array = np.append(V_array, voltage)
                            print(wattlog(amperage, voltage, elapsed_time))
            if (elapsed_time > 10):
                print("readed 60s data")
                print("mA_median: " + str(np.median(m_A_array)))
                print("vol_median: " + str(np.median(V_array)))
                break

if __name__ == '__main__':
    main()
```
This ```transform_ma_ina169()``` and ```transform_v_ina169()``` is not optimized, so
we tuned test circuit and set *SLOPE* and *OFFSET*.
We test INA 169 readings with LED circuit, and result is below.
These values are for 60's readings and calculated median.
Based on these values, SLOPE and OFFSET are calculated by least squres method.

#### Load Voltage Test
Voltage | Expected value (V) | Measured Value (V)  
--  | --  | --
2.5 | 2.5 | 2.484  
3.0 | 3.0 | 2.984
3.5 | 3.5 | 3.481
4.0 | 4.0 | 3.984  

#### LED's Amperage Test
resister (ohm)| Expected value (mA) | Measured value (mA)
 -- | -- | --
50  | 12.4 | 12.268  
100 | 9.59 | 9.433  
200 | 6.62 | 6.500  
300 | 5.08 | 4.985  

We can set SLOPE and OFFSET. This SLOPE and OFFSET depends on the environment.

```python
def transform_amperage(value_A0):
    RS = 0.1
    REF_VOLTAGE = 5.0
    SLOPE = 1.0083
    OFFSET = 0.0718
    value_A0 = int.from_bytes(value_A0, byteorder='big')
    sensorValue = (value_A0 * REF_VOLTAGE) / 1023.0
    current = sensorValue / (10 * RS)
    return (current * 1000) * SLOPE + OFFSET

def transform_voltage(value_A1):
    REF_VOLTAGE = 5.0
    SLOPE = 0.9996
    OFFSET = 0.0178
    value_A1 = int.from_bytes(value_A1, byteorder='big')
    # load_voltage is voltage between Vin+ ~ GND
    load_voltage = (value_A1 * REF_VOLTAGE) / 1023.0
    return load_voltage * SLOPE + OFFSET
```

We applied this calibration to green-miner's code.

```python
def _transform_ma_ina169(self, value_A0):
    RS = 0.1
    REF_VOLTAGE = 5.0
    SLOPE = 1.0083
    OFFSET = 0.0718

    value_A0 = int.from_bytes(value_A0, byteorder='big')
    sensorValue = (value_A0 * REF_VOLTAGE) / 1023.0
    measured_current = sensorValue / (10 * RS)
    measured_current_ma = measured_current * 1000
    current_ma = measured_current_ma * SLOPE + OFFSET
    return current_ma
```
```python
def _transform_v_ina169(self, value_A1):
    REF_VOLTAGE = 5.0
    SLOPE = 0.9996
    OFFSET = 0.0178

    value_A1 = int.from_bytes(value_A1, byteorder='big')
    # load_voltage is voltage between Vin+ ~ GND
    measured_load_voltage = (value_A1 * REF_VOLTAGE) / 1023.0
    load_voltage = measured_load_voltage * SLOPE + OFFSET
    return load_voltage
```


<!-- memo
- [x] test で使用したプログラムの記載．（中央値を使ったこととか）
- [x] 最終的に最小二乗法で求めたことも書いておく．
- [] testコードと調整したコードの中身を同じように揃えておく
 -->
