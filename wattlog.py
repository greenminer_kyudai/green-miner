#!/usr/bin/env python3
# Records data from the arduino for logging power consumption information
#
# How to Use:
#	   ./wattlog device_id
#
# Sample Usage:
#	   ./wattlog A > data_for_A.csv
#
# Copyright (c) 2013 Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import argparse, signal, sys, time
import libgreenminer
import numpy as np

# Print everything on exit
def exit_handler(signum, frame):
	sys.stdout.flush()
	sys.exit()
signal.signal(signal.SIGTERM, exit_handler)

# Arguments
parser = argparse.ArgumentParser(description='Records data from the arduino for logging power consumption information.')
parser.add_argument('device', metavar='device', help='Device Letter (e.g. A)')
args = parser.parse_args()

# Setup Device
builder = libgreenminer.DeviceBuilder(args.device)
arduino = builder.build_arduino()
wattlog = libgreenminer.Wattlog(arduino)



arduino.flush_buffer()


print(wattlog.return_header())
start_time = time.time()

count = 0
time_ = time.time()


while True:
	try:
		wattlog.return_line(arduino.get_reading(), time.time() - start_time).strip()
		count += 1
		if(time.time() - time_ >= 1.0):
			print(count)
			count = 0
			time_ = time.time()
		#print(wattlog.return_line(arduino.get_reading(), time.time() - start_time).strip())
		if time.time() - start_time > 60:
			raise KeyboardInterrupt
	except KeyboardInterrupt:
		# print("ave: " + str(sumsum / count) + "count: " + str(count) + "sum: " + str(sumsum))
		# print("median: " + str(np.median(m_A_array)))
		# print("vol_median: " + str(np.median(V_array)))
		# print("avevol: " + str(sumvol / count))
		break
	except:
		# Empty or malformed reading
		continue
