#!/usr/bin/env python3
#
# Copyright (c) 2013 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import argparse, json, requests, sys
from libgreenminer import Queue

config = json.loads(open('config.json').read())

def unschedule(batches, tests, apps, versions):
	queue = Queue('android')

	for app_v, app_v_tests in queue.data:
		app, version = app_v.split(':')

		if app not in apps or version not in versions:
			continue

		for test, count in app_v_tests.items():
			test, batch = test.split('#')
			if batch not in batches or test not in tests:
				continue

			for i in range(count):
				print('unschedule {}:{} {}#{}'.format(app,version, test, batch))
				queue.report_finished(test, app, version, batch)

class Universe(object):
	def __contains__(self, x):
		return True

def make_set(data):
	if type(data) is str:
		return set([data])
	if data is None:
		return Universe()
	return set(data)

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Unschedule tests')
	parser.add_argument('--batch', help='limit to batchs', nargs='*')
	parser.add_argument('--app', help='limit to app', nargs='*')
	parser.add_argument('--version', help='limit to versions', nargs='*')
	parser.add_argument('--test', help='limit to tests')

	args = parser.parse_args()

	# let's not delete the entire queue if we don't get any args
	if [args.batch, args.app, args.version, args.test] == [None] * 4:
		parser.parse_args(['-h'])
		exit()

	unschedule(batches=make_set(args.batch),
			apps=make_set(args.app), tests=make_set(args.test),
			versions=make_set(args.version))
