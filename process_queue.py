#!/usr/bin/env python3
#
# Copyright (c) 2013 Alex Wilson, Jed Barlow
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import argparse, json, logging, os, random, requests, sys, time

from itertools import groupby
from operator import itemgetter
from subprocess import call, check_call, check_output, CalledProcessError
from libgreenminer import DeviceBuilder, StatusUpdater, Queue

from run_test import run_test
import error_reporting

config = json.loads(open('config.json').read())


def git_update(device_id):
    while True:
        try:
            # get new commits from the server
            check_call('git fetch'.split())

            # See if anything's new
            new_commits = check_output('git log HEAD..origin/master --oneline'.split())

            # If something's new, update!
            if new_commits.decode('UTF-8').strip():
                StatusUpdater(device_id).updating()

                check_call('git pull --rebase'.split())
                print('update complete: restarting')

                # close arduino serial port before restarting
                if arduino and arduino.serial:
                    arduino.serial.close()
                # replace the current process with an up-to-date one
                os.execl('./process_queue.py', './process_queue.py',
                        device_id)
            else:
                return
        except OSError:
            print("failed to restart")
        except CalledProcessError:
            print("git command failed, try again in 60s.")
            time.sleep(60)

def fetch_apk(app, version):
    while True:
        ret = call(['./mount_apks.sh', app, version])
        if ret == 0:
            return True
        elif ret == 2:
            print("*** Error: Could not mount APK (Remote Down?). Trying again in 10 seconds.")
            time.sleep(10)
        else:
            print("*** Error: Mount APKs Threw Unknown Error")
            exit(1)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process the queue')
    parser.add_argument('device', help="device id")

    args = parser.parse_args()

    logging.basicConfig()
    logging.getLogger().addHandler(error_reporting.LogHandler(args.device))

    builder = DeviceBuilder(args.device)
    device = builder.build_device()
    arduino = builder.build_arduino()

    queue = Queue(device)

    # Basic test/run-picking concept:
    # Pick an app/version, try to run all of the tests once,
    # but refresh the queue after each run, because another
    # device may be running the same app/version.
    while True:
        # update the queue, status, git repo....
        for check in queue.queue_waiter():
            # this loop runs ~ once every 60 seconds
            StatusUpdater(arduino.deviceid).polling()
            git_update(arduino.deviceid)

        # make a copy of batches that we won't update
        # until we've done a test from each
        batches = queue.batches[:]
        random.shuffle(batches)

        for batch in batches:
            app, version, test = queue.get_random_test_from_batch(batch)
            if not app:
                # another device must have finished off this batch
                continue

            # charge
            device.prepare_for_test(arduino)
            # fetch apk
            if not fetch_apk(app, version):
                continue

            try:
                run_test(device, arduino, test, app, version, batch)
                queue.report_finished(test, app, version, batch)
            except (KeyboardInterrupt, SystemExit):
                raise
            except:
                extra = {
                    'run_info': {
                        'test': test,
                        'app': app,
                        'version': version,
                        'batch': batch
                    }
                }
                logging.exception('failed while running test', extra=extra)
                continue
            finally:
                # now we disconnect/rebuild the arduino/phone objects
                # this is useful because sometimes things go wrong and
                # we are no longer connected
                arduino.serial.close()
                builder = DeviceBuilder(args.device)
                device = builder.build_device()
                arduino = builder.build_arduino()


            StatusUpdater(arduino.deviceid).polling()
            queue.pull()
