/*
 Copyright (c) 2013 Kent Rasmussen

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/input.h>
#include <errno.h>
#include <time.h>

#define STEPS			10

enum InputEventValue {
	UP = 0,
	DOWN = 1
};

void write_event(int fd, __u16 type, __u16 code, __s32 value) {
	struct input_event event;
	memset(&event, 0, sizeof(event));
	int version;
	int ret;

	gettimeofday(&event.time, 0);
	event.type = type;
	event.code = code;
	event.value = value;

	ret = write(fd, &event, sizeof(event));
	if(ret < sizeof(event)) {
		fprintf(stderr, "write event failed, %s\n", strerror(errno));
		exit(-1);
	}
}

int main(int argc, char *argv[]) {
	int i;
	int fd;
	int ret;
	struct input_event event;
	memset(&event, 0, sizeof(event));

	char* tap_command = "tap";
	char* swipe_command = "swipe";

	// Howto
	if(argc == 1) {
		fprintf(stderr, "tapnswipe device tap x y [duration]\n");
		fprintf(stderr, "tapnswipe device swipe x1 y1 x2 y2 duration\n");
		return 1;
	}

	srand(time(NULL));
	fd = open(argv[1], O_RDWR);
	if(fd == -1) {
		fprintf(stderr, "could not open %s, %s\n", argv[1], strerror(errno));
		return 1;
	}

	// Tap
	if(!strncmp(argv[2], tap_command, 4)) {
		if(argc != 5 && argc != 6) {
			fprintf(stderr, "tapnswipe device tap x y [duration]\n");
			return 1;
		}

		//// Down
		write_event(fd, EV_KEY, BTN_TOUCH, DOWN);
		write_event(fd, EV_ABS, ABS_MT_TRACKING_ID, rand()); 
		write_event(fd, EV_ABS, ABS_MT_PRESSURE, 119); 
		write_event(fd, EV_ABS, ABS_MT_TOUCH_MAJOR, 13); 
		write_event(fd, EV_ABS, ABS_MT_POSITION_X, atoi(argv[3]));
		write_event(fd, EV_ABS, ABS_MT_POSITION_Y, atoi(argv[4]));
		write_event(fd, EV_SYN, SYN_MT_REPORT, 0);
		write_event(fd, EV_SYN, SYN_REPORT, 0);
		write_event(fd, EV_SYN, SYN_MT_REPORT, 0);
		if(argc == 5)
			usleep(50000);
		else
			usleep(atoi(argv[5]) * 1000);
		write_event(fd, EV_KEY, BTN_TOUCH, UP);
		write_event(fd, EV_SYN, SYN_REPORT, 0);

	// Swipe
	} else if(!strncmp(argv[2], swipe_command, 6)) {
		if(argc != 8) {
			fprintf(stderr, "tapnswipe device swipe x1 y1 x2 y2 duration\n");
		}

		// We've decided to have ten steps in the swipe
		int step_duration = atoi(argv[7]) / STEPS;
		int step_dx = (atoi(argv[5]) - atoi(argv[3])) / STEPS;
		int step_dy = (atoi(argv[6]) - atoi(argv[4])) / STEPS;
		int step_x = atoi(argv[3]);
		int step_y = atoi(argv[4]);
		int steps;

		int tracking_id = rand();

		write_event(fd, EV_KEY, BTN_TOUCH, DOWN);
		
		

		for(steps = STEPS; steps > 0; steps--) {			
			write_event(fd, EV_ABS, ABS_MT_TRACKING_ID, tracking_id); 
			write_event(fd, EV_ABS, ABS_MT_PRESSURE, 119); 
			write_event(fd, EV_ABS, ABS_MT_TOUCH_MAJOR, 13); 
			write_event(fd, EV_ABS, ABS_MT_POSITION_X, step_x);
			write_event(fd, EV_ABS, ABS_MT_POSITION_Y, step_y);
			
			usleep(step_duration * 1000);

			step_x += step_dx;
			step_y += step_dy;	
			write_event(fd, EV_SYN, SYN_MT_REPORT, 0);
			write_event(fd, EV_SYN, SYN_REPORT, 0);
		}
		
		write_event(fd, EV_SYN, SYN_MT_REPORT, 0);
		write_event(fd, EV_KEY, BTN_TOUCH, UP);		
		write_event(fd, EV_SYN, SYN_REPORT, 0);

	// Unknown
	} else {
		fprintf(stdout, "I don't know what you want to do...\n");
		fprintf(stderr, "\ttapnswipe device tap x y [duration]\n");
		fprintf(stderr, "\ttapnswipe device swipe x1 y1 x2 y2 duration\n");
		close(fd);
		return 1;
	}

	close(fd);

	return 0;
}
