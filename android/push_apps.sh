#!/usr/bin/env sh
# Installs some special apps to the phone to make our lives easier.

# Put files to sdcard
adb push microtime-lolipop/obj/local/arm64-v8a/microtime /sdcard/
adb push microsleep-lolipop/obj/local/arm64-v8a/microsleep /sdcard/
adb push tapnswipe-ZE500KL/obj/local/arm64-v8a/tapnswipe /sdcard/
adb push sqlite3-lolipop/sqlite3 /sdcard/
adb push busybox-lolipop/busybox /sdcard/

# Mount /system/
adb shell su -c "mount -o remount,rw /system"

# Move files
adb shell su -c "cp /sdcard/microtime /system/xbin"
adb shell rm /sdcard/microtime
adb shell su -c "cp /sdcard/microsleep /system/xbin"
adb shell rm /sdcard/microsleep
adb shell su -c "cp /sdcard/tapnswipe /system/xbin"
adb shell rm /sdcard/tapnswipe
adb shell su -c "cp /sdcard/sqlite3 /system/xbin"
adb shell rm /sdcard/sqlite3
adb shell su -c "cp /sdcard/busybox /system/xbin"
adb shell rm /sdcard/busybox

# Fix permissions
adb shell su -c "chmod 755 /system/xbin/microtime"
adb shell su -c "chmod 755 /system/xbin/microsleep"
adb shell su -c "chmod 755 /system/xbin/tapnswipe"
adb shell su -c "chmod 755 /system/xbin/sqlite3"
adb shell su -c "chmod 755 /system/xbin/busybox"

#Reboot
#adb reboot
