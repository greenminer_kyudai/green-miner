#!/usr/bin/env python3
#
# usage: ./get_device_info.py <device_letter>
#
# Copyright (c) 2013 Kent Rasmussen, Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import json, sys
import libgreenminer

builder = libgreenminer.DeviceBuilder(sys.argv[1])
phone = builder.build_phone()
arduino = builder.build_arduino()

# Ensure Phone is Connected
phone.connect(arduino)

# Dump Info
print(json.dumps(phone.dump_info()))
