/*
 Reads voltage and amperage from a device connected to an INA219;
 controls a transistor via. digital signal to turn on/off power to a USB
 device.

 Copyright (c) 2013 Abram Hindle, Jed Barlow, Kent Rasmussen

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <Wire.h>
#include <Adafruit_INA219.h>

#define MY_ID           'B'

#define USB_DISCON_PIN  4
#define STATE_ON        '+'
#define STATE_OFF       '#'

Adafruit_INA219 ina219;

char state = STATE_OFF;
float shuntvoltage = 0;
float busvoltage = 0;
float current_mA = 0;
float loadvoltage = 0;

unsigned long time;

void setup(void) {
  // Disconnect Phone
  pinMode(USB_DISCON_PIN, OUTPUT);
  digitalWrite(USB_DISCON_PIN, HIGH);

  // Begin Communication
  Serial.begin(19200);

  // Library Sets INA219 to 16V, ~1.3A Maximum
  ina219.begin();
}

void loop(void) {
  // Computer is Sending Info
  if(Serial.available() > 0) {
    int ser_dat = Serial.read();

    // Connect Phone
    if (ser_dat == '0') {
      digitalWrite(USB_DISCON_PIN, LOW);
      state = STATE_ON;
    }

    // Disconnect Phone
    else if (ser_dat == '1') {
      digitalWrite(USB_DISCON_PIN, HIGH);
      state = STATE_OFF;
    }
  }

  // Get Readings
  shuntvoltage = ina219.getShuntVoltage_mV();
  busvoltage = ina219.getBusVoltage_V();
  current_mA = ina219.getCurrent_mA();
  loadvoltage = busvoltage + (shuntvoltage / 1000);

  // Send Readings
  Serial.print(MY_ID);
  Serial.print("\t");
  Serial.print(state);
  Serial.print("\t");
  Serial.print(current_mA);
  Serial.print("\t");
  Serial.println(loadvoltage);

  // Conversion Time for Settings = 17.02ms
  delay(17);
}
